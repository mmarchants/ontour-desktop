package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author root
 */
public class Agencia {
    
    // Variables:
    
    private Long id;
    private String codigo;
    private String nombre;
    private String direccion;
    private String telefono;
    private String correo_electronico;
    private String enlace_logo;
    private Ciudad ciudad;
    private boolean estado;

    // Constructores:
    
    public Agencia() {
    }

    public Agencia(Long id, String codigo, String nombre, String direccion, String telefono, String correo_electronico, String enlace_logo, Ciudad ciudad, boolean estado) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.correo_electronico = correo_electronico;
        this.enlace_logo = enlace_logo;
        this.ciudad = ciudad;
        this.estado = estado;
    }

    // Accesadores y mutadores:
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo_electronico() {
        return correo_electronico;
    }

    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    public String getEnlace_logo() {
        return enlace_logo;
    }

    public void setEnlace_logo(String enlace_logo) {
        this.enlace_logo = enlace_logo;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    // Métodos:
    
    @Override
    public String toString() {
        return this.getCodigo() + " - " + this.getNombre();
    }
    
    public static List<Agencia> listadoAgencias() {
        List<Agencia> agencias = new ArrayList<Agencia>();
        String respuesta = Funciones.requestWS(Administrador.key, "", "agencia", "GET");
        if (!respuesta.isEmpty()) {
            Agencia agencia;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {
                agencia = new Agencia();
                agencia.setId(listado.getJSONObject(i).getLong("id"));
                agencia.setCodigo(listado.getJSONObject(i).getString("codigo"));
                agencia.setNombre(listado.getJSONObject(i).getString("nombre"));
                agencia.setDireccion(listado.getJSONObject(i).getString("direccion"));
                agencia.setTelefono(listado.getJSONObject(i).getString("telefono"));
                agencia.setCorreo_electronico(listado.getJSONObject(i).getString("correo_electronico"));
                agencia.setEnlace_logo(listado.getJSONObject(i).get("enlace_logo").toString());
                agencia.setCiudad(Ciudad.buscarPorId(listado.getJSONObject(i).getJSONObject("ciudad").get("id").toString()));
                agencia.setEstado(listado.getJSONObject(i).getBoolean("estado"));
                agencias.add(agencia);
            }
        }
        return agencias;
    }
    
    public static Agencia buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "agencia/"+id, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Agencia agencia = new Agencia();
            agencia.setId(json.getLong("id"));
            agencia.setCodigo(json.getString("codigo"));
            agencia.setNombre(json.getString("nombre"));
            agencia.setDireccion(json.getString("direccion"));
            agencia.setTelefono(json.getString("telefono"));
            agencia.setCorreo_electronico(json.getString("correo_electronico"));
            agencia.setEnlace_logo(json.get("enlace_logo").toString());
            agencia.setCiudad(Ciudad.buscarPorId(json.getJSONObject("ciudad").get("id").toString()));
            agencia.setEstado(json.getBoolean("estado"));
            return agencia;
        }
        return null;
    }
    
    public static Agencia buscarPorCodigo(String codigo) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "agencia/codigo/"+codigo, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Agencia agencia = new Agencia();
            agencia.setId(json.getLong("id"));
            agencia.setCodigo(json.getString("codigo"));
            agencia.setNombre(json.getString("nombre"));
            agencia.setDireccion(json.getString("direccion"));
            agencia.setTelefono(json.getString("telefono"));
            agencia.setCorreo_electronico(json.getString("correo_electronico"));
            agencia.setEnlace_logo(json.get("enlace_logo").toString());
            agencia.setCiudad(Ciudad.buscarPorId(json.getJSONObject("ciudad").get("id").toString()));
            agencia.setEstado(json.getBoolean("estado"));
            return agencia;
        }
        return null;
    }
    
    public static boolean modificarAgencia(String id, String codigo, String nombre, String direccion, String telefono, String correo_electronico, String enlace_logo, String id_ciudad, String estado) {
        String respuesta = Funciones.requestWS(Administrador.key, bodyAgencia(codigo, nombre, direccion, telefono, correo_electronico, enlace_logo, id_ciudad, estado), "agencia/" + id, "PUT");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    private static String bodyAgencia(String codigo, String nombre, String direccion, String telefono, String correo_electronico, String enlace_logo, String id_ciudad, String estado) {
        return 
            "{" +     
                "\"codigo\": \"" + codigo +"\"," +
                "\"nombre\": \"" + nombre + "\"," +
                "\"direccion\": \"" + direccion + "\"," +
                "\"telefono\": \"" + telefono + "\"," +
                "\"correo_electronico\": \"" + correo_electronico + "\"," +
                "\"enlace_logo\": " + (enlace_logo.isEmpty() ? "null" : "\"" + enlace_logo + "\"") + "," +
                "\"ciudad\": {" +
                    "\"id\": " + id_ciudad +
                "}," +
                "\"estado\": " + estado +
            "}";        
    }
    
    public static boolean verificaCodigo(String codigo) {
        if (buscarPorCodigo(codigo) != null) {
            Administrador.error = "Código ya ha sido registrado.";
            return false;
        }   
        return true;
    }
}
