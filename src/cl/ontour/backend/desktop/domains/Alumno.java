package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import javax.swing.table.DefaultTableModel;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Andrea
 */
public class Alumno {

    private int id_alumno;
    private String dni_alumno;
    private String nombre_alumno;
    private String primer_apellido;
    private String segundo_apellido;
    private String email_alumno;
    private int estado_alumno;

    public Alumno() {
    }

    public Alumno(int id_alumno, String dni_alumno, String nombre_alumno, String primer_apellido, String segundo_apellido, String email_alumno, int estado_alumno) {
        this.id_alumno = id_alumno;
        this.dni_alumno = dni_alumno;
        this.nombre_alumno = nombre_alumno;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
        this.email_alumno = email_alumno;
        this.estado_alumno = estado_alumno;
    }

    //GETERS
    public int getId_alumno() {
        return id_alumno;
    }

    public String getDni_alumno() {
        return dni_alumno;
    }

    public String getNombre_alumno() {
        return nombre_alumno;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public String getSegundo_apellido() {
        return segundo_apellido;
    }

    public String getEmail_alumno() {
        return email_alumno;
    }

    public int getEstado_alumno() {
        return estado_alumno;
    }

    //SETTERS
    public void setId_alumno(int id_alumno) {
        this.id_alumno = id_alumno;
    }

    public void setDni_alumno(String dni_alumno) {
        this.dni_alumno = dni_alumno;
    }

    public void setNombre_alumno(String nombre_alumno) {
        this.nombre_alumno = nombre_alumno;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public void setSegundo_apellido(String segundo_apellido) {
        this.segundo_apellido = segundo_apellido;
    }

    public void setEmail_alumno(String email_alumno) {
        this.email_alumno = email_alumno;
    }

    public void setEstado_alumno(int estado_alumno) {
        this.estado_alumno = estado_alumno;
    }

    //METODOS
    @Override
    public String toString() {
        return "Alumno{" + "id_alumno=" + id_alumno + ", dni_alumno=" + dni_alumno + ", nombre_alumno=" + nombre_alumno + ", primer_apellido=" + primer_apellido + ", segundo_apellido=" + segundo_apellido + ", email_alumno=" + email_alumno + ", estado_alumno=" + estado_alumno + '}';
    }

    public static DefaultTableModel listarAlumnos(DefaultTableModel tabla) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "alumno", "GET");
        //System.out.println(respuesta);
        if (!respuesta.isEmpty()) {
            JSONArray listado = new JSONArray(respuesta);
            JSONObject alumno = new JSONObject();
            for (int i = 0; i < listado.length(); i++) {
                //System.out.println("Alumno N°"+i+": "+listado.get(i));            
                alumno = listado.getJSONObject(i);
                tabla.insertRow(i, new Object[]{});
                tabla.setValueAt(alumno.get("id"), i, 0);
                tabla.setValueAt(alumno.get("dni"), i, 1);
                tabla.setValueAt(alumno.get("nombres"), i, 2);
                tabla.setValueAt(alumno.get("primer_apellido"), i, 3);
                tabla.setValueAt(alumno.get("segundo_apellido"), i, 4);
                tabla.setValueAt(alumno.get("correo_electronico"), i, 5);
                tabla.setValueAt(alumno.get("correo_electronico"), i, 6);

            }
        }
        return tabla;
    }

    public static boolean registrarAlumno(String dni, String nombre, String primer_apellido, String segundo_apellido, String email) {
        String respuesta = Funciones.requestWS(Administrador.key, Alumno.bodyAlumno(dni, nombre, primer_apellido, segundo_apellido, email), "alumno", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }

    private static String bodyAlumno(String dni, String nombre, String primer_apellido, String segundo_apellido, String email) {
        return "{"
                + "\"dni\": \"" + dni + "\","
                + "\"nombres\": \"" + nombre + "\","
                + "\"primer_apellido\": \"" + primer_apellido + "\","
                + "\"segundo_apellido\": \"" + segundo_apellido + "\","
                + "\"correo_electronico\": \"" + email + "\""
                + "}";
    }
}
