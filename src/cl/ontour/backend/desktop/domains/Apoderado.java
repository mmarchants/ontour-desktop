package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Mariano Marchant S.
 */
public class Apoderado {
    
    // Atributos:
    
    private Long id;
    private String nombre;
    private String primer_apellido;
    private String segundo_apellido;
    private String telefono;
    private Usuario usuario;
    
    // Constructores:

    public Apoderado() {
    }

    public Apoderado(Long id, String nombre, String primer_apellido, String segundo_apellido, String telefono, Usuario usuario) {
        this.id = id;
        this.nombre = nombre;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
        this.telefono = telefono;
        this.usuario = usuario;
    }
    
    // Accesadores y mutadores:

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public String getSegundo_apellido() {
        return segundo_apellido;
    }

    public void setSegundo_apellido(String segundo_apellido) {
        this.segundo_apellido = segundo_apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    // Métodos:

    @Override
    public String toString() {
        return this.getNombre() + " " + this.getPrimer_apellido() + " " + this.getSegundo_apellido();
    }
    
    public static List<Apoderado> listadoApoderados() {
        List<Apoderado> apoderados = new ArrayList<Apoderado>();
        String respuesta = Funciones.requestWS(Administrador.key, "", "apoderado", "GET");
        if (!respuesta.isEmpty()) {
            Apoderado apoderado;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {
                apoderado = new Apoderado();
                apoderado.setId(listado.getJSONObject(i).getLong("id"));                
                apoderado.setNombre(listado.getJSONObject(i).getString("nombre"));
                apoderado.setPrimer_apellido(listado.getJSONObject(i).getString("primer_apellido"));
                apoderado.setSegundo_apellido(listado.getJSONObject(i).getString("segundo_apellido"));
                apoderado.setTelefono(listado.getJSONObject(i).getString("telefono"));
                apoderado.setUsuario(Usuario.buscarPorId(String.valueOf(listado.getJSONObject(i).getJSONObject("usuario").getLong("id"))));
                apoderados.add(apoderado);
            }
        }
        return apoderados;
    }
    
    public static Apoderado buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "apoderado/"+id, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Apoderado apoderado = new Apoderado();
            apoderado.setId(json.getLong("id"));
            apoderado.setNombre(json.getString("nombre"));
            apoderado.setPrimer_apellido(json.getString("primer_apellido"));
            apoderado.setSegundo_apellido(json.getString("segundo_apellido"));
            apoderado.setTelefono(json.getString("telefono"));
            apoderado.setUsuario(Usuario.buscarPorId(String.valueOf(json.getJSONObject("usuario").getLong("id"))));            
            return apoderado;
        }
        return null;        
    }
    
    public static Apoderado buscarPorCorreo(String correo_electronico) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "apoderado/user/"+correo_electronico, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Apoderado apoderado = new Apoderado();
            apoderado.setId(json.getLong("id"));
            apoderado.setNombre(json.getString("nombre"));
            apoderado.setPrimer_apellido(json.getString("primer_apellido"));
            apoderado.setSegundo_apellido(json.getString("segundo_apellido"));
            apoderado.setTelefono(json.getString("telefono"));
            apoderado.setUsuario(Usuario.buscarPorId(String.valueOf(json.getJSONObject("usuario").getLong("id"))));
            return apoderado;
        }
        return null;   
    }
    
    public static boolean registrarApoderado(String nombre, String primer_apellido, String segundo_apellido, String telefono, Usuario usuario) {        
        String respuesta = Funciones.requestWS(Administrador.key, Apoderado.bodyApoderado(nombre, primer_apellido, segundo_apellido, telefono, usuario), "apoderado", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean modificarApoderado(String id, String nombre, String primer_apellido, String segundo_apellido, String telefono, Usuario usuario) {
        String respuesta = Funciones.requestWS(Administrador.key, Apoderado.bodyApoderado(nombre, primer_apellido, segundo_apellido, telefono, usuario), "apoderado/"+id, "PUT");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    private static String bodyApoderado(String nombre, String primer_apellido, String segundo_apellido, String telefono, Usuario usuario) {
        return 
            "{" +
                "\"nombre\": \"" + nombre + "\"," +
                "\"primer_apellido\": \"" + primer_apellido +"\"," +
                "\"segundo_apellido\": \"" + segundo_apellido +"\"," +
                "\"telefono\": \"" + telefono + "\"," +
                "\"usuario\": {" +
                    "\"id\": " + usuario.getId() +
                "}" + 
            "}";
    }
    
}
