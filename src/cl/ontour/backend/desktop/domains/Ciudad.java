package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Andrea
 */
public class Ciudad {

    private Long id_ciudad;
    private String codigo_ciudad;
    private String nombre_ciudad;
    private Pais pais_ciudad;
    private boolean estado_ciudad;

    public Ciudad() {
    }

    public Ciudad(Long id_ciudad, String codigo_ciudad, String nombre_ciudad, Pais pais_ciudad, boolean estado_ciudad) {
        this.id_ciudad = id_ciudad;
        this.codigo_ciudad = codigo_ciudad;
        this.nombre_ciudad = nombre_ciudad;
        this.pais_ciudad = pais_ciudad;
        this.estado_ciudad = estado_ciudad;
    }

    public Long getId_ciudad() {
        return id_ciudad;
    }

    public void setId_ciudad(Long id_ciudad) {
        this.id_ciudad = id_ciudad;
    }

    public String getCodigo_ciudad() {
        return codigo_ciudad;
    }

    public void setCodigo_ciudad(String codigo_ciudad) {
        this.codigo_ciudad = codigo_ciudad;
    }

    public String getNombre_ciudad() {
        return nombre_ciudad;
    }

    public void setNombre_ciudad(String nombre_ciudad) {
        this.nombre_ciudad = nombre_ciudad;
    }

    public Pais getPais_ciudad() {
        return pais_ciudad;
    }

    public void setPais_ciudad(Pais pais_ciudad) {
        this.pais_ciudad = pais_ciudad;
    }

    public boolean isEstado_ciudad() {
        return estado_ciudad;
    }

    public void setEstado_ciudad(boolean estado_ciudad) {
        this.estado_ciudad = estado_ciudad;
    }

    @Override
    public String toString() {
        return this.getCodigo_ciudad() + " - " + this.getNombre_ciudad();
    }

    public static List<Ciudad> listadoCiudades() {
        List<Ciudad> ciudades = new ArrayList<Ciudad>();
        String respuesta = Funciones.requestWS(Administrador.key, "", "ciudad", "GET");
        if (!respuesta.isEmpty()) {            
            Ciudad ciudad;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {                         
                ciudad = new Ciudad();
                ciudad.setId_ciudad(listado.getJSONObject(i).getLong("id"));
                ciudad.setCodigo_ciudad(listado.getJSONObject(i).getString("codigo"));
                ciudad.setNombre_ciudad(listado.getJSONObject(i).getString("nombre"));
                ciudad.setPais_ciudad(Pais.buscarPorId(String.valueOf(listado.getJSONObject(i).getJSONObject("pais").get("id"))));
                ciudad.setEstado_ciudad(listado.getJSONObject(i).getBoolean("estado"));
                ciudades.add(ciudad);                      
            }
        }
        return ciudades;
    }
    
    public static Ciudad buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "ciudad/"+id, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Ciudad ciudad = new Ciudad();
            ciudad.setId_ciudad(json.getLong("id"));
            ciudad.setCodigo_ciudad(json.getString("codigo"));
            ciudad.setNombre_ciudad(json.getString("nombre"));
            ciudad.setPais_ciudad(Pais.buscarPorId(String.valueOf(json.getJSONObject("pais").get("id"))));
            ciudad.setEstado_ciudad(json.getBoolean("estado"));
            return ciudad;
        }
        Administrador.error = "ID ingresado no existe.";
        return null;
    }

    public static Ciudad buscarPorCodigo(String codigo) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "ciudad/codigo/"+codigo, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Ciudad ciudad = new Ciudad();
            ciudad.setId_ciudad(json.getLong("id"));
            ciudad.setCodigo_ciudad(json.getString("codigo"));
            ciudad.setNombre_ciudad(json.getString("nombre"));
            ciudad.setPais_ciudad(Pais.buscarPorId(String.valueOf(json.getJSONObject("pais").get("id"))));
            ciudad.setEstado_ciudad(json.getBoolean("estado"));
            return ciudad;
        }
        Administrador.error = "Código ingresado no existe.";
        return null;
    }

    public static boolean registrarCiudad(String codigo, String nombre, Pais pais) {
        String respuesta = Funciones.requestWS(Administrador.key, Ciudad.bodyCiudad(codigo, nombre, pais, "true"), "ciudad", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean modificarCiudad(String id, String codigo, String nombre, Pais pais, String estado) {
        String respuesta = Funciones.requestWS(Administrador.key, bodyCiudad(codigo, nombre, pais, estado), "ciudad/"+id, "PUT");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean eliminarCiudad(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "ciudad/"+id, "DELETE");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }

    private static String bodyCiudad(String codigo, String nombre, Pais pais, String estado) {
        return 
            "{" +                
                "\"codigo\": \"" + codigo + "\"," +
                "\"nombre\": \"" + nombre + "\"," +
                "\"pais\": {" +
                    "\"id\": " + pais.getId_pais() +
                "}," +
                "\"estado\": " + estado +
            "}";
    }
    
    public static boolean verificaCodigo(String codigo) {
        if (buscarPorCodigo(codigo) != null) {
            Administrador.error = "Código ya ha sido registrado.";
            return false;
        }   
        return true;
    }

}
