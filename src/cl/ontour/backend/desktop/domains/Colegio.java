package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Andrea
 */
public class Colegio {

    // Variables:
    
    private Long id;
    private String codigo;
    private String nombre;
    private String direccion;
    private String telefono;
    private String correo_electronico;
    private Ciudad ciudad;
    private boolean estado;

    // Constructores:
    
    public Colegio() {
    }

    public Colegio(Long id, String codigo, String nombre, String direccion, String telefono, String correo_electronico, Ciudad ciudad, boolean estado) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.correo_electronico = correo_electronico;
        this.ciudad = ciudad;
        this.estado = estado;
    }
    
    // Accesadores y mutadores:
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo_electronico() {
        return correo_electronico;
    }

    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    // Métodos:

    @Override
    public String toString() {
        return this.getCodigo() + " - " + this.getNombre();
    }
    
    public static List<Colegio> listadoColegios() {
        List<Colegio> colegios = new ArrayList<Colegio>();
        String respuesta = Funciones.requestWS(Administrador.key, "", "colegio", "GET");
        if (!respuesta.isEmpty()) {
            Colegio colegio;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {
                colegio = new Colegio();
                colegio.setId(listado.getJSONObject(i).getLong("id"));
                colegio.setCodigo(listado.getJSONObject(i).getString("codigo"));
                colegio.setNombre(listado.getJSONObject(i).getString("nombre"));
                colegio.setDireccion(listado.getJSONObject(i).getString("direccion"));
                colegio.setTelefono(listado.getJSONObject(i).get("telefono").toString());
                colegio.setCorreo_electronico(listado.getJSONObject(i).get("correo_electronico").toString());
                colegio.setCiudad(Ciudad.buscarPorId(String.valueOf(listado.getJSONObject(i).getJSONObject("ciudad").getLong("id"))));
                colegio.setEstado(listado.getJSONObject(i).getBoolean("estado"));
                colegios.add(colegio);
            }
        }
        return colegios;
    }

    public static Colegio buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "colegio/"+id, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Colegio colegio = new Colegio();
            colegio.setId(json.getLong("id"));
            colegio.setCodigo(json.getString("codigo"));
            colegio.setNombre(json.getString("nombre"));
            colegio.setDireccion(json.getString("direccion"));
            colegio.setTelefono(json.get("telefono").toString());
            colegio.setCorreo_electronico(json.get("correo_electronico").toString());
            colegio.setCiudad(Ciudad.buscarPorId(String.valueOf(json.getJSONObject("ciudad").getLong("id"))));
            colegio.setEstado(json.getBoolean("estado"));
            return colegio;
        }
        Administrador.error = "ID ingresado no existe.";
        return null;
    }
    
    public static Colegio buscarPorCodigo(String codigo) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "colegio/codigo/"+codigo, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Colegio colegio = new Colegio();
            colegio.setId(json.getLong("id"));
            colegio.setCodigo(json.getString("codigo"));
            colegio.setNombre(json.getString("nombre"));
            colegio.setDireccion(json.getString("direccion"));
            colegio.setTelefono(json.get("telefono").toString());
            colegio.setCorreo_electronico(json.get("correo_electronico").toString());
            colegio.setCiudad(Ciudad.buscarPorId(String.valueOf(json.getJSONObject("ciudad").getLong("id"))));
            colegio.setEstado(json.getBoolean("estado"));
            return colegio;
        }
        Administrador.error = "Código ingresado no existe.";
        return null;
    }
    
    public static boolean registrarColegio(String codigo, String nombre, String direccion, String telefono, String correo_electronico, Ciudad ciudad) {        
        String respuesta = Funciones.requestWS(Administrador.key, Colegio.bodyColegio(codigo, nombre, direccion, telefono, correo_electronico, ciudad, "true"), "colegio", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean modificarColegio(String id, String codigo, String nombre, String direccion, String telefono, String correo_electronico, Ciudad ciudad, String estado) {
        String respuesta = Funciones.requestWS(Administrador.key, bodyColegio(codigo, nombre, direccion, telefono, correo_electronico, ciudad, estado), "colegio/"+id, "PUT");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean eliminarColegio(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "colegio/"+id, "DELETE");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }

    private static String bodyColegio(String codigo, String nombre, String direccion, String telefono, String correo_electronico, Ciudad ciudad, String estado) {
        return 
            "{" +
                "\"codigo\": \"" + codigo + "\"," +
                "\"nombre\": \"" + nombre + "\"," +
                "\"direccion\": \"" + direccion + "\"," +
                "\"telefono\": \"" + telefono + "\"," +
                "\"correo_electronico\": \"" + correo_electronico + "\"," +
                "\"ciudad\": {" +
                    "\"id\": " + ciudad.getId_ciudad() + 
                "}," +
                "\"estado\": " + estado + 
            "}";
    }

    
}
