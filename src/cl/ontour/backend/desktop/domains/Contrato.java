package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.Date;
import javax.swing.table.DefaultTableModel;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Andrea
 */
public class Contrato {

    private int id_contrato;
    private String codigo_contrato;
    private String descripcion_contrato;
    private Date fecha_contrato;
    private int estado_contrato;

    public Contrato() {
    }

    public Contrato(int id_contrato, String codigo_contrato, String descripcion_contrato, Date fecha_contrato, int estado_contrato) {
        this.id_contrato = id_contrato;
        this.codigo_contrato = codigo_contrato;
        this.descripcion_contrato = descripcion_contrato;
        this.fecha_contrato = fecha_contrato;
        this.estado_contrato = estado_contrato;
    }

    //GETERS
    public int getId_contrato() {
        return id_contrato;
    }

    public String getCodigo_contrato() {
        return codigo_contrato;
    }

    public String getDescripcion_contrato() {
        return descripcion_contrato;
    }

    public Date getFecha_contrato() {
        return fecha_contrato;
    }

    public int getEstado_contrato() {
        return estado_contrato;
    }

    //SETTERS 
    public void setId_contrato(int id_contrato) {
        this.id_contrato = id_contrato;
    }

    public void setCodigo_contrato(String codigo_contrato) {
        this.codigo_contrato = codigo_contrato;
    }

    public void setDescripcion_contrato(String descripcion_contrato) {
        this.descripcion_contrato = descripcion_contrato;
    }

    public void setFecha_contrato(Date fecha_contrato) {
        this.fecha_contrato = fecha_contrato;
    }

    public void setEstado_contrato(int estado_contrato) {
        this.estado_contrato = estado_contrato;
    }

    //METODOS
    @Override
    public String toString() {
        return "Contrato{" + "id_contrato=" + id_contrato + ", codigo_contrato=" + codigo_contrato + ", descripcion_contrato=" + descripcion_contrato + ", fecha_contrato=" + fecha_contrato + ", estado_contrato=" + estado_contrato + '}';
    }

    public static DefaultTableModel listarContratos(DefaultTableModel tabla) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "contrato", "GET");
        //System.out.println(respuesta);
        if (!respuesta.isEmpty()) {
            JSONArray listado = new JSONArray(respuesta);
            JSONObject contrato = new JSONObject();
            for (int i = 0; i < listado.length(); i++) {
                //System.out.println("Contrato N°"+i+": "+listado.get(i));            
                contrato = listado.getJSONObject(i);
                tabla.insertRow(i, new Object[]{});
                tabla.setValueAt(contrato.get("id"), i, 0);
                tabla.setValueAt(contrato.get("codigo"), i, 1);
                tabla.setValueAt(contrato.get("descripcion"), i, 2);
                tabla.setValueAt(contrato.get("fecha_suscripcion"), i, 3);
                tabla.setValueAt(contrato.get("estado"), i, 4);
            }
        }
        return tabla;
    }

    public static boolean registrarContrato(String codigo, String descripcion) {
        String respuesta = Funciones.requestWS(Administrador.key, Contrato.bodyContrato(codigo, descripcion), "contrato", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }

    private static String bodyContrato(String codigo, String descripcion) {
        return "{"
                + "\"codigo\": \"" + codigo + "\","
                + "\"descripcion\": \"" + descripcion + "\""
                + "}";
    }
}
