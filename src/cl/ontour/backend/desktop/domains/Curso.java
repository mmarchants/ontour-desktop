package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import javax.naming.spi.DirStateFactory.Result;
import javax.swing.table.DefaultTableModel;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Andrea
 */
public class Curso {

    private int id_curso;
    private String codigo_curso;
    private String email_curso;
    private int estado_curso;

    public Curso() {
    }

    public Curso(int id_curso, String codigo_curso, String email_curso, int estado_curso) {
        this.id_curso = id_curso;
        this.codigo_curso = codigo_curso;
        this.email_curso = email_curso;
        this.estado_curso = estado_curso;
    }

    //GETERS
    public int getId_curso() {
        return id_curso;
    }

    public void setId_curso(int id_curso) {
        this.id_curso = id_curso;
    }

    public String getCodigo_curso() {
        return codigo_curso;
    }

    //SETTERS
    public void setCodigo_curso(String codigo_curso) {
        this.codigo_curso = codigo_curso;
    }

    public String getEmail_curso() {
        return email_curso;
    }

    public void setEmail_curso(String email_curso) {
        this.email_curso = email_curso;
    }

    public int getEstado_curso() {
        return estado_curso;
    }

    public void setEstado_curso(int estado_curso) {
        this.estado_curso = estado_curso;
    }

    //METODOS
    @Override
    public String toString() {
        return "Curso{" + "id_curso=" + id_curso + ", codigo_curso=" + codigo_curso + ", email_curso=" + email_curso + ", estado_curso=" + estado_curso + '}';
    }

    public static DefaultTableModel listarCursos(DefaultTableModel tabla) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "curso", "GET");
        //System.out.println(respuesta);
        if (!respuesta.isEmpty()) {
            JSONArray listado = new JSONArray(respuesta);
            JSONObject curso = new JSONObject();
            for (int i = 0; i < listado.length(); i++) {
                curso = listado.getJSONObject(i);
                tabla.insertRow(i, new Object[]{});
                tabla.setValueAt(curso.get("id"), i, 0);
                tabla.setValueAt(curso.get("codigo"), i, 1);
                tabla.setValueAt(curso.get("correo_electronico"), i, 2);
                tabla.setValueAt(curso.get("estado"), i, 3);
            }
        }
        return tabla;
    }

    public static boolean registrarCurso(String codigo, String email) {
        String respuesta = Funciones.requestWS(Administrador.key, Curso.bodyCurso(codigo, email), "curso", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    

    private static String bodyCurso(String codigo, String email) {
        return "{"
                + "\"codigo\": \"" + codigo + "\","
                + "\"correo_electronico\": \"" + email + "\""
                + "}";

    }
}
