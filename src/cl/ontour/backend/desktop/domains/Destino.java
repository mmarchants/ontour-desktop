package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Mariano Marchant S.
 */
public class Destino {
    
    // Atributos:
    
    private Long id;
    private String codigo;
    private String nombre;
    private String descripcion;
    private String enlace_imagen;
    private Pais pais;
    private boolean estado;
    
    // Constructores:

    public Destino() {
    }

    public Destino(Long id, String codigo, String nombre, String descripcion, String enlace_imagen, Pais pais, boolean estado) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.enlace_imagen = enlace_imagen;
        this.pais = pais;
        this.estado = estado;
    }

    // Accesadores y mutadores:

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEnlace_imagen() {
        return enlace_imagen;
    }

    public void setEnlace_imagen(String enlace_imagen) {
        this.enlace_imagen = enlace_imagen;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    // Métodos:

    @Override
    public String toString() {
        return this.getCodigo() + " - " + this.getNombre();
    }
    
    public static List<Destino> listadoDestinos() {
        List<Destino> destinos = new ArrayList<Destino>();
        String respuesta = Funciones.requestWS(Administrador.key, "", "destino", "GET");
        if (!respuesta.isEmpty()) {
            Destino destino;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {
                destino = new Destino();
                destino.setId(listado.getJSONObject(i).getLong("id"));
                destino.setCodigo(listado.getJSONObject(i).getString("codigo"));
                destino.setNombre(listado.getJSONObject(i).getString("nombre"));
                destino.setDescripcion(listado.getJSONObject(i).getString("descripcion"));
                destino.setEnlace_imagen(listado.getJSONObject(i).get("enlace_imagen").toString());
                destino.setPais(Pais.buscarPorId(String.valueOf(listado.getJSONObject(i).getJSONObject("pais").getLong("id"))));
                destino.setEstado(listado.getJSONObject(i).getBoolean("estado"));
                destinos.add(destino);
            }
        }
        return destinos;
    }
    
    public static Destino buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "destino/"+id, "GET");
        if (!respuesta.isEmpty()) {
            Destino destino;
            JSONObject json = new JSONObject(respuesta);                
            destino = new Destino();
            destino.setId(json.getLong("id"));
            destino.setCodigo(json.getString("codigo"));
            destino.setNombre(json.getString("nombre"));
            destino.setDescripcion(json.getString("descripcion"));
            destino.setEnlace_imagen(json.get("enlace_imagen").toString());
            destino.setPais(Pais.buscarPorId(String.valueOf(json.getJSONObject("pais").getLong("id"))));
            destino.setEstado(json.getBoolean("estado"));
            return destino;
        }
        Administrador.error = "ID ingresado no existe.";
        return null;
    }
    
    public static Destino buscarPorCodigo(String codigo) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "destino/codigo/"+codigo, "GET");
        if (!respuesta.isEmpty()) {
            Destino destino;
            JSONObject json = new JSONObject(respuesta);                
            destino = new Destino();
            destino.setId(json.getLong("id"));
            destino.setCodigo(json.getString("codigo"));
            destino.setNombre(json.getString("nombre"));
            destino.setDescripcion(json.getString("descripcion"));
            destino.setEnlace_imagen(json.get("enlace_imagen").toString());
            destino.setPais(Pais.buscarPorId(String.valueOf(json.getJSONObject("pais").getLong("id"))));
            destino.setEstado(json.getBoolean("estado"));
            return destino;
        }
        Administrador.error = "Código ingresado no existe.";
        return null;
    }
    
    public static boolean registarDestino(String codigo, String nombre, String descripcion, String enlace_imagen, Pais pais) {
        String respuesta = Funciones.requestWS(Administrador.key, bodyDestino(codigo, nombre, descripcion, enlace_imagen, pais, "true"), "destino", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean modificarDestino(String id, String codigo, String nombre, String descripcion, String enlace_imagen, Pais pais, String estado) {
        String respuesta = Funciones.requestWS(Administrador.key, bodyDestino(codigo, nombre, descripcion, enlace_imagen, pais, estado), "destino/"+id, "PUT");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean eliminarDestino(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "destino/"+id, "DELETE");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;        
    }
    
    private static String bodyDestino(String codigo, String nombre, String descripcion, String enlace_imagen, Pais pais, String estado) {
        return 
            "{" +
                "\"codigo\": \"" + codigo + "\"," +
                "\"nombre\": \"" + nombre + "\"," +
                "\"descripcion\": \"" + descripcion + "\"," +
                "\"enlace_imagen\": " + (enlace_imagen.isEmpty() ? "null" : "\"" + enlace_imagen + "\"") + "," +
                "\"pais\": {" +
                    "\"id\": " + pais.getId_pais() + 
                "}," +
                "\"estado\": " + estado + 
            "}";        
    }
}
