package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Mariano Marchant S.
 */
public class Empleado {
    
    // Variables:
    
    private Long id;
    private String codigo;
    private String nombre;
    private String primer_apellido;
    private String segundo_apellido;
    private String telefono;
    private Usuario usuario;
    private Agencia agencia;

    // Constructores:
    
    public Empleado() {
    }
    
    public Empleado(Long id, String codigo, String nombre, String primer_apellido, String segundo_apellido, String telefono, Usuario usuario, Agencia agencia) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.primer_apellido = primer_apellido;
        this.segundo_apellido = segundo_apellido;
        this.telefono = telefono;
        this.usuario = usuario;
        this.agencia = agencia;
    }
    
    // Accesadores y mutadores:

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimer_apellido() {
        return primer_apellido;
    }

    public void setPrimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }

    public String getSegundo_apellido() {
        return segundo_apellido;
    }

    public void setSegundo_apellido(String segundo_apellido) {
        this.segundo_apellido = segundo_apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }
    
    // Métodos:

    @Override
    public String toString() {
        return this.getCodigo() + " - " + this.getNombre();
    }
    
    public static List<Empleado> listadoEmpleados() {
        List<Empleado> empleados = new ArrayList<Empleado>();
        String respuesta = Funciones.requestWS(Administrador.key, "", "empleado", "GET");
        if (!respuesta.isEmpty()) {
            Empleado empleado;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {
                empleado = new Empleado();
                empleado.setId(listado.getJSONObject(i).getLong("id"));
                empleado.setCodigo(listado.getJSONObject(i).getString("codigo"));
                empleado.setNombre(listado.getJSONObject(i).getString("nombre"));
                empleado.setPrimer_apellido(listado.getJSONObject(i).getString("primer_apellido"));
                empleado.setSegundo_apellido(listado.getJSONObject(i).getString("segundo_apellido"));
                empleado.setTelefono(listado.getJSONObject(i).getString("telefono"));
                empleado.setUsuario(Usuario.buscarPorId(String.valueOf(listado.getJSONObject(i).getJSONObject("usuario").getLong("id"))));
                empleado.setAgencia(Agencia.buscarPorId(String.valueOf(listado.getJSONObject(i).getJSONObject("agencia").getLong("id"))));
                empleados.add(empleado);
            }
        }
        return empleados;
    }
    
    public static Empleado buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "empleado/"+id, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Empleado empleado = new Empleado();
            empleado.setId(json.getLong("id"));
            empleado.setCodigo(json.getString("codigo"));
            empleado.setNombre(json.getString("nombre"));
            empleado.setPrimer_apellido(json.getString("primer_apellido"));
            empleado.setSegundo_apellido(json.getString("segundo_apellido"));
            empleado.setTelefono(json.getString("telefono"));
            empleado.setUsuario(Usuario.buscarPorId(String.valueOf(json.getJSONObject("usuario").getLong("id"))));
            empleado.setAgencia(Agencia.buscarPorId(String.valueOf(json.getJSONObject("agencia").getLong("id"))));
            return empleado;
        }
        return null;        
    }
    
    public static Empleado buscarPorCorreo(String correo_electronico) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "empleado/user/"+correo_electronico, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Empleado empleado = new Empleado();
            empleado.setId(json.getLong("id"));
            empleado.setCodigo(json.getString("codigo"));
            empleado.setNombre(json.getString("nombre"));
            empleado.setPrimer_apellido(json.getString("primer_apellido"));
            empleado.setSegundo_apellido(json.getString("segundo_apellido"));
            empleado.setTelefono(json.getString("telefono"));
            empleado.setUsuario(Usuario.buscarPorId(String.valueOf(json.getJSONObject("usuario").getLong("id"))));
            empleado.setAgencia(Agencia.buscarPorId(String.valueOf(json.getJSONObject("agencia").getLong("id"))));
            return empleado;
        }
        return null;   
    }
    
    public static boolean registrarEmpleado(String codigo, String nombre, String primer_apellido, String segundo_apellido, String telefono, Usuario usuario, Agencia agencia) {        
        String respuesta = Funciones.requestWS(Administrador.key, Empleado.bodyEmpleado(codigo, nombre, primer_apellido, segundo_apellido, telefono, usuario, agencia), "empleado", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean modificarEmpleado(String id, String codigo, String nombre, String primer_apellido, String segundo_apellido, String telefono, Usuario usuario, Agencia agencia) {
        String respuesta = Funciones.requestWS(Administrador.key, Empleado.bodyEmpleado(codigo, nombre, primer_apellido, segundo_apellido, telefono, usuario, agencia), "empleado/"+id, "PUT");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    private static String bodyEmpleado(String codigo, String nombre, String primer_apellido, String segundo_apellido, String telefono, Usuario usuario, Agencia agencia) {
        return 
            "{" +
                "\"codigo\": \"" + codigo + "\"," +
                "\"nombre\": \"" + nombre + "\"," +
                "\"primer_apellido\": \"" + primer_apellido +"\"," +
                "\"segundo_apellido\": \"" + segundo_apellido +"\"," +
                "\"telefono\": \"" + telefono + "\"," +
                "\"usuario\": {" +
                    "\"id\": " + usuario.getId() +
                "}," +
                "\"agencia\": {" +
                    "\"id\": " + agencia.getId() + 
                "}" +
            "}";
    }
}
