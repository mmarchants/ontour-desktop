package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Mariano Marchant S.
 */
public class Hospedaje {
    
    // Atributos:
    
    private Long id;
    private String codigo;
    private String nombre;
    private String detalle;
    private Date fecha_ingreso;
    private Date fecha_salida;
    private Destino destino;
    private boolean estado;
    
    // Constructores:

    public Hospedaje() {
    }

    public Hospedaje(Long id, String codigo, String nombre, String detalle, Date fecha_ingreso, Date fecha_salida, Destino destino, boolean estado) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.detalle = detalle;
        this.fecha_ingreso = fecha_ingreso;
        this.fecha_salida = fecha_salida;
        this.destino = destino;
        this.estado = estado;
    }
    
    // Accesadores y mutadores:

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public Date getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(Date fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public Date getFecha_salida() {
        return fecha_salida;
    }

    public void setFecha_salida(Date fecha_salida) {
        this.fecha_salida = fecha_salida;
    }

    public Destino getDestino() {
        return destino;
    }

    public void setDestino(Destino destino) {
        this.destino = destino;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    // Métodos:

    @Override
    public String toString() {
        return this.getCodigo() + " - " + this.getNombre();
    }
    
    public static List<Hospedaje> listadoHospedajes() {
        List<Hospedaje> hospedajes = new ArrayList<Hospedaje>();
        String respuesta = Funciones.requestWS(Administrador.key, "", "hospedaje", "GET");
        if (!respuesta.isEmpty()) {
            Hospedaje hospedaje;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {
                hospedaje = new Hospedaje();
                hospedaje.setId(listado.getJSONObject(i).getLong("id"));
                hospedaje.setCodigo(listado.getJSONObject(i).getString("codigo"));
                hospedaje.setNombre(listado.getJSONObject(i).getString("nombre"));
                hospedaje.setDetalle(listado.getJSONObject(i).getString("detalle"));                
                try {
                    hospedaje.setFecha_ingreso(Funciones.formateador.parse(listado.getJSONObject(i).getString("fecha_ingreso")));
                    hospedaje.setFecha_salida(Funciones.formateador.parse(listado.getJSONObject(i).getString("fecha_salida")));
                } catch (ParseException ex) {
                    Logger.getLogger(Hospedaje.class.getName()).log(Level.SEVERE, null, ex);
                }
                hospedaje.setDestino(Destino.buscarPorId(listado.getJSONObject(i).getJSONObject("destino").get("id").toString()));
                hospedaje.setEstado(listado.getJSONObject(i).getBoolean("estado"));
                hospedajes.add(hospedaje);
            }
        }
        return hospedajes;
    }
    
    public static Hospedaje buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "hospedaje/"+id, "GET");
        if (!respuesta.isEmpty()) {
            JSONObject json = new JSONObject(respuesta);            
            Hospedaje hospedaje = new Hospedaje();
            hospedaje.setId(json.getLong("id"));
            hospedaje.setCodigo(json.getString("codigo"));
            hospedaje.setNombre(json.getString("nombre"));
            hospedaje.setDetalle(json.getString("detalle"));                
            try {
                hospedaje.setFecha_ingreso(Funciones.formateador.parse(json.getString("fecha_ingreso")));
                hospedaje.setFecha_salida(Funciones.formateador.parse(json.getString("fecha_salida")));
            } catch (ParseException ex) {
                Logger.getLogger(Hospedaje.class.getName()).log(Level.SEVERE, null, ex);
            }
            hospedaje.setDestino(Destino.buscarPorId(json.getJSONObject("destino").get("id").toString()));
            hospedaje.setEstado(json.getBoolean("estado"));
            return hospedaje;
        }
        Administrador.error = "ID ingresado no existe.";
        return null;
    }
    
    public static Hospedaje buscarPorCodigo(String codigo) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "hospedaje/codigo/"+codigo, "GET");
        if (!respuesta.isEmpty()) {
            JSONObject json = new JSONObject(respuesta);            
            Hospedaje hospedaje = new Hospedaje();
            hospedaje.setId(json.getLong("id"));
            hospedaje.setCodigo(json.getString("codigo"));
            hospedaje.setNombre(json.getString("nombre"));
            hospedaje.setDetalle(json.getString("detalle"));                
            try {
                hospedaje.setFecha_ingreso(Funciones.formateador.parse(json.getString("fecha_ingreso")));
                hospedaje.setFecha_salida(Funciones.formateador.parse(json.getString("fecha_salida")));
            } catch (ParseException ex) {
                Logger.getLogger(Hospedaje.class.getName()).log(Level.SEVERE, null, ex);
            }
            hospedaje.setDestino(Destino.buscarPorId(json.getJSONObject("destino").get("id").toString()));
            hospedaje.setEstado(json.getBoolean("estado"));
            return hospedaje;
        }
        Administrador.error = "Código ingresado no existe.";
        return null;
    }
    
    public static boolean registrarHospedaje(String codigo, String nombre, String detalle, String fecha_ingreso, String fecha_salida, Destino destino) {
        String respuesta = Funciones.requestWS(Administrador.key, bodyHospedaje(codigo, nombre, detalle, fecha_ingreso, fecha_salida, destino, true), "hospedaje", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean modificarHospedaje(String id, String codigo, String nombre, String detalle, String fecha_ingreso, String fecha_salida, Destino destino, boolean estado) {
        String respuesta = Funciones.requestWS(Administrador.key, bodyHospedaje(codigo, nombre, detalle, fecha_ingreso, fecha_salida, destino, estado), "hospedaje/"+id, "PUT");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean eliminarHospedaje(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "hospedaje/"+id, "DELETE");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    private static String bodyHospedaje(String codigo, String nombre, String detalle, String fecha_ingreso, String fecha_salida, Destino destino, boolean estado) {
        return 
            "{" +
                "\"codigo\": \"" + codigo + "\"," +
                "\"nombre\": \"" + nombre + "\"," +
                "\"detalle\": \"" + detalle + "\"," +
                "\"fecha_ingreso\": \"" + fecha_ingreso + "\"," +
                "\"fecha_salida\": \"" + fecha_salida + "\"," +
                "\"destino\": {" +
                    "\"id\": " + destino.getId() +
                "}," +
                "\"estado\": " + estado + 
            "}";
    }
}
