package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Andrea
 */
public class Pais {

    private Long id_pais;
    private String codigo_pais;
    private String nombre_pais;
    private boolean estado_pais;

    public Pais() {
    }

    public Pais(Long id_pais, String codigo_pais, String nombre_pais, boolean estado_pais) {
        this.id_pais = id_pais;
        this.codigo_pais = codigo_pais;
        this.nombre_pais = nombre_pais;
        this.estado_pais = estado_pais;
    }

    //GETERS
    public String getCodigo_pais() {
        return codigo_pais;
    }

    public boolean getEstado_pais() {
        return estado_pais;
    }

    public Long getId_pais() {
        return id_pais;
    }

    public String getNombre_pais() {
        return nombre_pais;
    }

    //SETTERS 
    public void setCodigo_pais(String codigo_pais) {
        this.codigo_pais = codigo_pais;
    }

    public void setEstado_pais(boolean estado_pais) {
        this.estado_pais = estado_pais;
    }

    public void setId_pais(Long id_pais) {
        this.id_pais = id_pais;
    }

    public void setNombre_pais(String nombre_pais) {
        this.nombre_pais = nombre_pais;
    }

    //METODOS

    @Override
    public String toString() {
        return this.getCodigo_pais() + " - " + this.getNombre_pais();
    }
    
    public static List<Pais> listadoPaises() {
        List<Pais> paises = new ArrayList<Pais>();
        String respuesta = Funciones.requestWS(Administrador.key, "", "pais", "GET");
        if (!respuesta.isEmpty()) {            
            Pais pais;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {                         
                pais = new Pais();
                pais.setId_pais(listado.getJSONObject(i).getLong("id"));
                pais.setCodigo_pais(listado.getJSONObject(i).getString("codigo"));
                pais.setNombre_pais(listado.getJSONObject(i).getString("nombre"));
                pais.setEstado_pais(listado.getJSONObject(i).getBoolean("estado"));
                paises.add(pais);                      
            }
        }
        return paises;
    }
    
    public static Pais buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "pais/"+id, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Pais pais = new Pais();
            pais.setId_pais(json.getLong("id"));
            pais.setCodigo_pais(json.getString("codigo"));
            pais.setNombre_pais(json.getString("nombre"));
            pais.setEstado_pais(json.getBoolean("estado"));
            return pais;
        }
        Administrador.error = "ID ingresado no existe.";
        return null;
    }

    public static Pais buscarPorCodigo(String codigo) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "pais/codigo/"+codigo, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Pais pais = new Pais();
            pais.setId_pais(json.getLong("id"));
            pais.setCodigo_pais(json.getString("codigo"));
            pais.setNombre_pais(json.getString("nombre"));
            pais.setEstado_pais(json.getBoolean("estado"));
            return pais;
        }
        Administrador.error = "Código ingresado no existe.";
        return null;
    }
    
    public static boolean registrarPais(String codigo, String nombre) {
        if (verificaCodigo(codigo)) {
            String respuesta = Funciones.requestWS(Administrador.key, Pais.bodyPais(codigo, nombre, "true"), "pais", "POST");
            if (respuesta.equals("Ok")) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean modificarPais(String id, String codigo, String nombre, String estado) {
        String respuesta = Funciones.requestWS(Administrador.key, Pais.bodyPais(codigo, nombre, estado), "pais/"+id, "PUT");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean eliminarPais(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "pais/"+id, "DELETE");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    private static String bodyPais(String codigo, String nombre, String estado) {
        return 
            "{" +
                "\"codigo\": \"" + codigo + "\"," +
                "\"nombre\": \"" + nombre + "\"," +
                "\"estado\": " + estado +
            "}";        
    }
    
    public static boolean verificaCodigo(String codigo) {
        if (buscarPorCodigo(codigo) != null) {
            Administrador.error = "Código ya ha sido registrado.";
            return false;
        }   
        return true;
    }
}
