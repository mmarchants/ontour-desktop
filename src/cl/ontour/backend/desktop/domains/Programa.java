package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import javax.swing.table.DefaultTableModel;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Andrea
 */
public class Programa {

    private int id_programa;
    private String codigo_programa;
    private String nombre_programa;
    private String detalle_programa;
    private int valor_programa;
    private int estado_programa;

    public Programa() {
    }

    public Programa(int id_p, String codigo_p, String nombre_p, String detalle_p, int valor_p, int estado_p) {
        this.id_programa = id_p;
        this.codigo_programa = codigo_p;
        this.nombre_programa = nombre_p;
        this.detalle_programa = detalle_p;
        this.valor_programa = valor_p;
        this.estado_programa = estado_p;
    }

    public String getCodigo_programa() {
        return codigo_programa;
    }

    public void setCodigo_programa(String codigo_programa) {
        this.codigo_programa = codigo_programa;
    }

    public String getDetalle_programa() {
        return detalle_programa;
    }

    public void setDetalle_programa(String detalle_programa) {
        this.detalle_programa = detalle_programa;
    }

    public int getEstado_programa() {
        return estado_programa;
    }

    public void setEstado_programa(int estado_programa) {
        this.estado_programa = estado_programa;
    }

    public int getId_programa() {
        return id_programa;
    }

    public void setId_programa(int id_programa) {
        this.id_programa = id_programa;
    }

    public String getNombre_programa() {
        return nombre_programa;
    }

    public void setNombre_programa(String nombre_programa) {
        this.nombre_programa = nombre_programa;
    }

    public int getValor_programa() {
        return valor_programa;
    }

    public void setValor_programa(int valor_programa) {
        this.valor_programa = valor_programa;
    }

    @Override
    public String toString() {
        return "Programa{" + "id_programa=" + id_programa + ", codigo_programa=" + codigo_programa + ", nombre_programa=" + nombre_programa + ", detalle_programa=" + detalle_programa + ", valor_programa=" + valor_programa + ", estado_programa=" + estado_programa + '}';
    }

    public static DefaultTableModel listarProgramas(DefaultTableModel tabla) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "programa", "GET");
        //System.out.println(respuesta);
        if (!respuesta.isEmpty()) {
            JSONArray listado = new JSONArray(respuesta);
            JSONObject programa = new JSONObject();
            for (int i = 0; i < listado.length(); i++) {
                //System.out.println("País N°"+i+": "+listado.get(i));            
                programa = listado.getJSONObject(i);
                tabla.insertRow(i, new Object[]{});
                tabla.setValueAt(programa.get("id"), i, 0);
                tabla.setValueAt(programa.get("codigo"), i, 1);
                tabla.setValueAt(programa.get("nombre"), i, 2);
                tabla.setValueAt(programa.get("detalle"), i, 3);
                tabla.setValueAt(programa.get("valor"), i, 4);
                tabla.setValueAt(programa.get("estado"), i, 5);

            }
        }
        return tabla;
    }

    public static boolean registrarPrograma(String codigo, String nombre, String detalle, int valor) {
        String respuesta = Funciones.requestWS(Administrador.key, Programa.bodyPrograma(codigo, nombre, detalle, valor), "programa", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }

    private static String bodyPrograma(String codigo, String nombre, String detalle, int valor) {
        return "{"
                + "\"codigo\": \"" + codigo + "\","
                + "\"nombre\": \"" + nombre + "\","
                + "\"detalle\": \"" + detalle + "\","
                + "\"valor\": \"" + valor + "\""
                + "}";
    }

}
