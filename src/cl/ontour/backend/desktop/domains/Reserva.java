package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.Date;
import javax.swing.table.DefaultTableModel;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Andrea
 */
public class Reserva {

    private int id_reserva;
    private String codigo_reserva;
    private String detalle_reserva;
    private int valor_reserva;
    private int monto_aval;
    private int monto_acumulado;
    private Date fecha_viaje;
    private Date fecha_limite;
    private int participantes_reserva;
    private String estado_reserva;

    public Reserva() {
    }

    public Reserva(int id_reserva, String codigo_reserva, String detalle_reserva, int valor_reserva, int monto_aval, int monto_acumulado, Date fecha_viaje, Date fecha_limite, int participantes_reserva, String estado_reserva) {
        this.id_reserva = id_reserva;
        this.codigo_reserva = codigo_reserva;
        this.detalle_reserva = detalle_reserva;
        this.valor_reserva = valor_reserva;
        this.monto_aval = monto_aval;
        this.monto_acumulado = monto_acumulado;
        this.fecha_viaje = fecha_viaje;
        this.fecha_limite = fecha_limite;
        this.participantes_reserva = participantes_reserva;
        this.estado_reserva = estado_reserva;
    }

    //GETERS
    public int getId_reserva() {
        return id_reserva;
    }

    public String getCodigo_reserva() {
        return codigo_reserva;
    }

    public String getDetalle_reserva() {
        return detalle_reserva;
    }

    public int getValor_reserva() {
        return valor_reserva;
    }

    public int getMonto_aval() {
        return monto_aval;
    }

    public int getMonto_acumulado() {
        return monto_acumulado;
    }

    public Date getFecha_viaje() {
        return fecha_viaje;
    }

    public Date getFecha_limite() {
        return fecha_limite;
    }

    public int getParticipantes_reserva() {
        return participantes_reserva;
    }

    public String getEstado_reserva() {
        return estado_reserva;
    }

    //SETTERS
    public void setId_reserva(int id_reserva) {
        this.id_reserva = id_reserva;
    }

    public void setCodigo_reserva(String codigo_reserva) {
        this.codigo_reserva = codigo_reserva;
    }

    public void setDetalle_reserva(String detalle_reserva) {
        this.detalle_reserva = detalle_reserva;
    }

    public void setValor_reserva(int valor_reserva) {
        this.valor_reserva = valor_reserva;
    }

    public void setMonto_aval(int monto_aval) {
        this.monto_aval = monto_aval;
    }

    public void setMonto_acumulado(int monto_acumulado) {
        this.monto_acumulado = monto_acumulado;
    }

    public void setFecha_viaje(Date fecha_viaje) {
        this.fecha_viaje = fecha_viaje;
    }

    public void setFecha_limite(Date fecha_limite) {
        this.fecha_limite = fecha_limite;
    }

    public void setParticipantes_reserva(int participantes_reserva) {
        this.participantes_reserva = participantes_reserva;
    }

    public void setEstado_reserva(String estado_reserva) {
        this.estado_reserva = estado_reserva;
    }

    //METODOS    
    @Override
    public String toString() {
        return "Reserva{" + "id_reserva=" + id_reserva + ", codigo_reserva=" + codigo_reserva + ", detalle_reserva=" + detalle_reserva + ", valor_reserva=" + valor_reserva + ", monto_aval=" + monto_aval + ", monto_acumulado=" + monto_acumulado + ", fecha_viaje=" + fecha_viaje + ", fecha_limite=" + fecha_limite + ", participantes_reserva=" + participantes_reserva + ", estado_reserva=" + estado_reserva + '}';
    }

    public static DefaultTableModel listarReservas(DefaultTableModel tabla) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "reserva", "GET");
        //System.out.println(respuesta);
        if (!respuesta.isEmpty()) {
            JSONArray listado = new JSONArray(respuesta);
            JSONObject reserva = new JSONObject();
            for (int i = 0; i < listado.length(); i++) {
                //System.out.println("Reserva N°"+i+": "+listado.get(i));            
                reserva = listado.getJSONObject(i);
                tabla.insertRow(i, new Object[]{});
                tabla.setValueAt(reserva.get("id"), i, 0);
                tabla.setValueAt(reserva.get("codigo"), i, 1);
                tabla.setValueAt(reserva.get("detalle"), i, 2);
                tabla.setValueAt(reserva.get("valor"), i, 3);
                tabla.setValueAt(reserva.get("monto_aval"), i, 4);
                tabla.setValueAt(reserva.get("monto_acumulado"), i, 5);
                tabla.setValueAt(reserva.get("fecha_viaje"), i, 6);
                tabla.setValueAt(reserva.get("fecha_limite"), i, 7);
                tabla.setValueAt(reserva.get("participantes"), i, 8);
                tabla.setValueAt(reserva.get("estado"), i, 9);
            }
        }
        return tabla;
    }

    public static boolean registrarReserva(String codigo, String detalle, int valor, int monto_aval, int monto_acumulado, Date fecha_viaje, Date fecha_limite, int participantes) {
        String respuesta = Funciones.requestWS(Administrador.key, Reserva.bodyReserva(codigo, detalle, valor, monto_aval, monto_acumulado, fecha_viaje, fecha_limite, participantes), "reserva", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }

    private static String bodyReserva(String codigo, String detalle, int valor, int monto_aval, int monto_acumulado, Date fecha_viaje, Date fecha_limite, int participantes) {
        return "{"
                + "\"codigo\": \"" + codigo + "\","
                + "\"detalle\": \"" + detalle + "\","
                + "\"valor\": \"" + valor + "\","
                + "\"monto_aval\": \"" + monto_aval + "\","
                + "\"monto_acumulado\": \"" + monto_acumulado + "\","
                + "\"fecha_viaje\": \"" + fecha_viaje + "\","
                + "\"fecha_limite\": \"" + fecha_limite + "\","
                + "\"participantes\": \"" + participantes + "\""
                + "}";

    }
}
