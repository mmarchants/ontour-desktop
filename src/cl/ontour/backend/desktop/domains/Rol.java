package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Mariano Marchant S.
 */
public class Rol {
    
    // Atributos:
    
    private Long id;
    private String codigo;
    private String nombre;
    private boolean estado;

    // Constructores:
    
    public Rol() {
    }

    public Rol(Long id, String codigo, String nombre, boolean estado) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.estado = estado;
    }
    
    // Accesadores y mutadores:

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    // Métodos:

    @Override
    public String toString() {
        return this.getCodigo() + " - " + this.getNombre();
    }
    
    public static List<Rol> listadoRoles() {
        List<Rol> roles = new ArrayList<Rol>();
        String respuesta = Funciones.requestWS(Administrador.key, "", "rol", "GET");
        if (!respuesta.isEmpty()) {
            Rol rol;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {
                rol = new Rol();
                rol.setId(listado.getJSONObject(i).getLong("id"));
                rol.setCodigo(listado.getJSONObject(i).getString("codigo"));
                rol.setNombre(listado.getJSONObject(i).getString("nombre"));
                rol.setEstado(listado.getJSONObject(i).getBoolean("estado"));
                roles.add(rol);
            }
        }
        return roles;
    }
    
    public static Rol buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "rol/"+id, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Rol rol = new Rol();
            rol.setId(json.getLong("id"));
            rol.setCodigo(json.getString("codigo"));
            rol.setNombre(json.getString("nombre"));
            rol.setEstado(json.getBoolean("estado"));
            return rol;
        }
        Administrador.error = "ID ingresado no existe.";
        return null;
    }
    
}
