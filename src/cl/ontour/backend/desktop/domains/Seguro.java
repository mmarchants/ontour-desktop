package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Mariano Marchant S.
 */
public class Seguro {
    
    // Atributos:
    private Long id;
    private String codigo;
    private String nombre;
    private String detalle;
    private byte cobertura;
    private int valor;
    private Date fecha_inicio;
    private Date fecha_termino;
    private TipoSeguro tipo_seguro;
    private boolean estado;
    
    // Constructores:

    public Seguro() {
    }

    public Seguro(Long id, String codigo, String nombre, String detalle, byte cobertura, int valor, Date fecha_inicio, Date fecha_termino, TipoSeguro tipo_seguro, boolean estado) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.detalle = detalle;
        this.cobertura = cobertura;
        this.valor = valor;
        this.fecha_inicio = fecha_inicio;
        this.fecha_termino = fecha_termino;
        this.tipo_seguro = tipo_seguro;
        this.estado = estado;
    }
    
    // Accesadores y mutadores:

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public byte getCobertura() {
        return cobertura;
    }

    public void setCobertura(byte cobertura) {
        this.cobertura = cobertura;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_termino() {
        return fecha_termino;
    }

    public void setFecha_termino(Date fecha_termino) {
        this.fecha_termino = fecha_termino;
    }

    public TipoSeguro getTipo_seguro() {
        return tipo_seguro;
    }

    public void setTipo_seguro(TipoSeguro tipo_seguro) {
        this.tipo_seguro = tipo_seguro;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    // Métodos:

    @Override
    public String toString() {
        if (this.getCodigo() == null || this.getCodigo().isEmpty()) {
            return this.getNombre();
        } else {
            return this.getCodigo() + " - " + this.getNombre();
        }
    }
    
    public static List<Seguro> listadoSeguros() {
        List<Seguro> seguros = new ArrayList<Seguro>();
        String respuesta = Funciones.requestWS(Administrador.key, "", "seguro", "GET");
        if (!respuesta.isEmpty()) {
            Seguro seguro;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {
                seguro = new Seguro();
                SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
                seguro.setId(listado.getJSONObject(i).getLong("id"));
                seguro.setCodigo(listado.getJSONObject(i).getString("codigo"));
                seguro.setNombre(listado.getJSONObject(i).getString("nombre"));
                seguro.setDetalle(listado.getJSONObject(i).getString("detalle"));
                seguro.setCobertura((byte)listado.getJSONObject(i).getInt("cobertura"));
                seguro.setValor(listado.getJSONObject(i).getInt("valor"));
                try {
                    seguro.setFecha_inicio(formateador.parse(listado.getJSONObject(i).getString("fecha_inicio")));
                    seguro.setFecha_termino(formateador.parse(listado.getJSONObject(i).getString("fecha_termino")));
                } catch (ParseException ex) {
                    Logger.getLogger(Seguro.class.getName()).log(Level.SEVERE, null, ex);
                }
                seguro.setTipo_seguro(TipoSeguro.buscarPorId(String.valueOf(listado.getJSONObject(i).getJSONObject("tipo_seguro").getLong("id"))));
                seguro.setEstado(listado.getJSONObject(i).getBoolean("estado"));
                seguros.add(seguro);
            }
        }
        return seguros;
    }
    
    public static Seguro buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "seguro/"+id, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Seguro seguro = new Seguro();
            SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
            seguro.setId(json.getLong("id"));
            seguro.setCodigo(json.getString("codigo"));
            seguro.setNombre(json.getString("nombre"));
            seguro.setDetalle(json.getString("detalle"));
            seguro.setCobertura((byte)json.getInt("cobertura"));
            seguro.setValor(json.getInt("valor"));
            try {
                seguro.setFecha_inicio(formateador.parse(json.getString("fecha_inicio")));
                seguro.setFecha_termino(formateador.parse(json.getString("fecha_termino")));
            } catch (ParseException ex) {
                Logger.getLogger(Seguro.class.getName()).log(Level.SEVERE, null, ex);
            }
            seguro.setTipo_seguro(TipoSeguro.buscarPorId(String.valueOf(json.getJSONObject("tipo_seguro").getLong("id"))));
            seguro.setEstado(json.getBoolean("estado"));
            return seguro;
        }
        Administrador.error = "ID ingresado no existe.";
        return null;
    }
    
    public static Seguro buscarPorCodigo(String codigo) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "seguro/codigo/"+codigo, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Seguro seguro = new Seguro();
            SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
            seguro.setId(json.getLong("id"));
            seguro.setCodigo(json.getString("codigo"));
            seguro.setNombre(json.getString("nombre"));
            seguro.setDetalle(json.getString("detalle"));
            seguro.setCobertura((byte)json.getInt("cobertura"));
            seguro.setValor(json.getInt("valor"));
            try {
                seguro.setFecha_inicio(formateador.parse(json.getString("fecha_inicio")));
                seguro.setFecha_termino(formateador.parse(json.getString("fecha_termino")));
            } catch (ParseException ex) {
                Logger.getLogger(Seguro.class.getName()).log(Level.SEVERE, null, ex);
            }
            seguro.setTipo_seguro(TipoSeguro.buscarPorId(String.valueOf(json.getJSONObject("tipo_seguro").getLong("id"))));
            seguro.setEstado(json.getBoolean("estado"));
            return seguro;
        }
        Administrador.error = "Código ingresado no existe.";
        return null;
    }
    
    public static List<Seguro> solicitarOferta(int cobertura) {
        List<Seguro> seguros = new ArrayList<Seguro>();
        String respuesta = Funciones.requestWSSeguros(cobertura);
        if (!respuesta.isEmpty()) {
            Seguro seguro;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {
                seguro = new Seguro();
                SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
                seguro.setId(listado.getJSONObject(i).getLong("id"));
                seguro.setNombre(listado.getJSONObject(i).getString("nombre"));
                seguro.setDetalle(listado.getJSONObject(i).getString("detalle"));
                seguro.setCobertura((byte)listado.getJSONObject(i).getInt("cobertura"));
                seguro.setValor(listado.getJSONObject(i).getInt("valor"));
                try {
                    seguro.setFecha_inicio(formateador.parse(listado.getJSONObject(i).getString("fecha_inicio")));
                    seguro.setFecha_termino(formateador.parse(listado.getJSONObject(i).getString("fecha_termino")));
                } catch (ParseException ex) {
                    Logger.getLogger(Seguro.class.getName()).log(Level.SEVERE, null, ex);
                }
                seguros.add(seguro);
            }
        }
        return seguros;
    }
    
    public static boolean registrarSeguro(String nombre, String codigo, String detalle, String cobertura, String valor, String fecha_inicio, String fecha_termino, String tipo_seguro) {
        String respuesta = Funciones.requestWS(Administrador.key, bodySeguro(nombre, codigo, detalle, cobertura, valor, fecha_inicio, fecha_termino, tipo_seguro, valor), "seguro", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean modificarSeguro(String id, String nombre, String codigo, String detalle, String cobertura, String valor, String fecha_inicio, String fecha_termino, String tipo_seguro, String estado) {
        String respuesta = Funciones.requestWS(Administrador.key, bodySeguro(nombre, codigo, detalle, cobertura, valor, fecha_inicio, fecha_termino, tipo_seguro, valor), "seguro/"+id, "PUT");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean eliminarSeguro(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "seguro/"+id, "DELETE");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;           
    }
    
    public static String bodySeguro(String nombre, String codigo, String detalle, String cobertura, String valor, String fecha_inicio, String fecha_termino, String tipo_seguro, String estado) {
        return 
            "{" +
                "\"codigo\": \"" + codigo + "\"," +
                "\"nombre\": \"" + nombre + "\"," +
                "\"detalle\": \"" + detalle + "\"," +
                "\"cobertura\": " + cobertura + "," +
                "\"valor\": " + valor + "," +
                "\"fecha_inicio\": \"" + fecha_inicio + "\"," +
                "\"fecha_termino\": \"" + fecha_termino + "\"," +
                "\"tipo_seguro\": {" +
                    "\"id\": " + tipo_seguro + 
                "}," +
                "\"estado\": " + estado + 
            "}";
        
    }
}
