package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import javax.swing.table.DefaultTableModel;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Andrea
 */
public class Servicio {

    private int id_servicio;
    private String codigo_servicio;
    private String nombre_servicio;
    private String detalle_servicio;
    private String enlace_img;
    private int estado_servicio;

    public Servicio() {
    }

    public Servicio(int id_servicio, String codigo_servicio, String nombre_servicio, String detalle_servicio, String enlace_img, int estado_servicio) {
        this.id_servicio = id_servicio;
        this.codigo_servicio = codigo_servicio;
        this.nombre_servicio = nombre_servicio;
        this.detalle_servicio = detalle_servicio;
        this.enlace_img = enlace_img;
        this.estado_servicio = estado_servicio;
    }

    //GETERS
    public int getId_servicio() {
        return id_servicio;
    }

    public void setId_servicio(int id_servicio) {
        this.id_servicio = id_servicio;
    }

    public String getCodigo_servicio() {
        return codigo_servicio;
    }

    public void setCodigo_servicio(String codigo_servicio) {
        this.codigo_servicio = codigo_servicio;
    }

    public String getNombre_servicio() {
        return nombre_servicio;
    }

    public void setNombre_servicio(String nombre_servicio) {
        this.nombre_servicio = nombre_servicio;
    }

    public String getDetalle_servicio() {
        return detalle_servicio;
    }

    //SETTERS 
    public void setDetalle_servicio(String detalle_servicio) {
        this.detalle_servicio = detalle_servicio;
    }

    public String getEnlace_img() {
        return enlace_img;
    }

    public void setEnlace_img(String enlace_img) {
        this.enlace_img = enlace_img;
    }

    public int getEstado_servicio() {
        return estado_servicio;
    }

    public void setEstado_servicio(int estado_servicio) {
        this.estado_servicio = estado_servicio;
    }

    //METODOS
    @Override
    public String toString() {
        return "Servicio{" + "id_servicio=" + id_servicio + ", codigo_servicio=" + codigo_servicio + ", nombre_servicio=" + nombre_servicio + ", detalle_servicio=" + detalle_servicio + ", enlace_img=" + enlace_img + ", estado_servicio=" + estado_servicio + '}';
    }

    public static DefaultTableModel listarServicios(DefaultTableModel tabla) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "servicio", "GET");
        //System.out.println(respuesta);
        if (!respuesta.isEmpty()) {
            JSONArray listado = new JSONArray(respuesta);
            JSONObject servicio = new JSONObject();
            for (int i = 0; i < listado.length(); i++) {
                //System.out.println("País N°"+i+": "+listado.get(i));            
                servicio = listado.getJSONObject(i);
                tabla.insertRow(i, new Object[]{});
                tabla.setValueAt(servicio.get("id"), i, 0);
                tabla.setValueAt(servicio.get("codigo"), i, 1);
                tabla.setValueAt(servicio.get("nombre"), i, 2);
                tabla.setValueAt(servicio.get("detalle"), i, 3);
                tabla.setValueAt(servicio.get("enlace_imagen"), i, 4);
                tabla.setValueAt(servicio.get("estado"), i, 5);
            }
        }
        return tabla;
    }

    public static boolean registrarServicio(String codigo, String nombre, String detalle, String enlace_imgagen) {
        String respuesta = Funciones.requestWS(Administrador.key, Servicio.bodyServicio(codigo, nombre, detalle, enlace_imgagen), "servicio", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }

    private static String bodyServicio(String codigo, String nombre, String detalle, String enlace_imgagen) {
        return "{"
                + "\"codigo\": \"" + codigo + "\","
                + "\"codigo\": \"" + nombre + "\","
                + "\"codigo\": \"" + detalle + "\","
                + "\"nombre\": \"" + enlace_imgagen + "\""
                + "}";
    }
}
