package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author Mariano Marchant S.
 */
public class TipoSeguro {
    
    // Atributos:
    private Long id;
    private String codigo;
    private String nombre;
    private boolean estado;
    
    // Constructores:

    public TipoSeguro() {
    }

    public TipoSeguro(Long id, String codigo, String nombre, boolean estado) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.estado = estado;
    }
    
    // Accesadores y mutadores:

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    // Métodos:

    @Override
    public String toString() {
        return this.getCodigo() + " - " + this.getNombre();
    }
    
    public static List<TipoSeguro> listadoTiposSeguro() {
        List<TipoSeguro> tipos_seguro = new ArrayList<TipoSeguro>();
        String respuesta = Funciones.requestWS(Administrador.key, "", "tipo_seguro", "GET");
        if (!respuesta.isEmpty()) {
            TipoSeguro tipo_seguro;
            JSONArray listado = new JSONArray(respuesta);
            for (int i = 0; i < listado.length(); i++) {
                tipo_seguro = new TipoSeguro();
                tipo_seguro.setId(listado.getJSONObject(i).getLong("id"));
                tipo_seguro.setCodigo(listado.getJSONObject(i).getString("codigo"));
                tipo_seguro.setNombre(listado.getJSONObject(i).getString("nombre"));
                tipo_seguro.setEstado(listado.getJSONObject(i).getBoolean("estado"));
                tipos_seguro.add(tipo_seguro);
            }
        }
        return tipos_seguro;
    }    
    
    public static TipoSeguro buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "tipo_seguro/"+id, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            TipoSeguro tipo_seguro = new TipoSeguro();
            tipo_seguro.setId(json.getLong("id"));
            tipo_seguro.setCodigo(json.getString("codigo"));
            tipo_seguro.setNombre(json.getString("nombre"));
            tipo_seguro.setEstado(json.getBoolean("estado"));
            return tipo_seguro;
        }
        Administrador.error = "ID ingresado no existe.";
        return null;
    }
}
