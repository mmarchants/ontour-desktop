package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.util.Date;
import javax.swing.table.DefaultTableModel;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Andrea
 */
public class Traslado {

    private int id_traslado;
    private String codigo_traslado;
    private String nombre_traslado;
    private String detalle_traslado;
    private Date fecha_partida;
    private Date fecha_llegada;
    private int estado_contrato;

    public Traslado() {
    }

    public Traslado(int id_traslado, String codigo_traslado, String nombre_traslado, String detalle_traslado, Date fecha_partida, Date fecha_llegada, int estado_contrato) {
        this.id_traslado = id_traslado;
        this.codigo_traslado = codigo_traslado;
        this.nombre_traslado = nombre_traslado;
        this.detalle_traslado = detalle_traslado;
        this.fecha_partida = fecha_partida;
        this.fecha_llegada = fecha_llegada;
        this.estado_contrato = estado_contrato;
    }

    //GETERS
    public int getId_traslado() {
        return id_traslado;
    }

    public String getCodigo_traslado() {
        return codigo_traslado;
    }

    public String getNombre_traslado() {
        return nombre_traslado;
    }

    public String getDetalle_traslado() {
        return detalle_traslado;
    }

    public Date getFecha_partida() {
        return fecha_partida;
    }

    public Date getFecha_llegada() {
        return fecha_llegada;
    }

    public int getEstado_contrato() {
        return estado_contrato;
    }

    public void setId_traslado(int id_traslado) {
        this.id_traslado = id_traslado;
    }

    //SETTERS
    public void setCodigo_traslado(String codigo_traslado) {
        this.codigo_traslado = codigo_traslado;
    }

    public void setNombre_traslado(String nombre_traslado) {
        this.nombre_traslado = nombre_traslado;
    }

    public void setDetalle_traslado(String detalle_traslado) {
        this.detalle_traslado = detalle_traslado;
    }

    public void setFecha_partida(Date fecha_partida) {
        this.fecha_partida = fecha_partida;
    }

    public void setFecha_llegada(Date fecha_llegada) {
        this.fecha_llegada = fecha_llegada;
    }

    public void setEstado_contrato(int estado_contrato) {
        this.estado_contrato = estado_contrato;
    }

    //METODOS    
    @Override
    public String toString() {
        return "Traslado{" + "id_traslado=" + id_traslado + ", codigo_traslado=" + codigo_traslado + ", nombre_traslado=" + nombre_traslado + ", detalle_traslado=" + detalle_traslado + ", fecha_partida=" + fecha_partida + ", fecha_llegada=" + fecha_llegada + ", estado_contrato=" + estado_contrato + '}';
    }

    public static DefaultTableModel listarTraslados(DefaultTableModel tabla) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "traslado", "GET");
        //System.out.println(respuesta);
        if (!respuesta.isEmpty()) {
            JSONArray listado = new JSONArray(respuesta);
            JSONObject traslado = new JSONObject();
            for (int i = 0; i < listado.length(); i++) {
                //System.out.println("Traslado N°"+i+": "+listado.get(i));            
                traslado = listado.getJSONObject(i);
                tabla.insertRow(i, new Object[]{});
                tabla.setValueAt(traslado.get("id"), i, 0);
                tabla.setValueAt(traslado.get("codigo"), i, 1);
                tabla.setValueAt(traslado.get("nombre"), i, 2);
                tabla.setValueAt(traslado.get("detalle"), i, 3);
                tabla.setValueAt(traslado.get("fecha_partida"), i, 4);
                tabla.setValueAt(traslado.get("fecha_llegada"), i, 5);
                tabla.setValueAt(traslado.get("estado"), i, 6);
            }
        }
        return tabla;
    }

    public static boolean registrarTraslado(String codigo, String nombre, String detalle, Date fecha_partida, Date fecha_llegada) {
        String respuesta = Funciones.requestWS(Administrador.key, Traslado.bodyTraslado(codigo, nombre, detalle, fecha_partida, fecha_llegada), "traslado", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }

    private static String bodyTraslado(String codigo, String nombre, String detalle, Date fecha_partida, Date fecha_llegada) {
        return "{"
                + "\"codigo\": \"" + codigo + "\","
                + "\"nombre\": \"" + nombre + "\","
                + "\"detalle\": \"" + detalle + "\","
                + "\"fecha_partida\": \"" + fecha_partida + "\","
                + "\"fecha_llegada\": \"" + fecha_llegada + "\""
                + "}";
    }
}
