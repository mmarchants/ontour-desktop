package cl.ontour.backend.desktop.domains;

import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import org.json.JSONObject;

/**
 * @author Mariano Marchant S.
 */
public class Usuario {
    
    // Atributos:
    
    private Long id;
    private String correo_electronico;
    private String contrasegna;
    private boolean estado;
    private Rol rol;
    
    // Constructores:

    public Usuario() {
    }

    public Usuario(Long id, String correo_electronico, String contrasegna, boolean estado, Rol rol) {
        this.id = id;
        this.correo_electronico = correo_electronico;
        this.contrasegna = contrasegna;
        this.estado = estado;
        this.rol = rol;
    }
    
    // Accesadores y mutadores:

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCorreo_electronico() {
        return correo_electronico;
    }

    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    public String getContrasegna() {
        return contrasegna;
    }

    public void setContrasegna(String contrasegna) {
        this.contrasegna = contrasegna;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }
    
    // Métodos:

    @Override
    public String toString() {
        return this.getCorreo_electronico();
    }
    
    public static Usuario buscarPorId(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "usuario/"+id, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Usuario usuario = new Usuario();
            usuario.setId(json.getLong("id"));
            usuario.setCorreo_electronico(json.getString("correo_electronico"));
            usuario.setContrasegna(json.getString("contrasegna"));
            usuario.setEstado(json.getBoolean("estado"));
            usuario.setRol(Rol.buscarPorId(String.valueOf(json.getJSONObject("rol").getLong("id"))));
            return usuario;
        }
        Administrador.error = "ID ingresado no existe.";
        return null;                
    }
    
    public static Usuario buscarPorCorreo(String correo_electronico) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "usuario/user/"+correo_electronico, "GET");
        if (!respuesta.isEmpty()) {            
            JSONObject json = new JSONObject(respuesta);
            Usuario usuario = new Usuario();
            usuario.setId(json.getLong("id"));
            usuario.setCorreo_electronico(json.getString("correo_electronico"));
            usuario.setContrasegna(json.getString("contrasegna"));
            usuario.setEstado(json.getBoolean("estado"));
            usuario.setRol(Rol.buscarPorId(String.valueOf(json.getJSONObject("rol").getLong("id"))));
            return usuario;
        }
        return null;   
    }
    
    public static boolean registrarUsuario(String correo_electronico, String contrasegna, Rol rol) {
        String respuesta = Funciones.requestWS(Administrador.key, Usuario.bodyUsuario(correo_electronico, contrasegna, rol, "true"), "usuario", "POST");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean eliminarUsuario(String id) {
        String respuesta = Funciones.requestWS(Administrador.key, "", "usuario/"+id, "DELETE");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    public static boolean modificarUsuario(String id, String correo_electronico, String contrasegna, Rol rol, String estado) {
        String respuesta = Funciones.requestWS(Administrador.key, bodyUsuario(correo_electronico, contrasegna, rol, estado), "usuario/"+id, "PUT");
        if (respuesta.equals("Ok")) {
            return true;
        }
        return false;
    }
    
    private static String bodyUsuario(String correo_electronico, String contrasegna, Rol rol, String estado) {
        return 
            "{" +
                "\"correo_electronico\": \"" + correo_electronico + "\"," +
                "\"contrasegna\": \"" + contrasegna + "\"," + 
                "\"estado\": " + estado + "," +
                "\"rol\": {" +
                    "\"id\": " + rol.getId() +
                "}" +
            "}";                        
    }
}
