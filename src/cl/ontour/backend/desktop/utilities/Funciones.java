package cl.ontour.backend.desktop.utilities;

import cl.ontour.frontend.desktop.Administrador;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Funciones {
    
    public static SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
    
    /**
     * Método que realiza solicitud a servicio web.
     * @param body Cuerpo de solicitud.
     * @param extension Extensión de URL de solicitud.
     * @param method Método de solicitud.
     * @return Respuesta de solicitud.
     */
    public static String requestWS(String key, String body, String extension, String method) {
        StringBuilder response = new StringBuilder();
        // System.out.println("Key: " + key + "|\nBody: + " + body + "|\nExtensión: " + extension + "|\nMétodo: " + method + "|");
        try {            
            HttpURLConnection conn = (HttpURLConnection) (new URL("http://localhost:8080/"+extension).openConnection()); // Para trabajar local.
        // HttpURLConnection conn = (HttpURLConnection) (new URL("http://ontour2018.us-west-2.elasticbeanstalk.com/"+extension).openConnection());
            conn.setDoOutput(true);
            conn.setRequestMethod(method);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("cache-control", "no-cache");
            conn.setRequestProperty("Accept", "*/*");
            conn.setRequestProperty("accept-encoding", "gzip, deflate");
            conn.setRequestProperty("Transfer-Encoding", "chunked");
            if (!key.isEmpty()) {
                conn.setRequestProperty("Authorization", "Bearer " + key);                                
            }
            if (!body.isEmpty()) {
                DataOutputStream output = new DataOutputStream(conn.getOutputStream());
                output.writeBytes(body);
                output.flush();
            }                 
            if (conn.getResponseCode() == 200) {
                InputStream input = conn.getInputStream();
                BufferedReader bf = new BufferedReader(new InputStreamReader(input, "UTF-8"));
                String line;
                while ((line = bf.readLine()) != null) { response.append(line); } // Leyendo respuesta.                
            } else {
                /*
                Map<String, List<String>> map = conn.getHeaderFields(); 
                System.out.println("Response Header:");
                for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                    System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue());
                }
                */
                System.out.println("Error: " + conn.getResponseCode());
                switch (conn.getResponseCode()) {
                    case 400:
                        // Bad Request
                        Administrador.error = "Error en formato de solicitud.";
                        System.out.println("JSON: " + body);
                        break;
                    case 401:
                        // Bad Credencials
                        Administrador.error = "Credenciales incorrectas.";
                        break;
                    case 403:
                        // Forbidden
                        Administrador.error = "Sesión no autenticada. Inicie sesión e intente nuevamente.";
                        break;
                    case 404:
                        // Not Found
                        Administrador.error = "Recurso no encontrado.";
                        break;
                    case 405:
                        // Method Not Allowed
                        Administrador.error = "Solicitud no permitida.";
                        break;
                    case 408:
                        // Request Timeout
                        Administrador.error = "Tiempo de espera agotado para solicitud.";
                        break;
                    case 500:
                        // Server error
                        Administrador.error = "Error interno del servidor.";
                        break;
                }                
            }
        } catch (MalformedURLException url_ex) {
            System.out.println("Error 1");
            Logger.getLogger(Funciones.class.getName()).log(Level.SEVERE, null, url_ex);
        } catch (IOException io_ex) {
            System.out.println("Error 2");
            Logger.getLogger(Funciones.class.getName()).log(Level.SEVERE, null, io_ex);
        } catch (Exception ex) {
            System.out.println("Error 3");
            Logger.getLogger(Funciones.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return response.toString();
    }
    
    /**
     * Función para activar contenedores.
     * @param container Contenedor a activar/desactivar.
     * @param enable Boolean que determina activación.
     */
    public static void activarComponentes(Container container, boolean enable) {
        Component[] components = container.getComponents();
        for (Component component : components) {
            component.setEnabled(enable);
            if (component instanceof Container) {
                activarComponentes((Container) component, enable);
            }
        }
    }
    
    /**
     * Método que realiza solicitud a servicio web de seguros.
     * @return Respuesta de solicitud.
     */
    public static String requestWSSeguros(int cobertura) {
        StringBuilder response = new StringBuilder();
        // System.out.println("Key: " + key + "|\nBody: + " + body + "|\nExtensión: " + extension + "|\nMétodo: " + method + "|");
        try {            
            HttpURLConnection conn = (HttpURLConnection) (new URL("http://localhost:8081/seguros-ws/oferta/"+cobertura).openConnection());
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("cache-control", "no-cache");
            conn.setRequestProperty("Accept", "*/*");
            conn.setRequestProperty("accept-encoding", "gzip, deflate");
            conn.setRequestProperty("Transfer-Encoding", "chunked");
            if (conn.getResponseCode() == 200) {
                InputStream input = conn.getInputStream();
                BufferedReader bf = new BufferedReader(new InputStreamReader(input, "UTF-8"));
                String line;
                while ((line = bf.readLine()) != null) { response.append(line); } // Leyendo respuesta.                
            } else {
                /*
                Map<String, List<String>> map = conn.getHeaderFields(); 
                System.out.println("Response Header:");
                for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                    System.out.println("Key: " + entry.getKey() + ", Value: " + entry.getValue());
                }
                */
                System.out.println("Error: " + conn.getResponseCode());
                switch (conn.getResponseCode()) {
                    case 400:
                        // Bad Request
                        Administrador.error = "Error en formato de solicitud.";
                        break;
                    case 401:
                        // Bad Credencials
                        Administrador.error = "Credenciales incorrectas.";
                        break;
                    case 403:
                        // Forbidden
                        Administrador.error = "Sesión no autenticada. Inicie sesión e intente nuevamente.";
                        break;
                    case 404:
                        // Not Found
                        Administrador.error = "Recurso no encontrado.";
                        break;
                    case 405:
                        // Method Not Allowed
                        Administrador.error = "Solicitud no permitida.";
                        break;
                    case 408:
                        // Request Timeout
                        Administrador.error = "Tiempo de espera agotado para solicitud.";
                        break;
                    case 500:
                        // Server error
                        Administrador.error = "Error interno del servidor.";
                        break;
                }                
            }
        } catch (MalformedURLException url_ex) {
            System.out.println("Error 1");
            Logger.getLogger(Funciones.class.getName()).log(Level.SEVERE, null, url_ex);
        } catch (IOException io_ex) {
            System.out.println("Error 2");
            Logger.getLogger(Funciones.class.getName()).log(Level.SEVERE, null, io_ex);
        } catch (Exception ex) {
            System.out.println("Error 3");
            Logger.getLogger(Funciones.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return response.toString();
    }
}
