package cl.ontour.frontend.desktop.views.create;

/**
 *
 * @author Andrea
 */
public class RegistrarAlumno extends javax.swing.JFrame {

    public RegistrarAlumno() {
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelAlumno = new javax.swing.JPanel();
        lblRutAlumno = new javax.swing.JLabel();
        txtRutAlumno = new javax.swing.JTextField();
        lblNombreAlumno = new javax.swing.JLabel();
        txtNombreAlumno = new javax.swing.JTextField();
        jComboBoxCurso = new javax.swing.JComboBox();
        txtApe1Alumno = new javax.swing.JTextField();
        lblApe1Alumno = new javax.swing.JLabel();
        txtApe2Alumno = new javax.swing.JTextField();
        lblApe2Alumno = new javax.swing.JLabel();
        txtEmailAlumno = new javax.swing.JTextField();
        lblEmailAlumno = new javax.swing.JLabel();
        jComboBoxApoderado = new javax.swing.JComboBox();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        BtnRegistrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelAlumno.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ingrese un alumno"));

        lblRutAlumno.setText("Rut");

        lblNombreAlumno.setText("Nombre");

        jComboBoxCurso.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Curso", "--", "--" }));

        lblApe1Alumno.setText("Primer apellido");

        txtApe2Alumno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtApe2AlumnoActionPerformed(evt);
            }
        });

        lblApe2Alumno.setText("Segunto apellido");

        lblEmailAlumno.setText("Email");

        jComboBoxApoderado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Apoderado", "--", "--" }));

        org.jdesktop.layout.GroupLayout jPanelAlumnoLayout = new org.jdesktop.layout.GroupLayout(jPanelAlumno);
        jPanelAlumno.setLayout(jPanelAlumnoLayout);
        jPanelAlumnoLayout.setHorizontalGroup(
            jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelAlumnoLayout.createSequentialGroup()
                .add(37, 37, 37)
                .add(jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanelAlumnoLayout.createSequentialGroup()
                            .add(lblApe1Alumno)
                            .add(18, 18, 18)
                            .add(txtApe1Alumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 561, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jPanelAlumnoLayout.createSequentialGroup()
                                .add(lblApe2Alumno)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(txtApe2Alumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 561, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanelAlumnoLayout.createSequentialGroup()
                                .add(lblEmailAlumno)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .add(txtEmailAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 561, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanelAlumnoLayout.createSequentialGroup()
                                .add(jComboBoxApoderado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 229, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(83, 83, 83)
                                .add(jComboBoxCurso, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 141, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelAlumnoLayout.createSequentialGroup()
                        .add(jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(lblRutAlumno)
                            .add(lblNombreAlumno))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 65, Short.MAX_VALUE)
                        .add(jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(txtNombreAlumno, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 561, Short.MAX_VALUE)
                            .add(txtRutAlumno))))
                .addContainerGap(64, Short.MAX_VALUE))
        );
        jPanelAlumnoLayout.setVerticalGroup(
            jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelAlumnoLayout.createSequentialGroup()
                .add(44, 44, 44)
                .add(jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblRutAlumno)
                    .add(txtRutAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblNombreAlumno)
                    .add(txtNombreAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblApe1Alumno)
                    .add(txtApe1Alumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblApe2Alumno)
                    .add(txtApe2Alumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(lblEmailAlumno)
                    .add(txtEmailAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboBoxApoderado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jComboBoxCurso, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(63, Short.MAX_VALUE))
        );

        btnLimpiar.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        BtnRegistrar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnRegistrar.setText("Registrar");
        BtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegistrarActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(79, 79, 79)
                .add(jPanelAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(103, Short.MAX_VALUE))
            .add(layout.createSequentialGroup()
                .add(50, 50, 50)
                .add(btnCancelar)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 559, Short.MAX_VALUE)
                .add(btnLimpiar)
                .add(18, 18, 18)
                .add(BtnRegistrar)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(81, 81, 81)
                .add(jPanelAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(30, 30, 30)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(btnCancelar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(1, 1, 1)
                        .add(btnLimpiar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 56, Short.MAX_VALUE))
                    .add(BtnRegistrar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 57, Short.MAX_VALUE))
                .add(44, 44, 44))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    limpiaCampos();
}//GEN-LAST:event_btnLimpiarActionPerformed

private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
    dispose();

}//GEN-LAST:event_btnCancelarActionPerformed

private void BtnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegistrarActionPerformed


}//GEN-LAST:event_BtnRegistrarActionPerformed

private void txtApe2AlumnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtApe2AlumnoActionPerformed
// TODO add your handling code here:
}//GEN-LAST:event_txtApe2AlumnoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarAlumno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarAlumno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarAlumno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarAlumno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new RegistrarAlumno().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnRegistrar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JComboBox jComboBoxApoderado;
    private javax.swing.JComboBox jComboBoxCurso;
    private javax.swing.JPanel jPanelAlumno;
    private javax.swing.JLabel lblApe1Alumno;
    private javax.swing.JLabel lblApe2Alumno;
    private javax.swing.JLabel lblEmailAlumno;
    private javax.swing.JLabel lblNombreAlumno;
    private javax.swing.JLabel lblRutAlumno;
    private javax.swing.JTextField txtApe1Alumno;
    private javax.swing.JTextField txtApe2Alumno;
    private javax.swing.JTextField txtEmailAlumno;
    private javax.swing.JTextField txtNombreAlumno;
    private javax.swing.JTextField txtRutAlumno;
    // End of variables declaration//GEN-END:variables

    public void limpiaCampos() {
    }
}
