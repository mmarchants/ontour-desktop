package cl.ontour.frontend.desktop.views.create;

import cl.ontour.backend.desktop.domains.Ciudad;
import cl.ontour.backend.desktop.domains.Pais;
import cl.ontour.frontend.desktop.Administrador;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Andrea
 */
public class RegistrarCiudad extends javax.swing.JFrame {

    public RegistrarCiudad() {
        initComponents();
        setLocationRelativeTo(null);
        cargarPaises();
    }
    
    private void cargarPaises() {
        List<Pais> paises = Pais.listadoPaises();
        for (int i = 0; i < paises.size(); i++) {
            jComboBoxPais.addItem(paises.get(i));            
        }
    }
    
    private void limpiarCampos() {
        txtCodigoCiudad.setText("");
        txtNombreCiudad.setText("");
    }
    
    private boolean validaCampos() {
        if (txtCodigoCiudad.getText().isEmpty() || txtNombreCiudad.getText().isEmpty()) {
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnLimpiar = new javax.swing.JButton();
        BtnRegistrar = new javax.swing.JButton();
        jPanelCiudad = new javax.swing.JPanel();
        txtCodigoCiudad = new javax.swing.JTextField();
        lblCodigoCiudad = new javax.swing.JLabel();
        lblNombreCiudad = new javax.swing.JLabel();
        txtNombreCiudad = new javax.swing.JTextField();
        jComboBoxPais = new javax.swing.JComboBox();
        lblPais = new javax.swing.JLabel();
        BtnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Registr de Ciudades");

        btnLimpiar.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        BtnRegistrar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnRegistrar.setText("Registrar");
        BtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegistrarActionPerformed(evt);
            }
        });

        jPanelCiudad.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ingrese una ciudad"));
        jPanelCiudad.setPreferredSize(new java.awt.Dimension(1200, 275));

        lblCodigoCiudad.setText("Código:");

        lblNombreCiudad.setText("Nombre");

        lblPais.setText("Pais");

        org.jdesktop.layout.GroupLayout jPanelCiudadLayout = new org.jdesktop.layout.GroupLayout(jPanelCiudad);
        jPanelCiudad.setLayout(jPanelCiudadLayout);
        jPanelCiudadLayout.setHorizontalGroup(
            jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelCiudadLayout.createSequentialGroup()
                .add(52, 52, 52)
                .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelCiudadLayout.createSequentialGroup()
                        .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(lblNombreCiudad)
                            .add(lblPais))
                        .add(18, 18, 18)
                        .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jComboBoxPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 314, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(txtNombreCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 447, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(jPanelCiudadLayout.createSequentialGroup()
                        .add(lblCodigoCiudad)
                        .add(27, 27, 27)
                        .add(txtCodigoCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 210, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(151, Short.MAX_VALUE))
        );
        jPanelCiudadLayout.setVerticalGroup(
            jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelCiudadLayout.createSequentialGroup()
                .addContainerGap(63, Short.MAX_VALUE)
                .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblCodigoCiudad)
                    .add(txtCodigoCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jComboBoxPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblPais))
                .add(18, 18, 18)
                .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblNombreCiudad)
                    .add(txtNombreCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(79, 79, 79))
        );

        BtnCancelar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnCancelar.setText("Volver");
        BtnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCancelarActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(44, 44, 44)
                .add(BtnCancelar)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 396, Short.MAX_VALUE)
                .add(btnLimpiar)
                .add(43, 43, 43)
                .add(BtnRegistrar)
                .add(102, 102, 102))
            .add(layout.createSequentialGroup()
                .add(76, 76, 76)
                .add(jPanelCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 723, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(103, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(65, 65, 65)
                .add(jPanelCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(60, 60, 60)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnLimpiar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(BtnRegistrar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(BtnCancelar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(78, 78, 78))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    limpiarCampos();
}//GEN-LAST:event_btnLimpiarActionPerformed

private void BtnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegistrarActionPerformed
    if (!validaCampos()) {
        JOptionPane.showMessageDialog(null, "Ingrese datos faltantes.");
    } else if (Ciudad.registrarCiudad(txtCodigoCiudad.getText(), txtNombreCiudad.getText(), (Pais) jComboBoxPais.getSelectedItem())) {
        JOptionPane.showMessageDialog(null, "Registro exitoso.");
        limpiarCampos();
    } else if (!Administrador.error.isEmpty()) {
        JOptionPane.showMessageDialog(null, Administrador.error);
        Administrador.error = "";
    } else {
        JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
    }
}//GEN-LAST:event_BtnRegistrarActionPerformed

    private void BtnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_BtnCancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new RegistrarCiudad().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCancelar;
    private javax.swing.JButton BtnRegistrar;
    private javax.swing.JButton btnLimpiar;
    public static javax.swing.JComboBox jComboBoxPais;
    private javax.swing.JPanel jPanelCiudad;
    private javax.swing.JLabel lblCodigoCiudad;
    private javax.swing.JLabel lblNombreCiudad;
    private javax.swing.JLabel lblPais;
    private javax.swing.JTextField txtCodigoCiudad;
    private javax.swing.JTextField txtNombreCiudad;
    // End of variables declaration//GEN-END:variables

}
