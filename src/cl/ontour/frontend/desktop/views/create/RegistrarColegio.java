package cl.ontour.frontend.desktop.views.create;

import cl.ontour.backend.desktop.domains.Ciudad;
import cl.ontour.backend.desktop.domains.Colegio;
import cl.ontour.frontend.desktop.Administrador;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Andrea
 */
public class RegistrarColegio extends javax.swing.JFrame {

    public RegistrarColegio() {
        initComponents();
        setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/cl/ontour/frontend/desktop/resources/images/logo_ontour_icon.png")));
        cargarCiudades();
    }

    private void cargarCiudades() {
        List<Ciudad> ciudades = Ciudad.listadoCiudades();
        for (int i = 0; i < ciudades.size(); i++) {
            jComboBoxCiudad.addItem(ciudades.get(i));            
        }        
    }
    
    private void limpiaCampos() {
        txtCodigoColegio.setText("");
        txtNombreColegio.setText("");
        txtDireccionColegio.setText("");
        txtTelefonoColegio.setText("");
        txtEmailColegio.setText("");
    }
    
    private boolean validaCampos() {
        if (txtCodigoColegio.getText().isEmpty() || txtNombreColegio.getText().isEmpty() || txtDireccionColegio.getText().isEmpty()) {
            return false;
        }
        return true;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelColegio = new javax.swing.JPanel();
        lblCodigoColegio = new javax.swing.JLabel();
        txtCodigoColegio = new javax.swing.JTextField();
        lblNombreColegio = new javax.swing.JLabel();
        txtNombreColegio = new javax.swing.JTextField();
        txtDireccionColegio = new javax.swing.JTextField();
        lblDireccionColegio = new javax.swing.JLabel();
        txtTelefonoColegio = new javax.swing.JTextField();
        lblTelefonoColegio = new javax.swing.JLabel();
        txtEmailColegio = new javax.swing.JTextField();
        lblEmailColegio = new javax.swing.JLabel();
        jComboBoxCiudad = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        BtnRegistrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Registro de colegios");

        jPanelColegio.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ingrese un colegio"));

        lblCodigoColegio.setText("Código:");

        lblNombreColegio.setText("Nombre:");

        lblDireccionColegio.setText("Dirección:");

        lblTelefonoColegio.setText("Teléfono:");

        lblEmailColegio.setText("Correo electrónico:");

        jLabel1.setText("Ciudad:");

        org.jdesktop.layout.GroupLayout jPanelColegioLayout = new org.jdesktop.layout.GroupLayout(jPanelColegio);
        jPanelColegio.setLayout(jPanelColegioLayout);
        jPanelColegioLayout.setHorizontalGroup(
            jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelColegioLayout.createSequentialGroup()
                .add(37, 37, 37)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanelColegioLayout.createSequentialGroup()
                            .add(lblDireccionColegio)
                            .add(486, 486, 486))
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, txtDireccionColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 561, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(lblTelefonoColegio)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelColegioLayout.createSequentialGroup()
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 109, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(txtTelefonoColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 561, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(lblEmailColegio)
                                .add(jPanelColegioLayout.createSequentialGroup()
                                    .add(jLabel1)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(jComboBoxCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 229, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(txtEmailColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 561, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelColegioLayout.createSequentialGroup()
                        .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(lblCodigoColegio)
                            .add(lblNombreColegio))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(txtNombreColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 561, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(txtCodigoColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 561, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(80, Short.MAX_VALUE))
        );
        jPanelColegioLayout.setVerticalGroup(
            jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelColegioLayout.createSequentialGroup()
                .add(44, 44, 44)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblCodigoColegio)
                    .add(txtCodigoColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtNombreColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblNombreColegio))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtDireccionColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblDireccionColegio))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtTelefonoColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblTelefonoColegio))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtEmailColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblEmailColegio))
                .add(18, 18, 18)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboBoxCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel1))
                .add(68, 68, 68))
        );

        btnLimpiar.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        BtnRegistrar.setBackground(new java.awt.Color(153, 153, 153));
        BtnRegistrar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnRegistrar.setText("Registrar");
        BtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegistrarActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(53, 53, 53)
                .add(btnCancelar)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 531, Short.MAX_VALUE)
                .add(btnLimpiar)
                .add(18, 18, 18)
                .add(BtnRegistrar)
                .add(50, 50, 50))
            .add(layout.createSequentialGroup()
                .add(80, 80, 80)
                .add(jPanelColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(82, 82, 82)
                .add(jPanelColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(36, 36, 36)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(btnCancelar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(1, 1, 1)
                        .add(btnLimpiar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE))
                    .add(BtnRegistrar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 52, Short.MAX_VALUE))
                .add(36, 36, 36))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    limpiaCampos();
}//GEN-LAST:event_btnLimpiarActionPerformed

private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
    dispose();
}//GEN-LAST:event_btnCancelarActionPerformed

private void BtnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegistrarActionPerformed
    if (!validaCampos()) {
        JOptionPane.showMessageDialog(null, "Ingrese datos faltantes.");
    } else if (Colegio.registrarColegio(txtCodigoColegio.getText(), txtNombreColegio.getText(), txtDireccionColegio.getText(), txtTelefonoColegio.getText(), txtEmailColegio.getText(), (Ciudad) jComboBoxCiudad.getSelectedItem())) {
        JOptionPane.showMessageDialog(null, "Registro exitoso.");
        limpiaCampos();
    } else if (!Administrador.error.isEmpty()) {
        JOptionPane.showMessageDialog(null, Administrador.error);
        Administrador.error = "";
    } else {
        JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
    }
}//GEN-LAST:event_BtnRegistrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new RegistrarColegio().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnRegistrar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnLimpiar;
    public static javax.swing.JComboBox jComboBoxCiudad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelColegio;
    private javax.swing.JLabel lblCodigoColegio;
    private javax.swing.JLabel lblDireccionColegio;
    private javax.swing.JLabel lblEmailColegio;
    private javax.swing.JLabel lblNombreColegio;
    private javax.swing.JLabel lblTelefonoColegio;
    private javax.swing.JTextField txtCodigoColegio;
    private javax.swing.JTextField txtDireccionColegio;
    private javax.swing.JTextField txtEmailColegio;
    private javax.swing.JTextField txtNombreColegio;
    private javax.swing.JTextField txtTelefonoColegio;
    // End of variables declaration//GEN-END:variables

}
