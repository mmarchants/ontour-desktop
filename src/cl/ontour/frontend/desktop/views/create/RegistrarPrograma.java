package cl.ontour.frontend.desktop.views.create;

/**
 *
 * @author Andrea
 */
public class RegistrarPrograma extends javax.swing.JFrame {

    public RegistrarPrograma() {
        initComponents();

        setLocationRelativeTo(null);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        BtnRegistrar = new javax.swing.JButton();
        jPanelPrograma = new javax.swing.JPanel();
        lblCodigoPrograma = new javax.swing.JLabel();
        txtCodigoPrograma = new javax.swing.JTextField();
        lblNombrePrograma = new javax.swing.JLabel();
        lblDescripcionPrograma = new javax.swing.JLabel();
        txtNombrePrograma = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescripcionPrograma = new javax.swing.JTextArea();
        lblValorPrograma = new javax.swing.JLabel();
        txtValorPrograma = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnLimpiar.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar.setText("Volver");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        BtnRegistrar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnRegistrar.setText("Registrar");
        BtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegistrarActionPerformed(evt);
            }
        });

        jPanelPrograma.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ingrese un programa"));

        lblCodigoPrograma.setText("Código:");

        lblNombrePrograma.setText("Nombre");

        lblDescripcionPrograma.setText("Descripcion");

        txtDescripcionPrograma.setColumns(20);
        txtDescripcionPrograma.setRows(5);
        jScrollPane1.setViewportView(txtDescripcionPrograma);

        lblValorPrograma.setText("Valor");

        org.jdesktop.layout.GroupLayout jPanelProgramaLayout = new org.jdesktop.layout.GroupLayout(jPanelPrograma);
        jPanelPrograma.setLayout(jPanelProgramaLayout);
        jPanelProgramaLayout.setHorizontalGroup(
            jPanelProgramaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelProgramaLayout.createSequentialGroup()
                .addContainerGap(64, Short.MAX_VALUE)
                .add(jPanelProgramaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelProgramaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(jPanelProgramaLayout.createSequentialGroup()
                            .add(jPanelProgramaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(lblDescripcionPrograma)
                                .add(lblValorPrograma))
                            .add(18, 18, 18)
                            .add(jPanelProgramaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jPanelProgramaLayout.createSequentialGroup()
                                    .add(txtValorPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 213, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .add(jScrollPane1)))
                        .add(jPanelProgramaLayout.createSequentialGroup()
                            .add(lblNombrePrograma)
                            .add(41, 41, 41)
                            .add(txtNombrePrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 561, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(jPanelProgramaLayout.createSequentialGroup()
                        .add(38, 38, 38)
                        .add(lblCodigoPrograma)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(txtCodigoPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 210, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(58, 58, 58))
        );
        jPanelProgramaLayout.setVerticalGroup(
            jPanelProgramaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelProgramaLayout.createSequentialGroup()
                .add(26, 26, 26)
                .add(jPanelProgramaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblCodigoPrograma)
                    .add(txtCodigoPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(27, 27, 27)
                .add(jPanelProgramaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblNombrePrograma)
                    .add(txtNombrePrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelProgramaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(lblDescripcionPrograma)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(33, 33, 33)
                .add(jPanelProgramaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtValorPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblValorPrograma))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(102, 102, 102)
                .add(jPanelPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(85, Short.MAX_VALUE))
            .add(layout.createSequentialGroup()
                .add(73, 73, 73)
                .add(btnCancelar)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(btnLimpiar)
                .add(18, 18, 18)
                .add(BtnRegistrar)
                .add(57, 57, 57))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(71, 71, 71)
                .add(jPanelPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 23, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnCancelar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(BtnRegistrar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(btnLimpiar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(72, 72, 72))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    limpiaCampos();
}//GEN-LAST:event_btnLimpiarActionPerformed

private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
    dispose();

}//GEN-LAST:event_btnCancelarActionPerformed

private void BtnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegistrarActionPerformed


}//GEN-LAST:event_BtnRegistrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarPrograma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarPrograma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarPrograma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarPrograma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new RegistrarPrograma().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnRegistrar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JPanel jPanelPrograma;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCodigoPrograma;
    private javax.swing.JLabel lblDescripcionPrograma;
    private javax.swing.JLabel lblNombrePrograma;
    private javax.swing.JLabel lblValorPrograma;
    private javax.swing.JTextField txtCodigoPrograma;
    private javax.swing.JTextArea txtDescripcionPrograma;
    private javax.swing.JTextField txtNombrePrograma;
    private javax.swing.JTextField txtValorPrograma;
    // End of variables declaration//GEN-END:variables

    private void limpiaCampos() {
    }

}
