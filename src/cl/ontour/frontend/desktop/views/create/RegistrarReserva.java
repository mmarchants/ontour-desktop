package cl.ontour.frontend.desktop.views.create;

/**
 *
 * @author Andrea
 */
public class RegistrarReserva extends javax.swing.JFrame {

    public RegistrarReserva() {
        initComponents();

        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelReserva = new javax.swing.JPanel();
        lblCodigoReserva = new javax.swing.JLabel();
        txtCodigoReserva = new javax.swing.JTextField();
        lblDescripcionReserva = new javax.swing.JLabel();
        lblValorReserva = new javax.swing.JLabel();
        txtValorReserva = new javax.swing.JTextField();
        txtMontoAvalReserva = new javax.swing.JTextField();
        lblMontoAvalReserva = new javax.swing.JLabel();
        lblMontoAcumuladoReserva = new javax.swing.JLabel();
        lblFechaViajeReserva = new javax.swing.JLabel();
        txtFechaViajeReserva = new javax.swing.JTextField();
        lblFechaLimiteReserva = new javax.swing.JLabel();
        txtFechaLimiteReserva = new javax.swing.JTextField();
        txtMontoAcumuladoReserva = new javax.swing.JTextField();
        lblCantParticipantesReserva = new javax.swing.JLabel();
        txtCantParticipantesReserva = new javax.swing.JTextField();
        jComboBoxEjecutivo = new javax.swing.JComboBox();
        jComboBoxCurso = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jComboBoxPrograma = new javax.swing.JComboBox();
        txtDescripcionReserva = new javax.swing.JTextArea();
        btnCancelar = new javax.swing.JButton();
        BtnRegistrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelReserva.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ingrese una reserva"));

        lblCodigoReserva.setText("Código:");

        lblDescripcionReserva.setText("Descripcion");

        lblValorReserva.setText("Valor");

        lblMontoAvalReserva.setText("Monto aval");

        lblMontoAcumuladoReserva.setText("Monto acumulado");

        lblFechaViajeReserva.setText("Fecha viaje");

        txtFechaViajeReserva.setText("__/__/__");

        lblFechaLimiteReserva.setText("Fecha límite");

        txtFechaLimiteReserva.setText("__/__/__");

        lblCantParticipantesReserva.setText("Nº participantes");

        jComboBoxEjecutivo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ejecutivo a cargo", "--", "--" }));

        jComboBoxCurso.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Curso", "--", "--" }));

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));

        jComboBoxPrograma.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Programa", "--", "--" }));

        txtDescripcionReserva.setColumns(20);
        txtDescripcionReserva.setRows(5);

        org.jdesktop.layout.GroupLayout jPanelReservaLayout = new org.jdesktop.layout.GroupLayout(jPanelReserva);
        jPanelReserva.setLayout(jPanelReservaLayout);
        jPanelReservaLayout.setHorizontalGroup(
            jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelReservaLayout.createSequentialGroup()
                .add(82, 82, 82)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelReservaLayout.createSequentialGroup()
                        .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jSeparator3)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelReservaLayout.createSequentialGroup()
                                .add(13, 13, 13)
                                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jPanelReservaLayout.createSequentialGroup()
                                        .add(lblValorReserva)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(txtValorReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 162, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                            .add(lblMontoAcumuladoReserva)
                                            .add(lblMontoAvalReserva))
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                            .add(txtMontoAcumuladoReserva)
                                            .add(txtMontoAvalReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 168, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                    .add(jSeparator1)
                                    .add(jPanelReservaLayout.createSequentialGroup()
                                        .add(lblFechaLimiteReserva)
                                        .add(11, 11, 11)
                                        .add(txtFechaLimiteReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 121, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 382, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(jPanelReservaLayout.createSequentialGroup()
                                        .add(lblFechaViajeReserva)
                                        .add(18, 18, 18)
                                        .add(txtFechaViajeReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 121, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(26, 26, 26)
                                        .add(lblCantParticipantesReserva)
                                        .add(18, 18, 18)
                                        .add(txtCantParticipantesReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 39, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(jComboBoxCurso, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 159, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jSeparator2)))
                            .add(jPanelReservaLayout.createSequentialGroup()
                                .add(jComboBoxEjecutivo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 298, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jComboBoxPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 298, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(93, 93, 93))
                    .add(jPanelReservaLayout.createSequentialGroup()
                        .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanelReservaLayout.createSequentialGroup()
                                .add(lblDescripcionReserva)
                                .add(18, 18, 18)
                                .add(txtDescripcionReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 506, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanelReservaLayout.createSequentialGroup()
                                .add(35, 35, 35)
                                .add(lblCodigoReserva)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(txtCodigoReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 210, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanelReservaLayout.setVerticalGroup(
            jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelReservaLayout.createSequentialGroup()
                .add(31, 31, 31)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblCodigoReserva)
                    .add(txtCodigoReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblDescripcionReserva)
                    .add(txtDescripcionReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 40, Short.MAX_VALUE)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(txtValorReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(lblValorReserva))
                    .add(jPanelReservaLayout.createSequentialGroup()
                        .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(lblMontoAvalReserva)
                            .add(txtMontoAvalReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(lblMontoAcumuladoReserva)
                            .add(txtMontoAcumuladoReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .add(18, 18, 18)
                .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblFechaViajeReserva)
                    .add(txtFechaViajeReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblCantParticipantesReserva)
                    .add(txtCantParticipantesReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jComboBoxCurso, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblFechaLimiteReserva)
                    .add(txtFechaLimiteReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSeparator3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(28, 28, 28)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboBoxEjecutivo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jComboBoxPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(70, 70, 70))
        );

        btnCancelar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        BtnRegistrar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnRegistrar.setText("Registrar");
        BtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegistrarActionPerformed(evt);
            }
        });

        btnLimpiar.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(90, 90, 90)
                .add(jPanelReserva, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(106, Short.MAX_VALUE))
            .add(layout.createSequentialGroup()
                .add(66, 66, 66)
                .add(btnCancelar)
                .add(476, 476, 476)
                .add(btnLimpiar)
                .add(16, 16, 16)
                .add(BtnRegistrar)
                .addContainerGap(90, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(30, 30, 30)
                .add(jPanelReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(46, 46, 46)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btnCancelar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 46, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnLimpiar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(BtnRegistrar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(46, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    limpiaCampos();
}//GEN-LAST:event_btnLimpiarActionPerformed

private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
    dispose();

}//GEN-LAST:event_btnCancelarActionPerformed

private void BtnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegistrarActionPerformed


}//GEN-LAST:event_BtnRegistrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new RegistrarReserva().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnRegistrar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JComboBox jComboBoxCurso;
    private javax.swing.JComboBox jComboBoxEjecutivo;
    private javax.swing.JComboBox jComboBoxPrograma;
    private javax.swing.JPanel jPanelReserva;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel lblCantParticipantesReserva;
    private javax.swing.JLabel lblCodigoReserva;
    private javax.swing.JLabel lblDescripcionReserva;
    private javax.swing.JLabel lblFechaLimiteReserva;
    private javax.swing.JLabel lblFechaViajeReserva;
    private javax.swing.JLabel lblMontoAcumuladoReserva;
    private javax.swing.JLabel lblMontoAvalReserva;
    private javax.swing.JLabel lblValorReserva;
    private javax.swing.JTextField txtCantParticipantesReserva;
    private javax.swing.JTextField txtCodigoReserva;
    private javax.swing.JTextArea txtDescripcionReserva;
    private javax.swing.JTextField txtFechaLimiteReserva;
    private javax.swing.JTextField txtFechaViajeReserva;
    private javax.swing.JTextField txtMontoAcumuladoReserva;
    private javax.swing.JTextField txtMontoAvalReserva;
    private javax.swing.JTextField txtValorReserva;
    // End of variables declaration//GEN-END:variables

    public void limpiaCampos() {
    }
}
