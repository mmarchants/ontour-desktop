package cl.ontour.frontend.desktop.views.create;

/**
 *
 * @author Andrea
 */
public class RegistrarServicio extends javax.swing.JFrame {

    public RegistrarServicio() {
        initComponents();

        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        BtnRegistrar = new javax.swing.JButton();
        jPanelServicio = new javax.swing.JPanel();
        lblCodigoServicio = new javax.swing.JLabel();
        txtCodigoServicio = new javax.swing.JTextField();
        lblNombreServicio = new javax.swing.JLabel();
        lblDescripcionServicio = new javax.swing.JLabel();
        txtNombreServicio = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescripcionServicio = new javax.swing.JTextArea();
        jComboBoxDestino = new javax.swing.JComboBox();
        lblFoto = new javax.swing.JLabel();
        txtFoto = new javax.swing.JTextField();
        btnBusacrFoto = new javax.swing.JButton();
        btnGuardarFoto = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnLimpiar.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        BtnRegistrar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnRegistrar.setText("Registrar");
        BtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegistrarActionPerformed(evt);
            }
        });

        jPanelServicio.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ingrese un servicio"));

        lblCodigoServicio.setText("Código:");

        lblNombreServicio.setText("Nombre");

        lblDescripcionServicio.setText("Descripcion");

        txtDescripcionServicio.setColumns(20);
        txtDescripcionServicio.setRows(5);
        jScrollPane1.setViewportView(txtDescripcionServicio);

        jComboBoxDestino.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Destino", "--", "--" }));

        lblFoto.setText("Seleccionar foto");

        txtFoto.setText("no hay imagen cargada");

        btnBusacrFoto.setText("Buscar");

        btnGuardarFoto.setText("Subir foto");

        org.jdesktop.layout.GroupLayout jPanelServicioLayout = new org.jdesktop.layout.GroupLayout(jPanelServicio);
        jPanelServicio.setLayout(jPanelServicioLayout);
        jPanelServicioLayout.setHorizontalGroup(
            jPanelServicioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelServicioLayout.createSequentialGroup()
                .add(49, 49, 49)
                .add(jPanelServicioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelServicioLayout.createSequentialGroup()
                        .add(lblCodigoServicio)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(txtCodigoServicio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 210, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanelServicioLayout.createSequentialGroup()
                        .add(15, 15, 15)
                        .add(jPanelServicioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jPanelServicioLayout.createSequentialGroup()
                                .add(lblDescripcionServicio)
                                .add(18, 18, 18)
                                .add(jScrollPane1))
                            .add(jComboBoxDestino, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 298, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jPanelServicioLayout.createSequentialGroup()
                                .add(lblNombreServicio)
                                .add(41, 41, 41)
                                .add(txtNombreServicio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 561, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                    .add(jPanelServicioLayout.createSequentialGroup()
                        .add(lblFoto)
                        .add(18, 18, 18)
                        .add(txtFoto, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 213, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(btnBusacrFoto)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnGuardarFoto)))
                .addContainerGap(47, Short.MAX_VALUE))
        );
        jPanelServicioLayout.setVerticalGroup(
            jPanelServicioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelServicioLayout.createSequentialGroup()
                .add(28, 28, 28)
                .add(jPanelServicioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblCodigoServicio)
                    .add(txtCodigoServicio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(32, 32, 32)
                .add(jComboBoxDestino, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanelServicioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblNombreServicio)
                    .add(txtNombreServicio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelServicioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(lblDescripcionServicio)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 50, Short.MAX_VALUE)
                .add(jPanelServicioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblFoto)
                    .add(txtFoto, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBusacrFoto)
                    .add(btnGuardarFoto))
                .add(40, 40, 40))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(53, 53, 53)
                .add(btnCancelar)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(btnLimpiar)
                .add(18, 18, 18)
                .add(BtnRegistrar)
                .add(74, 74, 74))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap(108, Short.MAX_VALUE)
                .add(jPanelServicio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(105, 105, 105))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap(58, Short.MAX_VALUE)
                .add(jPanelServicio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(53, 53, 53)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnCancelar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(BtnRegistrar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnLimpiar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(47, 47, 47))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
    limpiaCampos();
}//GEN-LAST:event_btnLimpiarActionPerformed

private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
    dispose();

}//GEN-LAST:event_btnCancelarActionPerformed

private void BtnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegistrarActionPerformed


}//GEN-LAST:event_BtnRegistrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarServicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarServicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarServicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarServicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new RegistrarServicio().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnRegistrar;
    private javax.swing.JButton btnBusacrFoto;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardarFoto;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JComboBox jComboBoxDestino;
    private javax.swing.JPanel jPanelServicio;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblCodigoServicio;
    private javax.swing.JLabel lblDescripcionServicio;
    private javax.swing.JLabel lblFoto;
    private javax.swing.JLabel lblNombreServicio;
    private javax.swing.JTextField txtCodigoServicio;
    private javax.swing.JTextArea txtDescripcionServicio;
    private javax.swing.JTextField txtFoto;
    private javax.swing.JTextField txtNombreServicio;
    // End of variables declaration//GEN-END:variables

    public void limpiaCampos() {
    }
}
