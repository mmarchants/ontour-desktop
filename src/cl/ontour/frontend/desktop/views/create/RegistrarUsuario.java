package cl.ontour.frontend.desktop.views.create;

import cl.ontour.backend.desktop.domains.Agencia;
import cl.ontour.backend.desktop.domains.Apoderado;
import cl.ontour.backend.desktop.domains.Empleado;
import cl.ontour.backend.desktop.domains.Rol;
import cl.ontour.backend.desktop.domains.Usuario;
import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Andrea
 */
public class RegistrarUsuario extends javax.swing.JFrame {

    public RegistrarUsuario() {
        initComponents();
        setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/cl/ontour/frontend/desktop/resources/images/logo_ontour_icon.png")));
        Funciones.activarComponentes(jPanelEmpleado, false);
        Funciones.activarComponentes(jPanelApoderado, false);
    }
    
    private void cargarRoles() {
        List<Rol> roles = Rol.listadoRoles();
        for (int i = 0; i < roles.size(); i++) {
            if (!roles.get(i).getCodigo().equals("APO") && !roles.get(i).getCodigo().equals("REP")) {
                jComboBoxRol.addItem(roles.get(i));
            }            
        }
    }
    
    private void limpiaCampos() {
        txtUsuarioMail.setText("");
        txtPassUsuarioClave.setText("");
        txtCodigoEmpleado.setText("");
        txtEmpleadoNombre.setText("");
        txtEmpleadoApe1.setText("");
        txtEmpleadoApe2.setText("");
        txtEmpleadoFono.setText("");
        txtApoderadoNombre.setText("");
        txtApoderadoApe1.setText("");
        txtApoderadoApe2.setText("");
        txtApoderadoFono.setText("");
        jCheckBoxRepresentante.setSelected(false);
        jComboBoxRol.removeAllItems();
        jRdaBtnApoderado.setSelected(false);
        jRdaBtnEmpleado.setSelected(false);
        Funciones.activarComponentes(jPanelEmpleado, false);
        Funciones.activarComponentes(jPanelApoderado, false);
    }    
    
    private boolean validaCampos() {
        if (jRdaBtnEmpleado.isSelected()) {
            if (txtUsuarioMail.getText().isEmpty() || 
                String.valueOf(txtPassUsuarioClave.getPassword()).isEmpty() || 
                txtCodigoEmpleado.getText().isEmpty() || 
                txtEmpleadoNombre.getText().isEmpty() || 
                txtEmpleadoApe1.getText().isEmpty() || 
                txtEmpleadoApe2.getText().isEmpty() || 
                txtEmpleadoFono.getText().isEmpty()) {
                return false;
            }
            return true;            
        }
        if (jRdaBtnApoderado.isSelected()) {
            if (txtUsuarioMail.getText().isEmpty() || 
                String.valueOf(txtPassUsuarioClave.getPassword()).isEmpty() || 
                txtApoderadoNombre.getText().isEmpty() || 
                txtApoderadoApe1.getText().isEmpty() || 
                txtApoderadoApe2.getText().isEmpty() || 
                txtApoderadoFono.getText().isEmpty()) {
                return false;
            }
            return true;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupTipoUsuario = new javax.swing.ButtonGroup();
        jPanelUsuario = new javax.swing.JPanel();
        lblUsuarioPassw = new javax.swing.JLabel();
        lblUsuarioMail = new javax.swing.JLabel();
        txtUsuarioMail = new javax.swing.JTextField();
        txtPassUsuarioClave = new javax.swing.JPasswordField();
        jPanelTipoUsuario = new javax.swing.JPanel();
        jRdaBtnEmpleado = new javax.swing.JRadioButton();
        jRdaBtnApoderado = new javax.swing.JRadioButton();
        BtnRegistrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jPanelEmpleado = new javax.swing.JPanel();
        lblEmpleadoApe1 = new javax.swing.JLabel();
        lblEmpleadoNombre = new javax.swing.JLabel();
        lblEmpleadoCodigo = new javax.swing.JLabel();
        txtEmpleadoNombre = new javax.swing.JTextField();
        txtEmpleadoApe1 = new javax.swing.JTextField();
        txtCodigoEmpleado = new javax.swing.JTextField();
        lblEmpleado = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        lblEmpleadoApe2 = new javax.swing.JLabel();
        txtEmpleadoApe2 = new javax.swing.JTextField();
        lblEmpleadoFono = new javax.swing.JLabel();
        txtEmpleadoFono = new javax.swing.JTextField();
        jComboBoxRol = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jPanelApoderado = new javax.swing.JPanel();
        lblEmpleadoPassw4 = new javax.swing.JLabel();
        lblEmpleadoMail2 = new javax.swing.JLabel();
        txtApoderadoNombre = new javax.swing.JTextField();
        txtApoderadoApe1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        lblEmpleadoPassw5 = new javax.swing.JLabel();
        txtApoderadoApe2 = new javax.swing.JTextField();
        lblEmpleadoPassw6 = new javax.swing.JLabel();
        txtApoderadoFono = new javax.swing.JTextField();
        jCheckBoxRepresentante = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Registro de usuarios");
        setBounds(new java.awt.Rectangle(0, 23, 1085, 1000));

        jPanelUsuario.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51)));

        lblUsuarioPassw.setText("Contraseña:");

        lblUsuarioMail.setText("E-mail:");

        txtUsuarioMail.setPreferredSize(new java.awt.Dimension(110, 28));

        org.jdesktop.layout.GroupLayout jPanelUsuarioLayout = new org.jdesktop.layout.GroupLayout(jPanelUsuario);
        jPanelUsuario.setLayout(jPanelUsuarioLayout);
        jPanelUsuarioLayout.setHorizontalGroup(
            jPanelUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelUsuarioLayout.createSequentialGroup()
                .add(33, 33, 33)
                .add(lblUsuarioMail)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(txtUsuarioMail, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(50, 50, 50)
                .add(lblUsuarioPassw)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txtPassUsuarioClave, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(156, 156, 156))
        );
        jPanelUsuarioLayout.setVerticalGroup(
            jPanelUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelUsuarioLayout.createSequentialGroup()
                .add(23, 23, 23)
                .add(jPanelUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(txtUsuarioMail, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblUsuarioMail)
                    .add(lblUsuarioPassw)
                    .add(txtPassUsuarioClave, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(23, Short.MAX_VALUE))
        );

        jPanelTipoUsuario.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Tipo de Usuario a registrar"));

        btnGroupTipoUsuario.add(jRdaBtnEmpleado);
        jRdaBtnEmpleado.setText("Empleado");
        jRdaBtnEmpleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRdaBtnEmpleadoActionPerformed(evt);
            }
        });

        btnGroupTipoUsuario.add(jRdaBtnApoderado);
        jRdaBtnApoderado.setText("Apoderado");
        jRdaBtnApoderado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRdaBtnApoderadoActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanelTipoUsuarioLayout = new org.jdesktop.layout.GroupLayout(jPanelTipoUsuario);
        jPanelTipoUsuario.setLayout(jPanelTipoUsuarioLayout);
        jPanelTipoUsuarioLayout.setHorizontalGroup(
            jPanelTipoUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelTipoUsuarioLayout.createSequentialGroup()
                .addContainerGap()
                .add(jRdaBtnEmpleado)
                .add(30, 30, 30)
                .add(jRdaBtnApoderado)
                .addContainerGap(117, Short.MAX_VALUE))
        );
        jPanelTipoUsuarioLayout.setVerticalGroup(
            jPanelTipoUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelTipoUsuarioLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanelTipoUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jRdaBtnEmpleado)
                    .add(jRdaBtnApoderado))
                .addContainerGap())
        );

        BtnRegistrar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnRegistrar.setText("Registrar");
        BtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegistrarActionPerformed(evt);
            }
        });

        btnLimpiar.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar.setText("Volver");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jPanelEmpleado.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51)));

        lblEmpleadoApe1.setText("Primer apellido:");

        lblEmpleadoNombre.setText("Nombre:");

        lblEmpleadoCodigo.setText("Código:");

        txtEmpleadoNombre.setPreferredSize(new java.awt.Dimension(110, 28));

        txtEmpleadoApe1.setPreferredSize(new java.awt.Dimension(110, 28));

        lblEmpleado.setText("Datos Empleado");

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        lblEmpleadoApe2.setText("Segundo apellido:");

        txtEmpleadoApe2.setPreferredSize(new java.awt.Dimension(110, 28));

        lblEmpleadoFono.setText("Teléfono:");

        txtEmpleadoFono.setPreferredSize(new java.awt.Dimension(110, 28));

        jLabel1.setText("Rol:");

        org.jdesktop.layout.GroupLayout jPanelEmpleadoLayout = new org.jdesktop.layout.GroupLayout(jPanelEmpleado);
        jPanelEmpleado.setLayout(jPanelEmpleadoLayout);
        jPanelEmpleadoLayout.setHorizontalGroup(
            jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelEmpleadoLayout.createSequentialGroup()
                .add(22, 22, 22)
                .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jSeparator1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
                    .add(jPanelEmpleadoLayout.createSequentialGroup()
                        .add(6, 6, 6)
                        .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(lblEmpleado)
                                .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanelEmpleadoLayout.createSequentialGroup()
                                        .add(58, 58, 58)
                                        .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                            .add(lblEmpleadoNombre)
                                            .add(lblEmpleadoCodigo))
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                            .add(txtEmpleadoNombre, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                                            .add(txtCodigoEmpleado)))
                                    .add(jPanelEmpleadoLayout.createSequentialGroup()
                                        .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                            .add(lblEmpleadoFono)
                                            .add(jLabel1))
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                            .add(jComboBoxRol, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .add(txtEmpleadoFono, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)))))
                            .add(jPanelEmpleadoLayout.createSequentialGroup()
                                .add(lblEmpleadoApe1)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(txtEmpleadoApe1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanelEmpleadoLayout.createSequentialGroup()
                                .add(lblEmpleadoApe2)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(txtEmpleadoApe2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(0, 41, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelEmpleadoLayout.setVerticalGroup(
            jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelEmpleadoLayout.createSequentialGroup()
                .add(29, 29, 29)
                .add(lblEmpleado)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(34, 34, 34)
                .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblEmpleadoCodigo)
                    .add(txtCodigoEmpleado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblEmpleadoNombre)
                    .add(txtEmpleadoNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtEmpleadoApe1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblEmpleadoApe1))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblEmpleadoApe2)
                    .add(txtEmpleadoApe2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblEmpleadoFono)
                    .add(txtEmpleadoFono, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelEmpleadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboBoxRol, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel1))
                .addContainerGap(50, Short.MAX_VALUE))
        );

        jPanelApoderado.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(51, 51, 51)));

        lblEmpleadoPassw4.setText("Primer apellido:");

        lblEmpleadoMail2.setText("Nombre:");

        txtApoderadoNombre.setPreferredSize(new java.awt.Dimension(110, 28));

        txtApoderadoApe1.setPreferredSize(new java.awt.Dimension(110, 28));

        jLabel2.setText("Datos Apoderado");

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        lblEmpleadoPassw5.setText("Segundo apellido:");

        txtApoderadoApe2.setPreferredSize(new java.awt.Dimension(110, 28));

        lblEmpleadoPassw6.setText("Teléfono:");

        txtApoderadoFono.setPreferredSize(new java.awt.Dimension(110, 28));

        jCheckBoxRepresentante.setText("Representante");

        org.jdesktop.layout.GroupLayout jPanelApoderadoLayout = new org.jdesktop.layout.GroupLayout(jPanelApoderado);
        jPanelApoderado.setLayout(jPanelApoderadoLayout);
        jPanelApoderadoLayout.setHorizontalGroup(
            jPanelApoderadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelApoderadoLayout.createSequentialGroup()
                .add(28, 28, 28)
                .add(jPanelApoderadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jSeparator2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE)
                    .add(jPanelApoderadoLayout.createSequentialGroup()
                        .add(jPanelApoderadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel2)
                            .add(jPanelApoderadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                .add(jPanelApoderadoLayout.createSequentialGroup()
                                    .add(lblEmpleadoPassw5)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(txtApoderadoApe2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(org.jdesktop.layout.GroupLayout.LEADING, jPanelApoderadoLayout.createSequentialGroup()
                                    .add(58, 58, 58)
                                    .add(lblEmpleadoMail2)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(txtApoderadoNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(jPanelApoderadoLayout.createSequentialGroup()
                                    .add(lblEmpleadoPassw4)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(txtApoderadoApe1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(jPanelApoderadoLayout.createSequentialGroup()
                                    .add(lblEmpleadoPassw6)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(jPanelApoderadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(jCheckBoxRepresentante)
                                        .add(txtApoderadoFono, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                        .add(0, 30, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelApoderadoLayout.setVerticalGroup(
            jPanelApoderadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelApoderadoLayout.createSequentialGroup()
                .add(29, 29, 29)
                .add(jLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(34, 34, 34)
                .add(jPanelApoderadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblEmpleadoMail2)
                    .add(txtApoderadoNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelApoderadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtApoderadoApe1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblEmpleadoPassw4))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelApoderadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblEmpleadoPassw5)
                    .add(txtApoderadoApe2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelApoderadoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblEmpleadoPassw6)
                    .add(txtApoderadoFono, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jCheckBoxRepresentante)
                .addContainerGap(84, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(62, 62, 62)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(layout.createSequentialGroup()
                                .add(jPanelEmpleado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(jPanelApoderado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanelTipoUsuario, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jPanelUsuario, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .add(layout.createSequentialGroup()
                        .add(78, 78, 78)
                        .add(btnCancelar)
                        .add(588, 588, 588)
                        .add(btnLimpiar)
                        .add(18, 18, 18)
                        .add(BtnRegistrar)))
                .addContainerGap(88, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(51, 51, 51)
                .add(jPanelTipoUsuario, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanelUsuario, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jPanelEmpleado, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanelApoderado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(37, 37, 37)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(7, 7, 7)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(BtnRegistrar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(btnLimpiar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(btnCancelar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(42, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void jRdaBtnEmpleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRdaBtnEmpleadoActionPerformed
    if (jRdaBtnEmpleado.isSelected()) {
        Funciones.activarComponentes(jPanelEmpleado, true);
        Funciones.activarComponentes(jPanelApoderado, false);
        if (jComboBoxRol.getItemCount() == 0) {
            cargarRoles();
        }
    }
}//GEN-LAST:event_jRdaBtnEmpleadoActionPerformed

private void jRdaBtnApoderadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRdaBtnApoderadoActionPerformed
    if (jRdaBtnApoderado.isSelected()) {
        Funciones.activarComponentes(jPanelEmpleado, false);
        Funciones.activarComponentes(jPanelApoderado, true);
    }
}//GEN-LAST:event_jRdaBtnApoderadoActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        limpiaCampos();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void BtnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegistrarActionPerformed
        if (!validaCampos()) {
            JOptionPane.showMessageDialog(null, "Ingrese datos faltantes.");
            return;
        } 
        
        // Registro de empleados:
        if (jRdaBtnEmpleado.isSelected()) {
            if (Usuario.registrarUsuario(txtUsuarioMail.getText().trim(), String.valueOf(txtPassUsuarioClave.getPassword()).trim(), (Rol) jComboBoxRol.getSelectedItem())) {
                Usuario usuario = Usuario.buscarPorCorreo(txtUsuarioMail.getText().trim());
                if (Empleado.registrarEmpleado(txtCodigoEmpleado.getText(), txtEmpleadoNombre.getText(), txtEmpleadoApe1.getText(), txtEmpleadoApe2.getText(), txtEmpleadoFono.getText(), usuario, Agencia.buscarPorId("1"))) {
                    JOptionPane.showMessageDialog(null, "Registro exitoso.");
                    limpiaCampos();
                } else if (!Administrador.error.isEmpty()) {
                    JOptionPane.showMessageDialog(null, Administrador.error);
                    Administrador.error = "";
                } else {
                    JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
                }
            } else if (!Administrador.error.isEmpty()) {
                JOptionPane.showMessageDialog(null, Administrador.error);
                Administrador.error = "";
            } else {
                JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
            }   
        }
        
        // Registro de apoderados:
        if (jRdaBtnApoderado.isSelected()) {
            Rol rol;
            if (jCheckBoxRepresentante.isSelected()) {
                rol = Rol.buscarPorId("4"); // Rol de representante                
            } else {
                rol = Rol.buscarPorId("3"); // Rol de apoderado                
            }            
            if (Usuario.registrarUsuario(txtUsuarioMail.getText().trim(), String.valueOf(txtPassUsuarioClave.getPassword()).trim(), rol)) {
                Usuario usuario = Usuario.buscarPorCorreo(txtUsuarioMail.getText().trim());
                if (Apoderado.registrarApoderado(txtApoderadoNombre.getText(), txtApoderadoApe1.getText(), txtApoderadoApe2.getText(), txtApoderadoFono.getText(), usuario)) {
                    JOptionPane.showMessageDialog(null, "Registro exitoso.");
                    limpiaCampos();
                } else if (!Administrador.error.isEmpty()) {
                    JOptionPane.showMessageDialog(null, Administrador.error);
                    Administrador.error = "";
                } else {
                    JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
                }
            } else if (!Administrador.error.isEmpty()) {
                JOptionPane.showMessageDialog(null, Administrador.error);
                Administrador.error = "";
            } else {
                JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
            }   
        }
    }//GEN-LAST:event_BtnRegistrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new RegistrarUsuario().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnRegistrar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.ButtonGroup btnGroupTipoUsuario;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JCheckBox jCheckBoxRepresentante;
    private javax.swing.JComboBox jComboBoxRol;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanelApoderado;
    private javax.swing.JPanel jPanelEmpleado;
    private javax.swing.JPanel jPanelTipoUsuario;
    private javax.swing.JPanel jPanelUsuario;
    private javax.swing.JRadioButton jRdaBtnApoderado;
    private javax.swing.JRadioButton jRdaBtnEmpleado;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lblEmpleado;
    private javax.swing.JLabel lblEmpleadoApe1;
    private javax.swing.JLabel lblEmpleadoApe2;
    private javax.swing.JLabel lblEmpleadoCodigo;
    private javax.swing.JLabel lblEmpleadoFono;
    private javax.swing.JLabel lblEmpleadoMail2;
    private javax.swing.JLabel lblEmpleadoNombre;
    private javax.swing.JLabel lblEmpleadoPassw4;
    private javax.swing.JLabel lblEmpleadoPassw5;
    private javax.swing.JLabel lblEmpleadoPassw6;
    private javax.swing.JLabel lblUsuarioMail;
    private javax.swing.JLabel lblUsuarioPassw;
    private javax.swing.JTextField txtApoderadoApe1;
    private javax.swing.JTextField txtApoderadoApe2;
    private javax.swing.JTextField txtApoderadoFono;
    private javax.swing.JTextField txtApoderadoNombre;
    private javax.swing.JTextField txtCodigoEmpleado;
    private javax.swing.JTextField txtEmpleadoApe1;
    private javax.swing.JTextField txtEmpleadoApe2;
    private javax.swing.JTextField txtEmpleadoFono;
    private javax.swing.JTextField txtEmpleadoNombre;
    private javax.swing.JPasswordField txtPassUsuarioClave;
    private javax.swing.JTextField txtUsuarioMail;
    // End of variables declaration//GEN-END:variables

}
