package cl.ontour.frontend.desktop.views.generics;

import java.awt.GraphicsConfiguration;
import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;

import cl.ontour.frontend.desktop.views.update.AjustarPais;
import cl.ontour.frontend.desktop.views.read.ConsultarCiudad;
import cl.ontour.frontend.desktop.views.create.RegistrarAlumno;
import cl.ontour.frontend.desktop.views.create.RegistrarCiudad;
import cl.ontour.frontend.desktop.views.create.RegistrarColegio;
import cl.ontour.frontend.desktop.views.create.RegistrarContrato;
import cl.ontour.frontend.desktop.views.create.RegistrarCurso;
import cl.ontour.frontend.desktop.views.create.RegistrarDestino;
import cl.ontour.frontend.desktop.views.create.RegistrarPais;
import cl.ontour.frontend.desktop.views.create.RegistrarPrograma;
import cl.ontour.frontend.desktop.views.create.RegistrarReserva;
import cl.ontour.frontend.desktop.views.create.RegistrarSeguro;
import cl.ontour.frontend.desktop.views.create.RegistrarServicio;
import cl.ontour.frontend.desktop.views.create.RegistrarTraslado;
import cl.ontour.frontend.desktop.views.create.RegistrarUsuario;
import cl.ontour.frontend.desktop.views.read.ConsultarAgencia;
import cl.ontour.frontend.desktop.views.read.ConsultarAlumno;
import cl.ontour.frontend.desktop.views.read.ConsultarColegio;
import cl.ontour.frontend.desktop.views.read.ConsultarContrato;
import cl.ontour.frontend.desktop.views.read.ConsultarCurso;
import cl.ontour.frontend.desktop.views.read.ConsultarDestino;
import cl.ontour.frontend.desktop.views.read.ConsultarHospedaje;

import cl.ontour.frontend.desktop.views.read.ConsultarPais;
import cl.ontour.frontend.desktop.views.read.ConsultarPrograma;
import cl.ontour.frontend.desktop.views.read.ConsultarReserva;
import cl.ontour.frontend.desktop.views.read.ConsultarSeguro;
import cl.ontour.frontend.desktop.views.read.ConsultarServicio;
import cl.ontour.frontend.desktop.views.read.ConsultarTraslado;
import cl.ontour.frontend.desktop.views.read.ConsultarUsuario;
import cl.ontour.frontend.desktop.views.update.AjustarAgencia;
import cl.ontour.frontend.desktop.views.update.AjustarAlumno;
import cl.ontour.frontend.desktop.views.update.AjustarCiudad;
import cl.ontour.frontend.desktop.views.update.AjustarColegio;
import cl.ontour.frontend.desktop.views.update.AjustarContrato;
import cl.ontour.frontend.desktop.views.update.AjustarCurso;
import cl.ontour.frontend.desktop.views.update.AjustarDestino;
import cl.ontour.frontend.desktop.views.update.AjustarPrograma;
import cl.ontour.frontend.desktop.views.update.AjustarReserva;
import cl.ontour.frontend.desktop.views.update.AjustarSeguro;
import cl.ontour.frontend.desktop.views.update.AjustarServicio;
import cl.ontour.frontend.desktop.views.update.AjustarTraslado;
import cl.ontour.frontend.desktop.views.update.AjustarUsuario;
import java.awt.Color;
import java.awt.Toolkit;

/**
 * @author Andrea Saravia C.
 */
public class Home extends javax.swing.JFrame {

    // Variables:
    private int opcion;
    RegistrarAlumno registrar_alumno;
    RegistrarServicio registrar_servicio;
    RegistrarTraslado registrar_traslado;
    RegistrarContrato registrar_contrato;
    RegistrarReserva registrar_reserva;
    RegistrarUsuario registrar_usuario;
    RegistrarColegio registrar_colegio;
    RegistrarCurso registrar_curso;
    RegistrarPais registrar_pais;
    RegistrarCiudad registrar_ciudad;
    RegistrarPrograma registrar_programa;
    RegistrarSeguro registrar_seguro;
    RegistrarDestino registrar_destino;

    ConsultarPais consultar_pais;
    ConsultarCiudad consultar_ciudad;
    ConsultarPrograma consultar_programa;
    ConsultarServicio consultar_servico;
    ConsultarColegio consultar_colegio;
    ConsultarCurso consultar_curso;
    ConsultarContrato consultar_contrato;
    ConsultarTraslado consultar_traslado;
    ConsultarAlumno consultar_alumno;
    ConsultarReserva consultar_reserva;
    ConsultarAgencia consultar_agencia;
    ConsultarUsuario consultar_usuario;
    ConsultarSeguro consultar_seguro;
    ConsultarDestino consultar_destino;
    ConsultarHospedaje consultar_hospedaje;

    AjustarAlumno ajustar_alumno;
    AjustarCiudad ajustar_ciudad;
    AjustarColegio ajustar_colegio;
    AjustarContrato ajustar_contrato;
    AjustarCurso ajustar_curso;
    AjustarPais ajustar_pais;
    AjustarPrograma ajustar_programa;
    AjustarReserva ajustar_reserva;
    AjustarServicio ajustar_servicio;
    AjustarTraslado ajustar_traslado;
    AjustarUsuario ajustar_usuario;
    AjustarAgencia ajustar_agencia;
    AjustarDestino ajustar_destino;
    AjustarSeguro ajustar_seguro;

    // Constructores:
    public Home() {
        initComponents();
        setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/cl/ontour/frontend/desktop/resources/images/logo_ontour_icon.png")));
        Funciones.activarComponentes(jPanelOpcion, false);
        jPanelOpcion.setBackground(Color.gray);
        eliminarFocoBtnsHome();
    }

    public Home(GraphicsConfiguration gc) {
        super(gc);
    }

    private void eliminarFocoBtnsHome() {
        for (int i = 0; i < Home.jPanelAccion.getComponents().length; i++) {
            Home.jPanelAccion.getComponent(i).setFocusable(false);
        }
    }

    // Código autogenerado y funciones:
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSalir = new javax.swing.JButton();
        jPanelAccion = new javax.swing.JPanel();
        btnAjustes = new javax.swing.JButton();
        btnConsulta = new javax.swing.JButton();
        btnRegistrar = new javax.swing.JButton();
        jPanelOpcion = new javax.swing.JPanel();
        btnServicio = new javax.swing.JButton();
        btnTraslado = new javax.swing.JButton();
        btnSeguro = new javax.swing.JButton();
        btnPrograma = new javax.swing.JButton();
        btnCurso = new javax.swing.JButton();
        btnDeposito = new javax.swing.JButton();
        btnEvento = new javax.swing.JButton();
        btnReserva = new javax.swing.JButton();
        btnContrato = new javax.swing.JButton();
        btnDestino = new javax.swing.JButton();
        btnCiudad = new javax.swing.JButton();
        btnAlumno = new javax.swing.JButton();
        btnColegio = new javax.swing.JButton();
        btnUsuario = new javax.swing.JButton();
        btnPais = new javax.swing.JButton();
        btnTipoContrato = new javax.swing.JButton();
        btnHospedaje = new javax.swing.JButton();
        btnTipoTraslado = new javax.swing.JButton();
        btnTipoEvento = new javax.swing.JButton();
        btnTipoSeguro = new javax.swing.JButton();
        btnAgencia = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Sistema de administración OnTour");
        setResizable(false);

        btnSalir.setBackground(new java.awt.Color(153, 0, 0));
        btnSalir.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        btnSalir.setForeground(new java.awt.Color(255, 255, 255));
        btnSalir.setText("Salir");
        btnSalir.setFocusPainted(false);
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        jPanelAccion.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "MANTENEDOR ON-TOUR", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Lucida Grande", 1, 18))); // NOI18N

        btnAjustes.setBackground(new java.awt.Color(153, 153, 0));
        btnAjustes.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btnAjustes.setForeground(new java.awt.Color(255, 255, 255));
        btnAjustes.setText("AJUSTAR");
        btnAjustes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAjustesActionPerformed(evt);
            }
        });

        btnConsulta.setBackground(new java.awt.Color(0, 153, 153));
        btnConsulta.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btnConsulta.setForeground(new java.awt.Color(255, 255, 255));
        btnConsulta.setText("CONSULTAR");
        btnConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultaActionPerformed(evt);
            }
        });

        btnRegistrar.setBackground(new java.awt.Color(0, 102, 153));
        btnRegistrar.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        btnRegistrar.setForeground(new java.awt.Color(255, 255, 255));
        btnRegistrar.setText("REGISTRAR");
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanelAccionLayout = new org.jdesktop.layout.GroupLayout(jPanelAccion);
        jPanelAccion.setLayout(jPanelAccionLayout);
        jPanelAccionLayout.setHorizontalGroup(
            jPanelAccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelAccionLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelAccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btnAjustes, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanelAccionLayout.createSequentialGroup()
                        .add(btnConsulta, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 668, Short.MAX_VALUE)
                        .addContainerGap())
                    .add(btnRegistrar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanelAccionLayout.setVerticalGroup(
            jPanelAccionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelAccionLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(btnConsulta, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 56, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(btnRegistrar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 56, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(btnAjustes, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 56, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanelOpcion.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnServicio.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnServicio.setText("SERVICIOS");
        btnServicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnServicioActionPerformed(evt);
            }
        });

        btnTraslado.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnTraslado.setText("TRASLADOS");
        btnTraslado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTrasladoActionPerformed(evt);
            }
        });

        btnSeguro.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnSeguro.setText("SEGUROS");
        btnSeguro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeguroActionPerformed(evt);
            }
        });

        btnPrograma.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnPrograma.setText("PROGRAMAS");
        btnPrograma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProgramaActionPerformed(evt);
            }
        });

        btnCurso.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnCurso.setText("CURSOS");
        btnCurso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCursoActionPerformed(evt);
            }
        });

        btnDeposito.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnDeposito.setText("DEPÓSITOS");

        btnEvento.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnEvento.setText("EVENTOS");

        btnReserva.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnReserva.setText("RESERVAS");
        btnReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReservaActionPerformed(evt);
            }
        });

        btnContrato.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnContrato.setText("CONTRATOS");
        btnContrato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnContratoActionPerformed(evt);
            }
        });

        btnDestino.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnDestino.setText("DESTINOS");
        btnDestino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDestinoActionPerformed(evt);
            }
        });

        btnCiudad.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnCiudad.setText("CIUDADES");
        btnCiudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCiudadActionPerformed(evt);
            }
        });

        btnAlumno.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnAlumno.setText("ALUMNOS");
        btnAlumno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAlumnoActionPerformed(evt);
            }
        });

        btnColegio.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnColegio.setText("COLEGIOS");
        btnColegio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnColegioActionPerformed(evt);
            }
        });

        btnUsuario.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnUsuario.setText("USUARIOS");
        btnUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsuarioActionPerformed(evt);
            }
        });

        btnPais.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnPais.setText("PAÍSES");
        btnPais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaisActionPerformed(evt);
            }
        });

        btnTipoContrato.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnTipoContrato.setText("TIPOS CONTRATOS");
        btnTipoContrato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTipoContratoActionPerformed(evt);
            }
        });

        btnHospedaje.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnHospedaje.setText("HOSPEDAJES");
        btnHospedaje.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHospedajeActionPerformed(evt);
            }
        });

        btnTipoTraslado.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnTipoTraslado.setText("TIPOS TRASLADOS");
        btnTipoTraslado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTipoTrasladoActionPerformed(evt);
            }
        });

        btnTipoEvento.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnTipoEvento.setText("TIPOS EVENTOS");

        btnTipoSeguro.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnTipoSeguro.setText("TIPOS SEGUROS");

        btnAgencia.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnAgencia.setText("AGENCIA");
        btnAgencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgenciaActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanelOpcionLayout = new org.jdesktop.layout.GroupLayout(jPanelOpcion);
        jPanelOpcion.setLayout(jPanelOpcionLayout);
        jPanelOpcionLayout.setHorizontalGroup(
            jPanelOpcionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelOpcionLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelOpcionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelOpcionLayout.createSequentialGroup()
                        .add(btnDestino, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnDeposito, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnSeguro, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnTipoSeguro, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(btnAgencia, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(0, 0, Short.MAX_VALUE))
                    .add(jPanelOpcionLayout.createSequentialGroup()
                        .add(jPanelOpcionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanelOpcionLayout.createSequentialGroup()
                                .add(btnUsuario, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnCurso, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnContrato, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnTipoContrato, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanelOpcionLayout.createSequentialGroup()
                                .add(btnPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnServicio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnHospedaje, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnTipoTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnEvento, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnTipoEvento, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 175, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanelOpcionLayout.setVerticalGroup(
            jPanelOpcionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelOpcionLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelOpcionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnUsuario, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnCurso, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnContrato, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnTipoContrato, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelOpcionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnServicio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnHospedaje, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnTipoTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnEvento, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnTipoEvento, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelOpcionLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnDestino, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnDeposito, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnSeguro, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnTipoSeguro, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnAgencia, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 43, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/cl/ontour/frontend/desktop/resources/images/logo_ontour.png"))); // NOI18N

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel2)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jLabel2)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanelAccion, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(btnSalir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 157, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jPanelOpcion, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(10, 10, 10))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(10, 10, 10)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanelAccion, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelOpcion, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btnSalir, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 76, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(10, 10, 10))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
    opcion = 1;    
    jPanelOpcion.setBackground(new java.awt.Color(0,102,153));
    if (Administrador.rol.equals("ADM")) {
        Funciones.activarComponentes(jPanelOpcion, true);  
        btnUsuario.setEnabled(true);
        btnAlumno.setEnabled(false);
        btnCurso.setEnabled(true);
        btnColegio.setEnabled(true);
        btnReserva.setEnabled(true);
        btnContrato.setEnabled(true);
        btnTipoContrato.setEnabled(true);
        btnPrograma.setEnabled(true);
        btnServicio.setEnabled(true);
        btnHospedaje.setEnabled(true);
        btnTraslado.setEnabled(true);
        btnTipoTraslado.setEnabled(true);
        btnEvento.setEnabled(false);
        btnTipoEvento.setEnabled(true);
        btnDestino.setEnabled(true);
        btnCiudad.setEnabled(true);
        btnPais.setEnabled(true);
        btnDeposito.setEnabled(false);
        btnSeguro.setEnabled(false);
        btnTipoSeguro.setEnabled(false);
        btnAgencia.setEnabled(false);
    } else if (Administrador.rol.equals("EJE")) {
        Funciones.activarComponentes(jPanelOpcion, true);        
        btnUsuario.setEnabled(false);
        btnAlumno.setEnabled(false);
        btnCurso.setEnabled(false);
        btnColegio.setEnabled(false);
        btnReserva.setEnabled(true);
        btnContrato.setEnabled(true);
        btnTipoContrato.setEnabled(false);
        btnPrograma.setEnabled(false);
        btnServicio.setEnabled(false);
        btnHospedaje.setEnabled(false);
        btnTraslado.setEnabled(false);
        btnTipoTraslado.setEnabled(false);
        btnEvento.setEnabled(false);
        btnTipoEvento.setEnabled(false);
        btnDestino.setEnabled(false);
        btnCiudad.setEnabled(false);
        btnPais.setEnabled(false);
        btnDeposito.setEnabled(false);
        btnSeguro.setEnabled(true);
        btnTipoSeguro.setEnabled(true);
        btnAgencia.setEnabled(false);
    }
}//GEN-LAST:event_btnRegistrarActionPerformed

private void btnConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultaActionPerformed
    opcion = 2;
    jPanelOpcion.setBackground(new java.awt.Color(0,153,153));
    if (Administrador.rol.equals("ADM")) {
        Funciones.activarComponentes(jPanelOpcion, true);  
        btnUsuario.setEnabled(true);
        btnAlumno.setEnabled(true);
        btnCurso.setEnabled(true);
        btnColegio.setEnabled(true);
        btnReserva.setEnabled(true);
        btnContrato.setEnabled(true);
        btnTipoContrato.setEnabled(true);
        btnPrograma.setEnabled(true);
        btnServicio.setEnabled(true);
        btnHospedaje.setEnabled(true);
        btnTraslado.setEnabled(true);
        btnTipoTraslado.setEnabled(true);
        btnEvento.setEnabled(true);
        btnTipoEvento.setEnabled(true);
        btnDestino.setEnabled(true);
        btnCiudad.setEnabled(true);
        btnPais.setEnabled(true);
        btnDeposito.setEnabled(true);
        btnSeguro.setEnabled(true);
        btnTipoSeguro.setEnabled(true);
        btnAgencia.setEnabled(true);
    } else if (Administrador.rol.equals("EJE")) {
        Funciones.activarComponentes(jPanelOpcion, true);        
        btnUsuario.setEnabled(true);
        btnAlumno.setEnabled(true);
        btnCurso.setEnabled(true);
        btnColegio.setEnabled(true);
        btnReserva.setEnabled(true);
        btnContrato.setEnabled(true);
        btnTipoContrato.setEnabled(true);
        btnPrograma.setEnabled(true);
        btnServicio.setEnabled(true);
        btnHospedaje.setEnabled(true);
        btnTraslado.setEnabled(true);
        btnTipoTraslado.setEnabled(true);
        btnEvento.setEnabled(false);
        btnTipoEvento.setEnabled(false);
        btnDestino.setEnabled(true);
        btnCiudad.setEnabled(true);
        btnPais.setEnabled(true);
        btnDeposito.setEnabled(true);
        btnSeguro.setEnabled(true);
        btnTipoSeguro.setEnabled(true);
        btnAgencia.setEnabled(true);
    }
}//GEN-LAST:event_btnConsultaActionPerformed

private void btnAjustesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAjustesActionPerformed
    opcion = 3;
    jPanelOpcion.setBackground(new java.awt.Color(153,153,0));
    if (Administrador.rol.equals("ADM")) {
        Funciones.activarComponentes(jPanelOpcion, true);  
        btnUsuario.setEnabled(true);
        btnAlumno.setEnabled(true);
        btnCurso.setEnabled(true);
        btnColegio.setEnabled(true);
        btnReserva.setEnabled(true);
        btnContrato.setEnabled(true);
        btnTipoContrato.setEnabled(true);
        btnPrograma.setEnabled(true);
        btnServicio.setEnabled(true);
        btnHospedaje.setEnabled(true);
        btnTraslado.setEnabled(true);
        btnTipoTraslado.setEnabled(true);
        btnEvento.setEnabled(true);
        btnTipoEvento.setEnabled(true);
        btnDestino.setEnabled(true);
        btnCiudad.setEnabled(true);
        btnPais.setEnabled(true);
        btnDeposito.setEnabled(false);
        btnSeguro.setEnabled(false);
        btnTipoSeguro.setEnabled(false);
        btnAgencia.setEnabled(true);
    } else if (Administrador.rol.equals("EJE")) {
        Funciones.activarComponentes(jPanelOpcion, true);        
        btnUsuario.setEnabled(false);
        btnAlumno.setEnabled(false);
        btnCurso.setEnabled(false);
        btnColegio.setEnabled(false);
        btnReserva.setEnabled(true);
        btnContrato.setEnabled(true);
        btnTipoContrato.setEnabled(false);
        btnPrograma.setEnabled(false);
        btnServicio.setEnabled(false);
        btnHospedaje.setEnabled(false);
        btnTraslado.setEnabled(false);
        btnTipoTraslado.setEnabled(false);
        btnEvento.setEnabled(false);
        btnTipoEvento.setEnabled(false);
        btnDestino.setEnabled(false);
        btnCiudad.setEnabled(false);
        btnPais.setEnabled(false);
        btnDeposito.setEnabled(false);
        btnSeguro.setEnabled(true);
        btnTipoSeguro.setEnabled(true);
        btnAgencia.setEnabled(false);
    }
}//GEN-LAST:event_btnAjustesActionPerformed

    private void btnUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsuarioActionPerformed
        switch (opcion) {
            case 1:
                registrar_usuario = new RegistrarUsuario();
                registrar_usuario.setVisible(true);
                break;
            case 2:
                consultar_usuario = new ConsultarUsuario();
                consultar_usuario.setVisible(true);
                break;
            case 3:
                ajustar_usuario = new AjustarUsuario();
                ajustar_usuario.setVisible(true);
                break;
            default:
                break;
        }
        Funciones.activarComponentes(jPanelOpcion, false);
        jPanelOpcion.setBackground(Color.gray);
    }//GEN-LAST:event_btnUsuarioActionPerformed

private void btnPaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaisActionPerformed
    switch (opcion) {
        case 1:
            registrar_pais = new RegistrarPais();
            registrar_pais.setVisible(true);
            break;
        case 2:
            consultar_pais = new ConsultarPais();
            consultar_pais.setVisible(true);
            break;
        case 3:
            ajustar_pais = new AjustarPais();
            ajustar_pais.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);

}//GEN-LAST:event_btnPaisActionPerformed

private void btnCursoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCursoActionPerformed
    switch (opcion) {
        case 1:
            registrar_curso = new RegistrarCurso();
            registrar_curso.setVisible(true);
            break;
        case 2:
            consultar_curso = new ConsultarCurso();
            consultar_curso.setVisible(true);
            break;
        case 3:
            ajustar_curso = new AjustarCurso();
            ajustar_curso.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);
}//GEN-LAST:event_btnCursoActionPerformed

private void btnColegioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnColegioActionPerformed
    switch (opcion) {
        case 1:
            registrar_colegio = new RegistrarColegio();
            registrar_colegio.setVisible(true);
            break;
        case 2:
            consultar_colegio = new ConsultarColegio();
            consultar_colegio.setVisible(true);
            break;
        case 3:
            ajustar_colegio = new AjustarColegio();
            ajustar_colegio.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);
}//GEN-LAST:event_btnColegioActionPerformed

private void btnAlumnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAlumnoActionPerformed

    switch (opcion) {
        case 1:
            registrar_alumno = new RegistrarAlumno();
            registrar_alumno.setVisible(true);
            break;
        case 2:
            consultar_alumno = new ConsultarAlumno();
            consultar_alumno.setVisible(true);
            break;
        case 3:
            ajustar_alumno = new AjustarAlumno();
            ajustar_alumno.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);
}//GEN-LAST:event_btnAlumnoActionPerformed

private void btnReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReservaActionPerformed
    switch (opcion) {
        case 1:
            registrar_reserva = new RegistrarReserva();
            registrar_reserva.setVisible(true);
            break;
        case 2:
            consultar_reserva = new ConsultarReserva();
            consultar_reserva.setVisible(true);
            break;
        case 3:
            ajustar_reserva = new AjustarReserva();
            ajustar_reserva.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);
}//GEN-LAST:event_btnReservaActionPerformed

private void btnContratoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnContratoActionPerformed
    switch (opcion) {
        case 1:
            registrar_contrato = new RegistrarContrato();
            registrar_contrato.setVisible(true);
            break;
        case 2:
            consultar_contrato = new ConsultarContrato();
            consultar_contrato.setVisible(true);
            break;
        case 3:
            ajustar_contrato = new AjustarContrato();
            ajustar_contrato.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);
}//GEN-LAST:event_btnContratoActionPerformed

private void btnProgramaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProgramaActionPerformed
    switch (opcion) {
        case 1:
            registrar_programa = new RegistrarPrograma();
            registrar_programa.setVisible(true);
            break;
        case 2:
            consultar_programa = new ConsultarPrograma();
            consultar_programa.setVisible(true);
            break;
        case 3:
            ajustar_programa = new AjustarPrograma();
            ajustar_programa.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);
}//GEN-LAST:event_btnProgramaActionPerformed

private void btnServicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnServicioActionPerformed
    switch (opcion) {
        case 1:
            registrar_servicio = new RegistrarServicio();
            registrar_servicio.setVisible(true);
            break;
        case 2:
            consultar_servico = new ConsultarServicio();
            consultar_servico.setVisible(true);
            break;
        case 3:
            ajustar_servicio = new AjustarServicio();
            ajustar_servicio.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);
}//GEN-LAST:event_btnServicioActionPerformed

private void btnTrasladoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTrasladoActionPerformed
    switch (opcion) {
        case 1:
            registrar_traslado = new RegistrarTraslado();
            registrar_traslado.setVisible(true);
            break;
        case 2:
            consultar_traslado = new ConsultarTraslado();
            consultar_traslado.setVisible(true);
            break;
        case 3:
            ajustar_traslado = new AjustarTraslado();
            ajustar_traslado.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);
}//GEN-LAST:event_btnTrasladoActionPerformed

private void btnDestinoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDestinoActionPerformed
    switch (opcion) {
        case 1:
            registrar_destino = new RegistrarDestino();
            registrar_destino.setVisible(true);
            break;
        case 2:
            consultar_destino = new ConsultarDestino();
            consultar_destino.setVisible(true);
            break;
        case 3:
            ajustar_destino = new AjustarDestino();
            ajustar_destino.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);
}//GEN-LAST:event_btnDestinoActionPerformed

private void btnCiudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCiudadActionPerformed
    switch (opcion) {
        case 1:
            registrar_ciudad = new RegistrarCiudad();
            registrar_ciudad.setVisible(true);
            break;
        case 2:
            consultar_ciudad = new ConsultarCiudad();
            consultar_ciudad.setVisible(true);
            break;
        case 3:
            ajustar_ciudad = new AjustarCiudad();
            ajustar_ciudad.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);
}//GEN-LAST:event_btnCiudadActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        dispose();
        Administrador.key = "";
        new Login().setVisible(true);
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnTipoContratoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTipoContratoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnTipoContratoActionPerformed

    private void btnHospedajeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHospedajeActionPerformed
        switch (opcion) {
        case 1:
            registrar_ciudad = new RegistrarCiudad();
            registrar_ciudad.setVisible(true);
            break;
        case 2:
            consultar_ciudad = new ConsultarCiudad();
            consultar_ciudad.setVisible(true);
            break;
        case 3:
            ajustar_ciudad = new AjustarCiudad();
            ajustar_ciudad.setVisible(true);
            break;
        default:
            break;
    }
    Funciones.activarComponentes(jPanelOpcion, false);
    jPanelOpcion.setBackground(Color.gray);
    }//GEN-LAST:event_btnHospedajeActionPerformed

    private void btnTipoTrasladoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTipoTrasladoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnTipoTrasladoActionPerformed

    private void btnAgenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgenciaActionPerformed
        switch (opcion) {
            case 1:
                break;
            case 2:
                consultar_hospedaje = new ConsultarHospedaje();
                consultar_hospedaje.setVisible(true);
                break;
            case 3:
                break;
            default:
                break;
        }
        Funciones.activarComponentes(jPanelOpcion, false);
        jPanelOpcion.setBackground(Color.gray);
    }//GEN-LAST:event_btnAgenciaActionPerformed

    private void btnSeguroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeguroActionPerformed
        switch (opcion) {
            case 1:
                registrar_seguro = new RegistrarSeguro();
                registrar_seguro.setVisible(true);
                break;
            case 2:
                consultar_seguro = new ConsultarSeguro();
                consultar_seguro.setVisible(true);
                break;
            case 3:
                ajustar_seguro = new AjustarSeguro();
                ajustar_seguro.setVisible(true);
                break;
            default:
                break;
        }
        Funciones.activarComponentes(jPanelOpcion, false);
        jPanelOpcion.setBackground(Color.gray);
    }//GEN-LAST:event_btnSeguroActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new Home().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgencia;
    private javax.swing.JButton btnAjustes;
    private javax.swing.JButton btnAlumno;
    private javax.swing.JButton btnCiudad;
    private javax.swing.JButton btnColegio;
    private javax.swing.JButton btnConsulta;
    private javax.swing.JButton btnContrato;
    private javax.swing.JButton btnCurso;
    private javax.swing.JButton btnDeposito;
    private javax.swing.JButton btnDestino;
    private javax.swing.JButton btnEvento;
    private javax.swing.JButton btnHospedaje;
    private javax.swing.JButton btnPais;
    private javax.swing.JButton btnPrograma;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JButton btnReserva;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnSeguro;
    private javax.swing.JButton btnServicio;
    private javax.swing.JButton btnTipoContrato;
    private javax.swing.JButton btnTipoEvento;
    private javax.swing.JButton btnTipoSeguro;
    private javax.swing.JButton btnTipoTraslado;
    private javax.swing.JButton btnTraslado;
    private javax.swing.JButton btnUsuario;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    public static javax.swing.JPanel jPanelAccion;
    public static javax.swing.JPanel jPanelOpcion;
    // End of variables declaration//GEN-END:variables

}
