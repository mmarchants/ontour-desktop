package cl.ontour.frontend.desktop.views.read;

import cl.ontour.backend.desktop.domains.Alumno;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Andrea
 */
public class ConsultarAlumno extends javax.swing.JFrame {

    private DefaultTableModel tablaAlumnos;
    int cont = 0;

    public ConsultarAlumno() {
        initComponents();
        setLocationRelativeTo(null);
//        btnMostrarDatos.doClick();

        //mientras escondo el panel de buscar por nombre e ID
        for (int i = 0; i < jPanelBuscarAlumno.getComponents().length; i++) {
            jPanelBuscarAlumno.getComponent(i).setEnabled(false);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTableAlumnos = new javax.swing.JTable();
        btnMostrarDatos = new javax.swing.JButton();
        jPanelBuscarAlumno = new javax.swing.JPanel();
        txtBuscarNombre = new javax.swing.JTextField();
        btnBuscarNombre = new javax.swing.JButton();
        lblBuscarNombre = new javax.swing.JLabel();
        lblBuscarId = new javax.swing.JLabel();
        txtBuscarId = new javax.swing.JTextField();
        btnBuscarId = new javax.swing.JButton();
        lblNombreAlumno = new javax.swing.JLabel();
        lblIdAlumno = new javax.swing.JLabel();
        lblCodigoAlumno = new javax.swing.JLabel();
        lblEstadoAlumno = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        idAlumno = new javax.swing.JLabel();
        nomAlumno = new javax.swing.JLabel();
        estAlumno = new javax.swing.JLabel();
        codAlumno = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        btnCancelar1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Buscar Alumnos");

        jTableAlumnos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableAlumnos);

        btnMostrarDatos.setText("Mostrar datos");
        btnMostrarDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarDatosActionPerformed(evt);
            }
        });

        jPanelBuscarAlumno.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Buscar Alumno"));

        btnBuscarNombre.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarNombre.setText("Buscar");
        btnBuscarNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarNombreActionPerformed(evt);
            }
        });

        lblBuscarNombre.setText("Buscar por nombre:");

        lblBuscarId.setText("Buscar por Id:");

        btnBuscarId.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarId.setText("Buscar");
        btnBuscarId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIdActionPerformed(evt);
            }
        });

        lblNombreAlumno.setText("Nombre");

        lblIdAlumno.setText("Id:");

        lblCodigoAlumno.setText("Código:");

        lblEstadoAlumno.setText("Estado:");

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        idAlumno.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        idAlumno.setForeground(new java.awt.Color(153, 0, 0));

        nomAlumno.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        nomAlumno.setForeground(new java.awt.Color(153, 0, 0));

        estAlumno.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        estAlumno.setForeground(new java.awt.Color(153, 0, 0));

        codAlumno.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        codAlumno.setForeground(new java.awt.Color(153, 0, 0));

        org.jdesktop.layout.GroupLayout jPanelBuscarAlumnoLayout = new org.jdesktop.layout.GroupLayout(jPanelBuscarAlumno);
        jPanelBuscarAlumno.setLayout(jPanelBuscarAlumnoLayout);
        jPanelBuscarAlumnoLayout.setHorizontalGroup(
            jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBuscarAlumnoLayout.createSequentialGroup()
                .add(26, 26, 26)
                .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarAlumnoLayout.createSequentialGroup()
                        .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jSeparator1)
                            .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(lblNombreAlumno)
                                .add(jPanelBuscarAlumnoLayout.createSequentialGroup()
                                    .add(8, 8, 8)
                                    .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                        .add(jPanelBuscarAlumnoLayout.createSequentialGroup()
                                            .add(lblIdAlumno)
                                            .add(58, 58, 58)
                                            .add(idAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(nomAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(55, 55, 55)
                                    .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(jPanelBuscarAlumnoLayout.createSequentialGroup()
                                            .add(lblEstadoAlumno)
                                            .add(18, 18, 18)
                                            .add(estAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(jPanelBuscarAlumnoLayout.createSequentialGroup()
                                            .add(lblCodigoAlumno)
                                            .add(18, 18, 18)
                                            .add(codAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))))
                        .add(38, 38, 38))
                    .add(jPanelBuscarAlumnoLayout.createSequentialGroup()
                        .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(lblBuscarId)
                            .add(lblBuscarNombre))
                        .add(35, 35, 35)
                        .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(txtBuscarNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(32, 32, 32)
                        .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarNombre)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarId))
                        .add(238, 238, 238))))
        );
        jPanelBuscarAlumnoLayout.setVerticalGroup(
            jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarAlumnoLayout.createSequentialGroup()
                .add(20, 20, 20)
                .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarNombre)
                    .add(lblBuscarNombre))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarId)
                    .add(lblBuscarId))
                .add(18, 18, 18)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblIdAlumno)
                    .add(lblCodigoAlumno)
                    .add(idAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(codAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelBuscarAlumnoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblNombreAlumno)
                    .add(lblEstadoAlumno)
                    .add(nomAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(estAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        jLabel1.setText("Listado de Alumnos");

        btnCancelar1.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar1.setText("Volver");
        btnCancelar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelar1ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(35, 35, 35)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanelBuscarAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(btnMostrarDatos)
                            .add(layout.createSequentialGroup()
                                .add(jLabel1)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 711, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(11, 11, 11))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(btnCancelar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 129, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 830, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(35, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(45, 45, 45)
                .add(jPanelBuscarAlumno, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 41, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel1)
                    .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btnMostrarDatos)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 202, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(31, 31, 31)
                .add(btnCancelar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnMostrarDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarDatosActionPerformed
    String data[][] = {};
    String col[] = {"ID ALUMNO", "RUT", "NOMBRE", "APELLIDO 1", "APELLIDO 2", "CORREO", "ESTADO"};
    tablaAlumnos = new DefaultTableModel(data, col);
    jTableAlumnos.setModel(tablaAlumnos);
    tablaAlumnos = Alumno.listarAlumnos(tablaAlumnos);
}//GEN-LAST:event_btnMostrarDatosActionPerformed

private void btnBuscarNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarNombreActionPerformed

    txtBuscarNombre.setText("");

}//GEN-LAST:event_btnBuscarNombreActionPerformed

private void btnBuscarIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIdActionPerformed

    txtBuscarId.setText("");

}//GEN-LAST:event_btnBuscarIdActionPerformed

    private void btnCancelar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelar1ActionPerformed

        dispose();

    }//GEN-LAST:event_btnCancelar1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultarAlumno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultarAlumno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultarAlumno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultarAlumno.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new ConsultarAlumno().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarId;
    private javax.swing.JButton btnBuscarNombre;
    private javax.swing.JButton btnCancelar1;
    private javax.swing.JButton btnMostrarDatos;
    private javax.swing.JLabel codAlumno;
    private javax.swing.JLabel estAlumno;
    private javax.swing.JLabel idAlumno;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelBuscarAlumno;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable jTableAlumnos;
    private javax.swing.JLabel lblBuscarId;
    private javax.swing.JLabel lblBuscarNombre;
    private javax.swing.JLabel lblCodigoAlumno;
    private javax.swing.JLabel lblEstadoAlumno;
    private javax.swing.JLabel lblIdAlumno;
    private javax.swing.JLabel lblNombreAlumno;
    private javax.swing.JLabel nomAlumno;
    private javax.swing.JTextField txtBuscarId;
    private javax.swing.JTextField txtBuscarNombre;
    // End of variables declaration//GEN-END:variables

}
