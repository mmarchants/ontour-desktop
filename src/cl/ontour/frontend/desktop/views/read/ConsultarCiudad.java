package cl.ontour.frontend.desktop.views.read;

import javax.swing.table.DefaultTableModel;
import cl.ontour.backend.desktop.domains.Ciudad;
import cl.ontour.frontend.desktop.Administrador;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Andrea
 */
public class ConsultarCiudad extends javax.swing.JFrame {

    private DefaultTableModel tablaCiudades;
    int cont = 0;

    public ConsultarCiudad() {
        initComponents();
        setLocationRelativeTo(null);
        btnMostrarDatos.doClick();
    }
    
    private void limpiarCampos() {
        txtBuscarId.setText("");
        txtBuscarCodigo.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTableCiudades = new javax.swing.JTable();
        btnMostrarDatos = new javax.swing.JButton();
        jPanelBuscarCiudad = new javax.swing.JPanel();
        txtBuscarId = new javax.swing.JTextField();
        btnBuscarId = new javax.swing.JButton();
        lblBuscarId = new javax.swing.JLabel();
        lblBuscarCodigo = new javax.swing.JLabel();
        txtBuscarCodigo = new javax.swing.JTextField();
        btnBuscarCodigo = new javax.swing.JButton();
        lblNombreCiudad = new javax.swing.JLabel();
        lblIdCiudad = new javax.swing.JLabel();
        lblCodigoCiudad = new javax.swing.JLabel();
        lblEstadoCiudad = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        idCiudad = new javax.swing.JLabel();
        nomCiudad = new javax.swing.JLabel();
        estCiudad = new javax.swing.JLabel();
        codCiudad = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        paisCiudad = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Buscar Ciudades");

        jTableCiudades.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableCiudades);

        btnMostrarDatos.setText("Mostrar datos");
        btnMostrarDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarDatosActionPerformed(evt);
            }
        });

        jPanelBuscarCiudad.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Buscar ciudad"));

        btnBuscarId.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarId.setText("Buscar");
        btnBuscarId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIdActionPerformed(evt);
            }
        });

        lblBuscarId.setText("Buscar por ID:");

        lblBuscarCodigo.setText("Buscar por código:");

        btnBuscarCodigo.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarCodigo.setText("Buscar");
        btnBuscarCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarCodigoActionPerformed(evt);
            }
        });

        lblNombreCiudad.setText("Nombre:");

        lblIdCiudad.setText("ID:");

        lblCodigoCiudad.setText("Código:");

        lblEstadoCiudad.setText("Estado:");

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        idCiudad.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        idCiudad.setForeground(new java.awt.Color(153, 0, 0));

        nomCiudad.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        nomCiudad.setForeground(new java.awt.Color(153, 0, 0));

        estCiudad.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        estCiudad.setForeground(new java.awt.Color(153, 0, 0));

        codCiudad.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        codCiudad.setForeground(new java.awt.Color(153, 0, 0));

        jLabel2.setText("País:");

        paisCiudad.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        paisCiudad.setForeground(new java.awt.Color(153, 0, 0));

        org.jdesktop.layout.GroupLayout jPanelBuscarCiudadLayout = new org.jdesktop.layout.GroupLayout(jPanelBuscarCiudad);
        jPanelBuscarCiudad.setLayout(jPanelBuscarCiudadLayout);
        jPanelBuscarCiudadLayout.setHorizontalGroup(
            jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                .add(26, 26, 26)
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel2)
                    .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                            .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, jSeparator1)
                                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(lblNombreCiudad)
                                    .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                                        .add(8, 8, 8)
                                        .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                            .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                                                .add(lblIdCiudad)
                                                .add(58, 58, 58)
                                                .add(idCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                            .add(nomCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                            .add(paisCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(55, 55, 55)
                                        .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                            .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                                                .add(lblEstadoCiudad)
                                                .add(18, 18, 18)
                                                .add(estCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                            .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                                                .add(lblCodigoCiudad)
                                                .add(18, 18, 18)
                                                .add(codCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))))
                            .add(38, 38, 38))
                        .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                            .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(lblBuscarCodigo)
                                .add(lblBuscarId))
                            .add(35, 35, 35)
                            .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(txtBuscarCodigo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(32, 32, 32)
                            .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarId)
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarCodigo))
                            .add(238, 238, 238)))))
        );
        jPanelBuscarCiudadLayout.setVerticalGroup(
            jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarCiudadLayout.createSequentialGroup()
                .add(20, 20, 20)
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarId)
                    .add(lblBuscarId))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarCodigo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarCodigo)
                    .add(lblBuscarCodigo))
                .add(18, 18, 18)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblIdCiudad)
                    .add(lblCodigoCiudad)
                    .add(idCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(codCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblNombreCiudad)
                    .add(lblEstadoCiudad)
                    .add(nomCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(estCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                        .add(18, 18, 18)
                        .add(jLabel2))
                    .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(paisCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        jLabel1.setText("Listado de ciudades");

        btnCancelar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar.setText("Volver");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 830, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(layout.createSequentialGroup()
                            .add(42, 42, 42)
                            .add(btnCancelar))
                        .add(layout.createSequentialGroup()
                            .add(57, 57, 57)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                .add(jPanelBuscarCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(layout.createSequentialGroup()
                                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                        .add(btnMostrarDatos)
                                        .add(layout.createSequentialGroup()
                                            .add(jLabel1)
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 684, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                    .add(11, 11, 11))))))
                .addContainerGap(35, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(45, 45, 45)
                .add(jPanelBuscarCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 30, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel1))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btnMostrarDatos)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 202, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(btnCancelar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(38, 38, 38))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnMostrarDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarDatosActionPerformed
    if (!Administrador.error.isEmpty()) {
        JOptionPane.showMessageDialog(null, Administrador.error);
        Administrador.error = "";
    } else {
        String data[][] = {};
        String col[] = {"ID CIUDAD", "CÓDIGO", "NOMBRE", "PAÍS", "ESTADO"};
        tablaCiudades = new DefaultTableModel(data, col);
        jTableCiudades.setModel(tablaCiudades);
        List<Ciudad> listado = Ciudad.listadoCiudades();
        for (int i = 0; i < listado.size(); i++) {         
            tablaCiudades.insertRow(i, new Object[]{});
            tablaCiudades.setValueAt(listado.get(i).getId_ciudad(), i, 0);
            tablaCiudades.setValueAt(listado.get(i).getCodigo_ciudad(), i, 1);
            tablaCiudades.setValueAt(listado.get(i).getNombre_ciudad(), i, 2);
            tablaCiudades.setValueAt(listado.get(i).getPais_ciudad().toString(), i, 3);
            tablaCiudades.setValueAt(listado.get(i).isEstado_ciudad() ? "Activo" : "Inactivo", i, 4);                
        }
    }
}//GEN-LAST:event_btnMostrarDatosActionPerformed

private void btnBuscarIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIdActionPerformed
    if (!txtBuscarId.getText().trim().isEmpty()) {
        String c_id = txtBuscarId.getText().trim();
        Ciudad ciudad = Ciudad.buscarPorId(c_id);
        if (ciudad != null) {
            limpiarCampos();
            idCiudad.setText(String.valueOf(ciudad.getId_ciudad()));
            nomCiudad.setText(ciudad.getNombre_ciudad());
            codCiudad.setText(ciudad.getCodigo_ciudad());  
            paisCiudad.setText(ciudad.getPais_ciudad().toString());
            estCiudad.setText(ciudad.isEstado_ciudad() ? "Activo" : "Inactivo");
        } else {
            JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
        }
    }
}//GEN-LAST:event_btnBuscarIdActionPerformed

private void btnBuscarCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarCodigoActionPerformed
    if (!txtBuscarCodigo.getText().trim().isEmpty()) {
        String c_codigo = txtBuscarCodigo.getText().trim();
        Ciudad ciudad = Ciudad.buscarPorCodigo(c_codigo);
        if (ciudad != null) {
            limpiarCampos();
            idCiudad.setText(String.valueOf(ciudad.getId_ciudad()));
            nomCiudad.setText(ciudad.getNombre_ciudad());
            codCiudad.setText(ciudad.getCodigo_ciudad());  
            paisCiudad.setText(ciudad.getPais_ciudad().toString());
            estCiudad.setText(ciudad.isEstado_ciudad() ? "Activo" : "Inactivo");
        } else {
            JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
        }
    }
}//GEN-LAST:event_btnBuscarCodigoActionPerformed

private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
    dispose();
}//GEN-LAST:event_btnCancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new ConsultarCiudad().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarCodigo;
    private javax.swing.JButton btnBuscarId;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnMostrarDatos;
    private javax.swing.JLabel codCiudad;
    private javax.swing.JLabel estCiudad;
    private javax.swing.JLabel idCiudad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanelBuscarCiudad;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable jTableCiudades;
    private javax.swing.JLabel lblBuscarCodigo;
    private javax.swing.JLabel lblBuscarId;
    private javax.swing.JLabel lblCodigoCiudad;
    private javax.swing.JLabel lblEstadoCiudad;
    private javax.swing.JLabel lblIdCiudad;
    private javax.swing.JLabel lblNombreCiudad;
    private javax.swing.JLabel nomCiudad;
    private javax.swing.JLabel paisCiudad;
    private javax.swing.JTextField txtBuscarCodigo;
    private javax.swing.JTextField txtBuscarId;
    // End of variables declaration//GEN-END:variables

}
