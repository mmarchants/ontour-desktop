package cl.ontour.frontend.desktop.views.read;

import cl.ontour.backend.desktop.domains.Colegio;
import cl.ontour.frontend.desktop.Administrador;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Andrea
 */
public class ConsultarColegio extends javax.swing.JFrame {

    private DefaultTableModel tablaColegios;
    int cont = 0;

    public ConsultarColegio() {
        initComponents();
        setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/cl/ontour/frontend/desktop/resources/images/logo_ontour_icon.png")));
        btnMostrarDatos.doClick();
    }
    
    private void limpiarCampos() {
        txtBuscarId.setText("");
        txtBuscarCodigo.setText("");
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTableColegio = new javax.swing.JTable();
        btnMostrarDatos = new javax.swing.JButton();
        jPanelBuscarColegio = new javax.swing.JPanel();
        txtBuscarId = new javax.swing.JTextField();
        btnBuscarId = new javax.swing.JButton();
        lblBuscarId = new javax.swing.JLabel();
        lblBuscarCodigo = new javax.swing.JLabel();
        txtBuscarCodigo = new javax.swing.JTextField();
        btnBuscarCodigo = new javax.swing.JButton();
        lblNombreColegio = new javax.swing.JLabel();
        lblIdColegio = new javax.swing.JLabel();
        lblCodigoColegio = new javax.swing.JLabel();
        lblEstadoColegio = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        idColegio = new javax.swing.JLabel();
        nomColegio = new javax.swing.JLabel();
        estColegio = new javax.swing.JLabel();
        codColegio = new javax.swing.JLabel();
        lblDireccion = new javax.swing.JLabel();
        direccionColegio = new javax.swing.JLabel();
        lblTelefono = new javax.swing.JLabel();
        telefonoColegio = new javax.swing.JLabel();
        lblCorreo = new javax.swing.JLabel();
        correoColegio = new javax.swing.JLabel();
        lblCiudad = new javax.swing.JLabel();
        ciudadColegio = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        btnCancelar1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Consulta de colegios");

        jTableColegio.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableColegio);

        btnMostrarDatos.setText("Mostrar datos");
        btnMostrarDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarDatosActionPerformed(evt);
            }
        });

        jPanelBuscarColegio.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Buscar colegio"));

        btnBuscarId.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarId.setText("Buscar");
        btnBuscarId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIdActionPerformed(evt);
            }
        });

        lblBuscarId.setText("Buscar por ID:");

        lblBuscarCodigo.setText("Buscar por código:");

        btnBuscarCodigo.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarCodigo.setText("Buscar");
        btnBuscarCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarCodigoActionPerformed(evt);
            }
        });

        lblNombreColegio.setText("Nombre:");

        lblIdColegio.setText("ID:");

        lblCodigoColegio.setText("Código:");

        lblEstadoColegio.setText("Estado:");

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        idColegio.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        idColegio.setForeground(new java.awt.Color(153, 0, 0));

        nomColegio.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        nomColegio.setForeground(new java.awt.Color(153, 0, 0));

        estColegio.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        estColegio.setForeground(new java.awt.Color(153, 0, 0));

        codColegio.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        codColegio.setForeground(new java.awt.Color(153, 0, 0));

        lblDireccion.setText("Dirección:");

        lblTelefono.setText("Teléfono:");

        lblCorreo.setText("Correo electrónico:");

        lblCiudad.setText("Ciudad:");

        org.jdesktop.layout.GroupLayout jPanelBuscarColegioLayout = new org.jdesktop.layout.GroupLayout(jPanelBuscarColegio);
        jPanelBuscarColegio.setLayout(jPanelBuscarColegioLayout);
        jPanelBuscarColegioLayout.setHorizontalGroup(
            jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBuscarColegioLayout.createSequentialGroup()
                .add(26, 26, 26)
                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarColegioLayout.createSequentialGroup()
                        .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jSeparator1)
                            .add(jPanelBuscarColegioLayout.createSequentialGroup()
                                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(lblCiudad)
                                    .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                            .add(jPanelBuscarColegioLayout.createSequentialGroup()
                                                .add(8, 8, 8)
                                                .add(lblIdColegio))
                                            .add(jPanelBuscarColegioLayout.createSequentialGroup()
                                                .add(4, 4, 4)
                                                .add(lblNombreColegio)))
                                        .add(lblTelefono)))
                                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                    .add(jPanelBuscarColegioLayout.createSequentialGroup()
                                        .add(28, 28, 28)
                                        .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                            .add(telefonoColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .add(org.jdesktop.layout.GroupLayout.TRAILING, nomColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .add(org.jdesktop.layout.GroupLayout.TRAILING, idColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)))
                                    .add(jPanelBuscarColegioLayout.createSequentialGroup()
                                        .add(18, 18, 18)
                                        .add(ciudadColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(jPanelBuscarColegioLayout.createSequentialGroup()
                                            .add(55, 55, 55)
                                            .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                                .add(lblCodigoColegio)
                                                .add(lblDireccion)))
                                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarColegioLayout.createSequentialGroup()
                                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                            .add(lblCorreo)))
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarColegioLayout.createSequentialGroup()
                                        .add(70, 70, 70)
                                        .add(lblEstadoColegio)))
                                .add(18, 18, 18)
                                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(codColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                                    .add(direccionColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, estColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(correoColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .add(38, 38, 38))
                    .add(jPanelBuscarColegioLayout.createSequentialGroup()
                        .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(lblBuscarCodigo)
                            .add(lblBuscarId))
                        .add(35, 35, 35)
                        .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(txtBuscarCodigo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(32, 32, 32)
                        .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarId)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarCodigo))
                        .add(238, 238, 238))))
        );
        jPanelBuscarColegioLayout.setVerticalGroup(
            jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarColegioLayout.createSequentialGroup()
                .add(20, 20, 20)
                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarId)
                    .add(lblBuscarId))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarCodigo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarCodigo)
                    .add(lblBuscarCodigo))
                .add(18, 18, 18)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblIdColegio)
                    .add(lblCodigoColegio)
                    .add(idColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(codColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblNombreColegio)
                    .add(nomColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblDireccion)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, direccionColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanelBuscarColegioLayout.createSequentialGroup()
                            .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                .add(lblTelefono)
                                .add(telefonoColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(14, 14, 14))
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarColegioLayout.createSequentialGroup()
                            .add(lblCorreo)
                            .add(18, 18, 18)))
                    .add(jPanelBuscarColegioLayout.createSequentialGroup()
                        .add(correoColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)))
                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(estColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
                        .add(lblEstadoColegio)
                        .add(ciudadColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(lblCiudad))
                .addContainerGap())
        );

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        jLabel1.setText("Listado de colegios");

        btnCancelar1.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar1.setText("Volver");
        btnCancelar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelar1ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(35, 35, 35)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanelBuscarColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(btnMostrarDatos)
                            .add(layout.createSequentialGroup()
                                .add(jLabel1)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 711, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(11, 11, 11))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(btnCancelar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 129, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 830, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(38, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(45, 45, 45)
                .add(jPanelBuscarColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel1)
                    .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btnMostrarDatos)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 202, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(31, 31, 31)
                .add(btnCancelar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnMostrarDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarDatosActionPerformed
    if (!Administrador.error.isEmpty()) {
        JOptionPane.showMessageDialog(null, Administrador.error);
        Administrador.error = "";
    } else {
        String data[][] = {};
        String col[] = {"ID COLEGIO", "CÓDIGO", "NOMBRE", "DIRECCIÓN", "TELÉFONO", "CORREO ELECTRÓNICO", "CIUDAD", "ESTADO"};
        tablaColegios = new DefaultTableModel(data, col);
        jTableColegio.setModel(tablaColegios);
        List<Colegio> listado = Colegio.listadoColegios();
        for (int i = 0; i < listado.size(); i++) {         
            tablaColegios.insertRow(i, new Object[]{});
            tablaColegios.setValueAt(listado.get(i).getId(), i, 0);
            tablaColegios.setValueAt(listado.get(i).getCodigo(), i, 1);
            tablaColegios.setValueAt(listado.get(i).getNombre(), i, 2);
            tablaColegios.setValueAt(listado.get(i).getDireccion(), i, 3);
            if (listado.get(i).getTelefono().isEmpty() || listado.get(i).getTelefono().equals("null")) {
                tablaColegios.setValueAt("No especificado", i, 4);                
            } else {
                tablaColegios.setValueAt(listado.get(i).getTelefono(), i, 4);                
            }
            if (listado.get(i).getCorreo_electronico().isEmpty() || listado.get(i).getCorreo_electronico().equals("null")) {
                tablaColegios.setValueAt("No especificado", i, 5);
            } else {
                tablaColegios.setValueAt(listado.get(i).getCorreo_electronico(), i, 5);
            }
            tablaColegios.setValueAt(listado.get(i).getCiudad().toString(), i, 6);            
            tablaColegios.setValueAt(listado.get(i).isEstado() ? "Activo" : "Inactivo", i, 7);                
        }
    }    
}//GEN-LAST:event_btnMostrarDatosActionPerformed

private void btnBuscarIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIdActionPerformed
    if (!txtBuscarId.getText().trim().isEmpty()) {
        String c_id = txtBuscarId.getText().trim();
        Colegio colegio = Colegio.buscarPorId(c_id);
        if (colegio != null) {
            limpiarCampos();
            idColegio.setText(String.valueOf(colegio.getId()));
            codColegio.setText(colegio.getCodigo());
            nomColegio.setText(colegio.getNombre());
            direccionColegio.setText(colegio.getDireccion());
            if (colegio.getTelefono().isEmpty() || colegio.getTelefono().equals("null")) {
                telefonoColegio.setText("No especificado");
            } else {
                telefonoColegio.setText(colegio.getTelefono());
            }
            if (colegio.getCorreo_electronico().isEmpty() || colegio.getCorreo_electronico().equals("null")) {
                correoColegio.setText("No especificado");
            } else {
                correoColegio.setText(colegio.getCorreo_electronico());
            }
            ciudadColegio.setText(colegio.getCiudad().toString());
            estColegio.setText(colegio.isEstado() ? "Activo" : "Inactivo");
        } else {
            JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
        }
    }
}//GEN-LAST:event_btnBuscarIdActionPerformed

private void btnBuscarCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarCodigoActionPerformed
    if (!txtBuscarCodigo.getText().trim().isEmpty()) {
        String c_cod = txtBuscarCodigo.getText().trim();
        Colegio colegio = Colegio.buscarPorCodigo(c_cod);
        if (colegio != null) {
            limpiarCampos();
            idColegio.setText(String.valueOf(colegio.getId()));
            codColegio.setText(colegio.getCodigo());
            nomColegio.setText(colegio.getNombre());
            direccionColegio.setText(colegio.getDireccion());            
            if (colegio.getTelefono().isEmpty() || colegio.getTelefono().equals("null")) {
                telefonoColegio.setText("No especificado");
            } else {
                telefonoColegio.setText(colegio.getTelefono());
            }
            if (colegio.getCorreo_electronico().isEmpty() || colegio.getCorreo_electronico().equals("null")) {
                correoColegio.setText("No especificado");
            } else {
                correoColegio.setText(colegio.getCorreo_electronico());
            }
            ciudadColegio.setText(colegio.getCiudad().toString());
            estColegio.setText(colegio.isEstado() ? "Activo" : "Inactivo");
        } else {
            JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
        }
    }
}//GEN-LAST:event_btnBuscarCodigoActionPerformed

    private void btnCancelar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelar1ActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelar1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new ConsultarColegio().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarCodigo;
    private javax.swing.JButton btnBuscarId;
    private javax.swing.JButton btnCancelar1;
    private javax.swing.JButton btnMostrarDatos;
    private javax.swing.JLabel ciudadColegio;
    private javax.swing.JLabel codColegio;
    private javax.swing.JLabel correoColegio;
    private javax.swing.JLabel direccionColegio;
    private javax.swing.JLabel estColegio;
    private javax.swing.JLabel idColegio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelBuscarColegio;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable jTableColegio;
    private javax.swing.JLabel lblBuscarCodigo;
    private javax.swing.JLabel lblBuscarId;
    private javax.swing.JLabel lblCiudad;
    private javax.swing.JLabel lblCodigoColegio;
    private javax.swing.JLabel lblCorreo;
    private javax.swing.JLabel lblDireccion;
    private javax.swing.JLabel lblEstadoColegio;
    private javax.swing.JLabel lblIdColegio;
    private javax.swing.JLabel lblNombreColegio;
    private javax.swing.JLabel lblTelefono;
    private javax.swing.JLabel nomColegio;
    private javax.swing.JLabel telefonoColegio;
    private javax.swing.JTextField txtBuscarCodigo;
    private javax.swing.JTextField txtBuscarId;
    // End of variables declaration//GEN-END:variables


}
