package cl.ontour.frontend.desktop.views.read;

import javax.swing.table.DefaultTableModel;
import cl.ontour.backend.desktop.domains.Pais;
import cl.ontour.frontend.desktop.Administrador;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Andrea
 */
public class ConsultarPais extends javax.swing.JFrame {

   
    private DefaultTableModel tablaPaises;
    int cont = 0;

    public ConsultarPais() {
        initComponents();
        setLocationRelativeTo(null);
        btnMostrarDatos.doClick();
    }
    
    private void limpiarCampos() {
        txtBuscarId.setText("");
        txtBuscarCodigo.setText("");
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTablePaises = new javax.swing.JTable();
        btnMostrarDatos = new javax.swing.JButton();
        jPanelBuscarPais = new javax.swing.JPanel();
        txtBuscarId = new javax.swing.JTextField();
        btnBuscarId = new javax.swing.JButton();
        lblBuscarNombre = new javax.swing.JLabel();
        lblBuscarId = new javax.swing.JLabel();
        txtBuscarCodigo = new javax.swing.JTextField();
        btnBuscarCodigo = new javax.swing.JButton();
        lblNombrePais = new javax.swing.JLabel();
        lblIdPais = new javax.swing.JLabel();
        lblCodigoPais = new javax.swing.JLabel();
        lblEstadoPais = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        idPais = new javax.swing.JLabel();
        nomPais = new javax.swing.JLabel();
        estPais = new javax.swing.JLabel();
        codPais = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        btnCancelar1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Buscar Paices");

        jTablePaises.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTablePaises);

        btnMostrarDatos.setText("Mostrar datos");
        btnMostrarDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarDatosActionPerformed(evt);
            }
        });

        jPanelBuscarPais.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Buscar país"));

        btnBuscarId.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarId.setText("Buscar");
        btnBuscarId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIdActionPerformed(evt);
            }
        });

        lblBuscarNombre.setText("Buscar por ID:");

        lblBuscarId.setText("Buscar por código:");

        btnBuscarCodigo.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarCodigo.setText("Buscar");
        btnBuscarCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarCodigoActionPerformed(evt);
            }
        });

        lblNombrePais.setText("Nombre:");

        lblIdPais.setText("ID:");

        lblCodigoPais.setText("Código:");

        lblEstadoPais.setText("Estado:");

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        idPais.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        idPais.setForeground(new java.awt.Color(153, 0, 0));

        nomPais.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        nomPais.setForeground(new java.awt.Color(153, 0, 0));

        estPais.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        estPais.setForeground(new java.awt.Color(153, 0, 0));

        codPais.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        codPais.setForeground(new java.awt.Color(153, 0, 0));

        org.jdesktop.layout.GroupLayout jPanelBuscarPaisLayout = new org.jdesktop.layout.GroupLayout(jPanelBuscarPais);
        jPanelBuscarPais.setLayout(jPanelBuscarPaisLayout);
        jPanelBuscarPaisLayout.setHorizontalGroup(
            jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBuscarPaisLayout.createSequentialGroup()
                .add(26, 26, 26)
                .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarPaisLayout.createSequentialGroup()
                        .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jSeparator1)
                            .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(lblNombrePais)
                                .add(jPanelBuscarPaisLayout.createSequentialGroup()
                                    .add(8, 8, 8)
                                    .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                        .add(jPanelBuscarPaisLayout.createSequentialGroup()
                                            .add(lblIdPais)
                                            .add(58, 58, 58)
                                            .add(idPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(nomPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(55, 55, 55)
                                    .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(jPanelBuscarPaisLayout.createSequentialGroup()
                                            .add(lblEstadoPais)
                                            .add(18, 18, 18)
                                            .add(estPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(jPanelBuscarPaisLayout.createSequentialGroup()
                                            .add(lblCodigoPais)
                                            .add(18, 18, 18)
                                            .add(codPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))))
                        .add(38, 38, 38))
                    .add(jPanelBuscarPaisLayout.createSequentialGroup()
                        .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(lblBuscarId)
                            .add(lblBuscarNombre))
                        .add(35, 35, 35)
                        .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(txtBuscarCodigo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(32, 32, 32)
                        .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarId)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarCodigo))
                        .add(238, 238, 238))))
        );
        jPanelBuscarPaisLayout.setVerticalGroup(
            jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarPaisLayout.createSequentialGroup()
                .add(20, 20, 20)
                .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarId)
                    .add(lblBuscarNombre))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarCodigo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarCodigo)
                    .add(lblBuscarId))
                .add(18, 18, 18)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblIdPais)
                    .add(lblCodigoPais)
                    .add(idPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(codPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelBuscarPaisLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblNombrePais)
                    .add(lblEstadoPais)
                    .add(nomPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(estPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        jLabel1.setText("Listado de países");

        btnCancelar1.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar1.setText("Volver");
        btnCancelar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelar1ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(35, 35, 35)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanelBuscarPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(btnMostrarDatos)
                            .add(layout.createSequentialGroup()
                                .add(jLabel1)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 711, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(11, 11, 11))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(btnCancelar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 129, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 830, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(51, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(45, 45, 45)
                .add(jPanelBuscarPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 70, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel1)
                    .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btnMostrarDatos)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 202, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(31, 31, 31)
                .add(btnCancelar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnMostrarDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarDatosActionPerformed
    if (!Administrador.error.isEmpty()) {
        JOptionPane.showMessageDialog(null, Administrador.error);
        Administrador.error = "";
    } else { 
        String data[][] = {};
        String col[] = {"ID PAÍS", "CÓDIGO", "NOMBRE", "ESTADO"};
        tablaPaises = new DefaultTableModel(data, col);
        jTablePaises.setModel(tablaPaises);
        List<Pais> listado = Pais.listadoPaises();
        for (int i = 0; i < listado.size(); i++) {         
            tablaPaises.insertRow(i, new Object[]{});
            tablaPaises.setValueAt(listado.get(i).getId_pais(), i, 0);
            tablaPaises.setValueAt(listado.get(i).getCodigo_pais(), i, 1);
            tablaPaises.setValueAt(listado.get(i).getNombre_pais(), i, 2);
            tablaPaises.setValueAt(listado.get(i).getEstado_pais() ? "Activo" : "Inactivo", i, 3);                
        }
    }
}//GEN-LAST:event_btnMostrarDatosActionPerformed

private void btnBuscarIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIdActionPerformed
    if (!txtBuscarId.getText().trim().isEmpty()) {
        String p_id = txtBuscarId.getText().trim();
        Pais pais = Pais.buscarPorId(p_id);
        if (pais != null) {
            limpiarCampos();
            idPais.setText(String.valueOf(pais.getId_pais()));
            nomPais.setText(pais.getNombre_pais());
            codPais.setText(pais.getCodigo_pais());    
            estPais.setText(pais.getEstado_pais() ? "Activo" : "Inactivo");
        } else {
            JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
        }
    }
}//GEN-LAST:event_btnBuscarIdActionPerformed

private void btnBuscarCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarCodigoActionPerformed
    if (!txtBuscarCodigo.getText().trim().isEmpty()) {
        String p_codigo = txtBuscarCodigo.getText().trim();
        Pais pais = Pais.buscarPorCodigo(p_codigo);
        if (pais != null) {
            limpiarCampos();
            idPais.setText(String.valueOf(pais.getId_pais()));
            nomPais.setText(pais.getNombre_pais());
            codPais.setText(pais.getCodigo_pais());    
            estPais.setText(pais.getEstado_pais() ? "Activo" : "Inactivo");
        } else {
            JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
        }
    }
}//GEN-LAST:event_btnBuscarCodigoActionPerformed

    private void btnCancelar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelar1ActionPerformed
        dispose();        
    }//GEN-LAST:event_btnCancelar1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultarPais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultarPais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultarPais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultarPais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new ConsultarPais().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarCodigo;
    private javax.swing.JButton btnBuscarId;
    private javax.swing.JButton btnCancelar1;
    private javax.swing.JButton btnMostrarDatos;
    private javax.swing.JLabel codPais;
    private javax.swing.JLabel estPais;
    private javax.swing.JLabel idPais;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelBuscarPais;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable jTablePaises;
    private javax.swing.JLabel lblBuscarId;
    private javax.swing.JLabel lblBuscarNombre;
    private javax.swing.JLabel lblCodigoPais;
    private javax.swing.JLabel lblEstadoPais;
    private javax.swing.JLabel lblIdPais;
    private javax.swing.JLabel lblNombrePais;
    private javax.swing.JLabel nomPais;
    private javax.swing.JTextField txtBuscarCodigo;
    private javax.swing.JTextField txtBuscarId;
    // End of variables declaration//GEN-END:variables
}
