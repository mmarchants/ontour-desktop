package cl.ontour.frontend.desktop.views.read;

import javax.swing.table.DefaultTableModel;
import cl.ontour.backend.desktop.domains.Reserva;

/**
 *
 * @author Andrea
 */
public class ConsultarReserva extends javax.swing.JFrame {

    private DefaultTableModel tablaReservas;
    int cont = 0;

    public ConsultarReserva() {
        initComponents();
        setLocationRelativeTo(null);
//        btnMostrarDatos.doClick();

        //mientras escondo el panel de buscar por nombre e ID
        for (int i = 0; i < jPanelBuscarReserva.getComponents().length; i++) {
            jPanelBuscarReserva.getComponent(i).setEnabled(false);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTableReservas = new javax.swing.JTable();
        btnMostrarDatos = new javax.swing.JButton();
        jPanelBuscarReserva = new javax.swing.JPanel();
        txtBuscarNombre = new javax.swing.JTextField();
        btnBuscarNombre = new javax.swing.JButton();
        lblBuscarNombre = new javax.swing.JLabel();
        lblBuscarId = new javax.swing.JLabel();
        txtBuscarId = new javax.swing.JTextField();
        btnBuscarId = new javax.swing.JButton();
        lblNombreReserva = new javax.swing.JLabel();
        lblIdReserva = new javax.swing.JLabel();
        lblCodigoReserva = new javax.swing.JLabel();
        lblEstadoReserva = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        idReserva = new javax.swing.JLabel();
        nomReserva = new javax.swing.JLabel();
        estReserva = new javax.swing.JLabel();
        codReserva = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Buscar Reservas");

        jTableReservas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableReservas);

        btnMostrarDatos.setText("Mostrar datos");
        btnMostrarDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarDatosActionPerformed(evt);
            }
        });

        jPanelBuscarReserva.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Buscar reserva"));

        btnBuscarNombre.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarNombre.setText("Buscar");
        btnBuscarNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarNombreActionPerformed(evt);
            }
        });

        lblBuscarNombre.setText("Buscar por nombre:");

        lblBuscarId.setText("Buscar por Id:");

        btnBuscarId.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarId.setText("Buscar");
        btnBuscarId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIdActionPerformed(evt);
            }
        });

        lblNombreReserva.setText("Nombre");

        lblIdReserva.setText("Id:");

        lblCodigoReserva.setText("Código:");

        lblEstadoReserva.setText("Estado:");

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        idReserva.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        idReserva.setForeground(new java.awt.Color(153, 0, 0));

        nomReserva.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        nomReserva.setForeground(new java.awt.Color(153, 0, 0));

        estReserva.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        estReserva.setForeground(new java.awt.Color(153, 0, 0));

        codReserva.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        codReserva.setForeground(new java.awt.Color(153, 0, 0));

        org.jdesktop.layout.GroupLayout jPanelBuscarReservaLayout = new org.jdesktop.layout.GroupLayout(jPanelBuscarReserva);
        jPanelBuscarReserva.setLayout(jPanelBuscarReservaLayout);
        jPanelBuscarReservaLayout.setHorizontalGroup(
            jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBuscarReservaLayout.createSequentialGroup()
                .add(26, 26, 26)
                .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarReservaLayout.createSequentialGroup()
                        .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jSeparator1)
                            .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(lblNombreReserva)
                                .add(jPanelBuscarReservaLayout.createSequentialGroup()
                                    .add(8, 8, 8)
                                    .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                        .add(jPanelBuscarReservaLayout.createSequentialGroup()
                                            .add(lblIdReserva)
                                            .add(58, 58, 58)
                                            .add(idReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(nomReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(55, 55, 55)
                                    .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(jPanelBuscarReservaLayout.createSequentialGroup()
                                            .add(lblEstadoReserva)
                                            .add(18, 18, 18)
                                            .add(estReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(jPanelBuscarReservaLayout.createSequentialGroup()
                                            .add(lblCodigoReserva)
                                            .add(18, 18, 18)
                                            .add(codReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))))
                        .add(38, 38, 38))
                    .add(jPanelBuscarReservaLayout.createSequentialGroup()
                        .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(lblBuscarId)
                            .add(lblBuscarNombre))
                        .add(35, 35, 35)
                        .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(txtBuscarNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(32, 32, 32)
                        .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarNombre)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarId))
                        .add(238, 238, 238))))
        );
        jPanelBuscarReservaLayout.setVerticalGroup(
            jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarReservaLayout.createSequentialGroup()
                .add(20, 20, 20)
                .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarNombre)
                    .add(lblBuscarNombre))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarId)
                    .add(lblBuscarId))
                .add(18, 18, 18)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblIdReserva)
                    .add(lblCodigoReserva)
                    .add(idReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(codReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelBuscarReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblNombreReserva)
                    .add(lblEstadoReserva)
                    .add(nomReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(estReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        jLabel1.setText("Listado de Reservas");

        btnCancelar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar.setText("Volver");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(35, 35, 35)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanelBuscarReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(btnMostrarDatos)
                            .add(layout.createSequentialGroup()
                                .add(jLabel1)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 711, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(11, 11, 11))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(btnCancelar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 129, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 830, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(36, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(45, 45, 45)
                .add(jPanelBuscarReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 70, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel1)
                    .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btnMostrarDatos)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 202, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(31, 31, 31)
                .add(btnCancelar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnMostrarDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarDatosActionPerformed
    String data[][] = {};
    String col[] = {"ID RESERVA", "CÓDIGO", "DETALLE", "VALOR", "MONTO AVAL", "MONTO ACUMULADO", "FECHA VIAJE", "FECHA LIMITE", "PARTICIPANTES", "ESTADO"};
    tablaReservas = new DefaultTableModel(data, col);
    jTableReservas.setModel(tablaReservas);
    tablaReservas = Reserva.listarReservas(tablaReservas);
}//GEN-LAST:event_btnMostrarDatosActionPerformed

private void btnBuscarNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarNombreActionPerformed

    txtBuscarNombre.setText("");

}//GEN-LAST:event_btnBuscarNombreActionPerformed

private void btnBuscarIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIdActionPerformed

    txtBuscarId.setText("");

}//GEN-LAST:event_btnBuscarIdActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

        dispose();

    }//GEN-LAST:event_btnCancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new ConsultarReserva().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarId;
    private javax.swing.JButton btnBuscarNombre;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnMostrarDatos;
    private javax.swing.JLabel codReserva;
    private javax.swing.JLabel estReserva;
    private javax.swing.JLabel idReserva;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelBuscarReserva;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable jTableReservas;
    private javax.swing.JLabel lblBuscarId;
    private javax.swing.JLabel lblBuscarNombre;
    private javax.swing.JLabel lblCodigoReserva;
    private javax.swing.JLabel lblEstadoReserva;
    private javax.swing.JLabel lblIdReserva;
    private javax.swing.JLabel lblNombreReserva;
    private javax.swing.JLabel nomReserva;
    private javax.swing.JTextField txtBuscarId;
    private javax.swing.JTextField txtBuscarNombre;
    // End of variables declaration//GEN-END:variables

}
