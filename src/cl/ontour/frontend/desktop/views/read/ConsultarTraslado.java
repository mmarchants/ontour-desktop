package cl.ontour.frontend.desktop.views.read;

import javax.swing.table.DefaultTableModel;
import cl.ontour.backend.desktop.domains.Traslado;

/**
 *
 * @author Andrea
 */
public class ConsultarTraslado extends javax.swing.JFrame {

    private DefaultTableModel tablaTraslados;
    int cont = 0;

    public ConsultarTraslado() {
        initComponents();
        setLocationRelativeTo(null);
//        btnMostrarDatos.doClick();

        //mientras escondo el panel de buscar por nombre e ID
        for (int i = 0; i < jPanelBuscarTraslado.getComponents().length; i++) {
            jPanelBuscarTraslado.getComponent(i).setEnabled(false);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTableTraslados = new javax.swing.JTable();
        btnMostrarDatos = new javax.swing.JButton();
        jPanelBuscarTraslado = new javax.swing.JPanel();
        txtBuscarNombre = new javax.swing.JTextField();
        btnBuscarNombre = new javax.swing.JButton();
        lblBuscarNombre = new javax.swing.JLabel();
        lblBuscarId = new javax.swing.JLabel();
        txtBuscarId = new javax.swing.JTextField();
        btnBuscarId = new javax.swing.JButton();
        lblNombreTraslado = new javax.swing.JLabel();
        lblIdTraslado = new javax.swing.JLabel();
        lblCodigoTraslado = new javax.swing.JLabel();
        lblEstadoTraslado = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        idTraslado = new javax.swing.JLabel();
        nomTraslado = new javax.swing.JLabel();
        estTraslado = new javax.swing.JLabel();
        codTraslado = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        btnCancelar1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Buscar Traslados");

        jTableTraslados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableTraslados);

        btnMostrarDatos.setText("Mostrar datos");
        btnMostrarDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarDatosActionPerformed(evt);
            }
        });

        jPanelBuscarTraslado.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Buscar Traslado"));

        btnBuscarNombre.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarNombre.setText("Buscar");
        btnBuscarNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarNombreActionPerformed(evt);
            }
        });

        lblBuscarNombre.setText("Buscar por nombre:");

        lblBuscarId.setText("Buscar por Id:");

        btnBuscarId.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarId.setText("Buscar");
        btnBuscarId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIdActionPerformed(evt);
            }
        });

        lblNombreTraslado.setText("Nombre");

        lblIdTraslado.setText("Id:");

        lblCodigoTraslado.setText("Código:");

        lblEstadoTraslado.setText("Estado:");

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        idTraslado.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        idTraslado.setForeground(new java.awt.Color(153, 0, 0));

        nomTraslado.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        nomTraslado.setForeground(new java.awt.Color(153, 0, 0));

        estTraslado.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        estTraslado.setForeground(new java.awt.Color(153, 0, 0));

        codTraslado.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        codTraslado.setForeground(new java.awt.Color(153, 0, 0));

        org.jdesktop.layout.GroupLayout jPanelBuscarTrasladoLayout = new org.jdesktop.layout.GroupLayout(jPanelBuscarTraslado);
        jPanelBuscarTraslado.setLayout(jPanelBuscarTrasladoLayout);
        jPanelBuscarTrasladoLayout.setHorizontalGroup(
            jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBuscarTrasladoLayout.createSequentialGroup()
                .add(26, 26, 26)
                .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarTrasladoLayout.createSequentialGroup()
                        .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jSeparator1)
                            .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(lblNombreTraslado)
                                .add(jPanelBuscarTrasladoLayout.createSequentialGroup()
                                    .add(8, 8, 8)
                                    .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                        .add(jPanelBuscarTrasladoLayout.createSequentialGroup()
                                            .add(lblIdTraslado)
                                            .add(58, 58, 58)
                                            .add(idTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(nomTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(55, 55, 55)
                                    .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(jPanelBuscarTrasladoLayout.createSequentialGroup()
                                            .add(lblEstadoTraslado)
                                            .add(18, 18, 18)
                                            .add(estTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(jPanelBuscarTrasladoLayout.createSequentialGroup()
                                            .add(lblCodigoTraslado)
                                            .add(18, 18, 18)
                                            .add(codTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))))
                        .add(38, 38, 38))
                    .add(jPanelBuscarTrasladoLayout.createSequentialGroup()
                        .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(lblBuscarId)
                            .add(lblBuscarNombre))
                        .add(35, 35, 35)
                        .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(txtBuscarNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(32, 32, 32)
                        .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarNombre)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarId))
                        .add(238, 238, 238))))
        );
        jPanelBuscarTrasladoLayout.setVerticalGroup(
            jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarTrasladoLayout.createSequentialGroup()
                .add(20, 20, 20)
                .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarNombre)
                    .add(lblBuscarNombre))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarId)
                    .add(lblBuscarId))
                .add(18, 18, 18)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblIdTraslado)
                    .add(lblCodigoTraslado)
                    .add(idTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(codTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelBuscarTrasladoLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblNombreTraslado)
                    .add(lblEstadoTraslado)
                    .add(nomTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(estTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        jLabel1.setText("Listado de Traslados");

        btnCancelar1.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar1.setText("Volver");
        btnCancelar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelar1ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(35, 35, 35)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanelBuscarTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(btnMostrarDatos)
                            .add(layout.createSequentialGroup()
                                .add(jLabel1)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 711, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(11, 11, 11))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(btnCancelar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 129, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 830, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(30, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(45, 45, 45)
                .add(jPanelBuscarTraslado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 70, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jLabel1)
                    .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btnMostrarDatos)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 202, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(31, 31, 31)
                .add(btnCancelar1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 45, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void btnMostrarDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarDatosActionPerformed
    String data[][] = {};
    String col[] = {"ID PAÍS", "CÓDIGO", "NOMBRE", "DETALLE", "FECHA PARTIDA", "FECHA LLEGADA", "ESTADO"};
    tablaTraslados = new DefaultTableModel(data, col);
    jTableTraslados.setModel(tablaTraslados);
    tablaTraslados = Traslado.listarTraslados(tablaTraslados);
}//GEN-LAST:event_btnMostrarDatosActionPerformed

private void btnBuscarNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarNombreActionPerformed

    txtBuscarNombre.setText("");

}//GEN-LAST:event_btnBuscarNombreActionPerformed

private void btnBuscarIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIdActionPerformed

    txtBuscarId.setText("");

}//GEN-LAST:event_btnBuscarIdActionPerformed

    private void btnCancelar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelar1ActionPerformed

        dispose();

    }//GEN-LAST:event_btnCancelar1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConsultarTraslado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConsultarTraslado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConsultarTraslado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConsultarTraslado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new ConsultarTraslado().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarId;
    private javax.swing.JButton btnBuscarNombre;
    private javax.swing.JButton btnCancelar1;
    private javax.swing.JButton btnMostrarDatos;
    private javax.swing.JLabel codTraslado;
    private javax.swing.JLabel estTraslado;
    private javax.swing.JLabel idTraslado;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelBuscarTraslado;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable jTableTraslados;
    private javax.swing.JLabel lblBuscarId;
    private javax.swing.JLabel lblBuscarNombre;
    private javax.swing.JLabel lblCodigoTraslado;
    private javax.swing.JLabel lblEstadoTraslado;
    private javax.swing.JLabel lblIdTraslado;
    private javax.swing.JLabel lblNombreTraslado;
    private javax.swing.JLabel nomTraslado;
    private javax.swing.JTextField txtBuscarId;
    private javax.swing.JTextField txtBuscarNombre;
    // End of variables declaration//GEN-END:variables

    

}
