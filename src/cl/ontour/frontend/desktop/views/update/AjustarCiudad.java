package cl.ontour.frontend.desktop.views.update;

import cl.ontour.backend.desktop.domains.Ciudad;
import cl.ontour.backend.desktop.domains.Pais;
import cl.ontour.frontend.desktop.Administrador;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Andrea
 */
public class AjustarCiudad extends javax.swing.JFrame {

    public AjustarCiudad() {
        initComponents();
        setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/cl/ontour/frontend/desktop/resources/images/logo_ontour_icon.png")));
        modificarOff();
    }
    
    private void limpiarCampos() {
        txtBuscarId.setText("");
        txtBuscarCodigo.setText("");
        idCiudad.setText("");
        nomCiudad.setText("");
        codCiudad.setText("");
        paisCiudad.setText("");
        estCiudad.setText("");
        idCiudadActual.setText("");
        txtCodigoCiudad.setText("");
        txtNombreCiudad.setText("");
        jCheckBoxActivo.setSelected(false);
        jComboBoxPais.removeAllItems();
        modificarOff();
    }
    
    private boolean validaCampos() {
        if (txtNombreCiudad.getText().isEmpty() || txtCodigoCiudad.getText().isEmpty()) {
            return false;
        }
        return true;
    }

    private void modificarOff() {
        for (int i = 0; i < jPanelCiudad.getComponents().length; i++) {
            jPanelCiudad.getComponent(i).setEnabled(false);
        }
    }

    private void modificarOn() {
        for (int i = 0; i < jPanelCiudad.getComponents().length; i++) {
            jPanelCiudad.getComponent(i).setEnabled(true);
        }
    }

    private void formatearEstado() {
        String estado = estCiudad.getText();
        if (estado.equals("Activo")) {
            jCheckBoxActivo.setSelected(true);
        }
        if (estado.equals("Inactivo")) {
            jCheckBoxActivo.setSelected(false);
        }
    }

    private boolean nuevoEstado() {
        if (jCheckBoxActivo.isSelected()) {
            return true;
        }
        return false;
    }
    
    private void cargarPaises() {
        jComboBoxPais.removeAllItems();
        List<Pais> paises = Pais.listadoPaises();
        for (int i = 0; i < paises.size(); i++) {
            jComboBoxPais.addItem(paises.get(i));
        }
    }

    private void datosAModificar() {
        idCiudadActual.setText(idCiudad.getText());
        txtCodigoCiudad.setText(codCiudad.getText());
        txtNombreCiudad.setText(nomCiudad.getText());
        for (int i = 0; i < jComboBoxPais.getItemCount(); i++) {
            if (jComboBoxPais.getItemAt(i).toString().equals(paisCiudad.getText())) {
                jComboBoxPais.setSelectedIndex(i);
                break;
            }
        }
        formatearEstado();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCiudad = new javax.swing.JPanel();
        txtCodigoCiudad = new javax.swing.JTextField();
        lblCodigoCiudad = new javax.swing.JLabel();
        lblNombreCiudad = new javax.swing.JLabel();
        txtNombreCiudad = new javax.swing.JTextField();
        jComboBoxPais = new javax.swing.JComboBox();
        lblPais = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        idCiudadActual = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jCheckBoxActivo = new javax.swing.JCheckBox();
        BtnGuardar = new javax.swing.JButton();
        BtnVolver = new javax.swing.JButton();
        jPanelBuscarCiudad = new javax.swing.JPanel();
        txtBuscarId = new javax.swing.JTextField();
        btnBuscarId = new javax.swing.JButton();
        lblBuscarId = new javax.swing.JLabel();
        lblBuscarCodigo = new javax.swing.JLabel();
        txtBuscarCodigo = new javax.swing.JTextField();
        btnBuscarCodigo = new javax.swing.JButton();
        lblNombreCiudad1 = new javax.swing.JLabel();
        lblIdCiudad = new javax.swing.JLabel();
        lblCodigoCiudad1 = new javax.swing.JLabel();
        lblEstadoCiudad = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        idCiudad = new javax.swing.JLabel();
        nomCiudad = new javax.swing.JLabel();
        estCiudad = new javax.swing.JLabel();
        codCiudad = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        paisCiudad = new javax.swing.JLabel();
        btnEliminar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Registr de Ciudades");

        jPanelCiudad.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ingrese nuevos datos"));
        jPanelCiudad.setPreferredSize(new java.awt.Dimension(1200, 275));

        lblCodigoCiudad.setText("Código:");

        lblNombreCiudad.setText("Nombre:");

        lblPais.setText("País:");

        jLabel1.setText("ID:");

        jLabel3.setText("Estado:");

        jCheckBoxActivo.setText("Activo");

        BtnGuardar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnGuardar.setText("Guardar");
        BtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnGuardarActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanelCiudadLayout = new org.jdesktop.layout.GroupLayout(jPanelCiudad);
        jPanelCiudad.setLayout(jPanelCiudadLayout);
        jPanelCiudadLayout.setHorizontalGroup(
            jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelCiudadLayout.createSequentialGroup()
                .add(52, 52, 52)
                .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(lblNombreCiudad)
                    .add(lblPais)
                    .add(jLabel1)
                    .add(jLabel3))
                .add(18, 18, 18)
                .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelCiudadLayout.createSequentialGroup()
                        .add(idCiudadActual, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                        .add(18, 18, 18)
                        .add(lblCodigoCiudad)
                        .add(18, 18, 18)
                        .add(txtCodigoCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanelCiudadLayout.createSequentialGroup()
                        .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(txtNombreCiudad)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jCheckBoxActivo)
                            .add(jComboBoxPais, 0, 314, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(BtnGuardar)))
                .add(16, 16, 16))
        );
        jPanelCiudadLayout.setVerticalGroup(
            jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelCiudadLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(lblCodigoCiudad)
                        .add(txtCodigoCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jLabel1))
                    .add(idCiudadActual, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 24, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jComboBoxPais, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblPais))
                .add(18, 18, 18)
                .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblNombreCiudad)
                    .add(txtNombreCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel3)
                    .add(jCheckBoxActivo)
                    .add(BtnGuardar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(37, 37, 37))
        );

        BtnVolver.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnVolver.setText("Volver");
        BtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVolverActionPerformed(evt);
            }
        });

        jPanelBuscarCiudad.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Buscar ciudad"));

        btnBuscarId.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarId.setText("Buscar");
        btnBuscarId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIdActionPerformed(evt);
            }
        });

        lblBuscarId.setText("Buscar por ID:");

        lblBuscarCodigo.setText("Buscar por código:");

        btnBuscarCodigo.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarCodigo.setText("Buscar");
        btnBuscarCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarCodigoActionPerformed(evt);
            }
        });

        lblNombreCiudad1.setText("Nombre:");

        lblIdCiudad.setText("ID:");

        lblCodigoCiudad1.setText("Código:");

        lblEstadoCiudad.setText("Estado:");

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        idCiudad.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        idCiudad.setForeground(new java.awt.Color(153, 0, 0));
        idCiudad.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                idCiudadPropertyChange(evt);
            }
        });

        nomCiudad.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        nomCiudad.setForeground(new java.awt.Color(153, 0, 0));

        estCiudad.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        estCiudad.setForeground(new java.awt.Color(153, 0, 0));
        estCiudad.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                estCiudadPropertyChange(evt);
            }
        });

        codCiudad.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        codCiudad.setForeground(new java.awt.Color(153, 0, 0));

        jLabel2.setText("País:");

        paisCiudad.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        paisCiudad.setForeground(new java.awt.Color(153, 0, 0));

        btnEliminar.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnModificar.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanelBuscarCiudadLayout = new org.jdesktop.layout.GroupLayout(jPanelBuscarCiudad);
        jPanelBuscarCiudad.setLayout(jPanelBuscarCiudadLayout);
        jPanelBuscarCiudadLayout.setHorizontalGroup(
            jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                .add(26, 26, 26)
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel2)
                    .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                            .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                .add(org.jdesktop.layout.GroupLayout.LEADING, jSeparator1)
                                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(lblNombreCiudad1)
                                    .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                                        .add(8, 8, 8)
                                        .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                            .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                                                .add(lblIdCiudad)
                                                .add(58, 58, 58)
                                                .add(idCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                            .add(nomCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                            .add(paisCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .add(55, 55, 55)
                                        .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                            .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                                                .add(lblEstadoCiudad)
                                                .add(18, 18, 18)
                                                .add(estCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                            .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                                                .add(lblCodigoCiudad1)
                                                .add(18, 18, 18)
                                                .add(codCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 279, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))))
                            .add(38, 38, 38))
                        .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                            .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(lblBuscarCodigo)
                                .add(lblBuscarId))
                            .add(35, 35, 35)
                            .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(txtBuscarCodigo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(32, 32, 32)
                            .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarId)
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarCodigo))
                            .add(238, 238, 238)))))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarCiudadLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(btnModificar)
                .add(18, 18, 18)
                .add(btnEliminar)
                .add(132, 132, 132))
        );
        jPanelBuscarCiudadLayout.setVerticalGroup(
            jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarCiudadLayout.createSequentialGroup()
                .add(20, 20, 20)
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarId)
                    .add(lblBuscarId))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarCodigo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarCodigo)
                    .add(lblBuscarCodigo))
                .add(18, 18, 18)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblIdCiudad)
                    .add(lblCodigoCiudad1)
                    .add(idCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(codCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblNombreCiudad1)
                    .add(lblEstadoCiudad)
                    .add(nomCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(estCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                        .add(18, 18, 18)
                        .add(jLabel2))
                    .add(jPanelBuscarCiudadLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(paisCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelBuscarCiudadLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnModificar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnEliminar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jPanelBuscarCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 723, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .add(63, 63, 63)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(BtnVolver)
                            .add(jPanelCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 723, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(110, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(jPanelBuscarCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(32, 32, 32)
                .add(jPanelCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 211, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(BtnVolver, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );

        jPanelCiudad.getAccessibleContext().setAccessibleName("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void BtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGuardarActionPerformed
    if (!validaCampos()) {
        JOptionPane.showMessageDialog(null, "Ingrese datos faltantes.");
    } else if (!codCiudad.getText().equals(txtCodigoCiudad.getText()) && !Ciudad.verificaCodigo(txtCodigoCiudad.getText())) { // Si código se modificó y no existe
        JOptionPane.showMessageDialog(null, Administrador.error);
        Administrador.error = "";
    } else if (Ciudad.modificarCiudad(idCiudadActual.getText(), txtCodigoCiudad.getText(), txtNombreCiudad.getText(), (Pais) jComboBoxPais.getSelectedItem(), String.valueOf(nuevoEstado()))) {
        JOptionPane.showMessageDialog(null, "Actualización exitosa.");
        limpiarCampos();
    } else if (!Administrador.error.isEmpty()) {
        JOptionPane.showMessageDialog(null, Administrador.error);
        Administrador.error = "";
    } else {
        JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
    }
}//GEN-LAST:event_BtnGuardarActionPerformed

    private void BtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVolverActionPerformed
        dispose();
    }//GEN-LAST:event_BtnVolverActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        if (Ciudad.eliminarCiudad(idCiudad.getText())) {
            JOptionPane.showMessageDialog(null, "Eliminación exitosa.");
            limpiarCampos();
        } else if (!Administrador.error.isEmpty()) {
            JOptionPane.showMessageDialog(null, Administrador.error);
            Administrador.error = "";
        } else {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        modificarOn();        
        cargarPaises();
        datosAModificar();
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnBuscarIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIdActionPerformed
        if (!txtBuscarId.getText().trim().isEmpty()) {
            String c_id = txtBuscarId.getText().trim();
            Ciudad ciudad = Ciudad.buscarPorId(c_id);
            if (ciudad != null) {
                limpiarCampos();
                idCiudad.setText(String.valueOf(ciudad.getId_ciudad()));
                nomCiudad.setText(ciudad.getNombre_ciudad());
                codCiudad.setText(ciudad.getCodigo_ciudad());
                paisCiudad.setText(ciudad.getPais_ciudad().toString());
                estCiudad.setText(ciudad.isEstado_ciudad() ? "Activo" : "Inactivo");
            } else {
                JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
            }
        }
    }//GEN-LAST:event_btnBuscarIdActionPerformed

    private void btnBuscarCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarCodigoActionPerformed
        if (!txtBuscarCodigo.getText().trim().isEmpty()) {
            String c_codigo = txtBuscarCodigo.getText().trim();
            Ciudad ciudad = Ciudad.buscarPorCodigo(c_codigo);
            if (ciudad != null) {
                limpiarCampos();
                idCiudad.setText(String.valueOf(ciudad.getId_ciudad()));
                nomCiudad.setText(ciudad.getNombre_ciudad());
                codCiudad.setText(ciudad.getCodigo_ciudad());
                paisCiudad.setText(ciudad.getPais_ciudad().toString());
                estCiudad.setText(ciudad.isEstado_ciudad() ? "Activo" : "Inactivo");
            } else {
                JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
            }
        }
    }//GEN-LAST:event_btnBuscarCodigoActionPerformed

    private void idCiudadPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_idCiudadPropertyChange
        if (idCiudad.getText().isEmpty()) {
            btnModificar.setEnabled(false);
            btnEliminar.setEnabled(false);
        } else {
            btnModificar.setEnabled(true);
            btnEliminar.setEnabled(true);
        }
    }//GEN-LAST:event_idCiudadPropertyChange

    private void estCiudadPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_estCiudadPropertyChange
        if (estCiudad.getText().equals("Activo")) {
            btnEliminar.setEnabled(true);
        } else {
            btnEliminar.setEnabled(false);
        }
    }//GEN-LAST:event_estCiudadPropertyChange

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AjustarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AjustarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AjustarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AjustarCiudad.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new AjustarCiudad().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnGuardar;
    private javax.swing.JButton BtnVolver;
    private javax.swing.JButton btnBuscarCodigo;
    private javax.swing.JButton btnBuscarId;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JLabel codCiudad;
    private javax.swing.JLabel estCiudad;
    private javax.swing.JLabel idCiudad;
    private javax.swing.JLabel idCiudadActual;
    private javax.swing.JCheckBox jCheckBoxActivo;
    public static javax.swing.JComboBox jComboBoxPais;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanelBuscarCiudad;
    private javax.swing.JPanel jPanelCiudad;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblBuscarCodigo;
    private javax.swing.JLabel lblBuscarId;
    private javax.swing.JLabel lblCodigoCiudad;
    private javax.swing.JLabel lblCodigoCiudad1;
    private javax.swing.JLabel lblEstadoCiudad;
    private javax.swing.JLabel lblIdCiudad;
    private javax.swing.JLabel lblNombreCiudad;
    private javax.swing.JLabel lblNombreCiudad1;
    private javax.swing.JLabel lblPais;
    private javax.swing.JLabel nomCiudad;
    private javax.swing.JLabel paisCiudad;
    private javax.swing.JTextField txtBuscarCodigo;
    private javax.swing.JTextField txtBuscarId;
    private javax.swing.JTextField txtCodigoCiudad;
    private javax.swing.JTextField txtNombreCiudad;
    // End of variables declaration//GEN-END:variables

}
