package cl.ontour.frontend.desktop.views.update;

import cl.ontour.backend.desktop.domains.Ciudad;
import cl.ontour.backend.desktop.domains.Colegio;
import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Andrea
 */
public class AjustarColegio extends javax.swing.JFrame {

    public AjustarColegio() {
        initComponents();
        setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/cl/ontour/frontend/desktop/resources/images/logo_ontour_icon.png")));
        Funciones.activarComponentes(jPanelColegio, false);
    }
    
    private void limpiarCampos() {
        txtBuscarId.setText("");
        txtBuscarCodigo.setText("");
        lblId.setText("");
        lblCodigo.setText("");
        lblNombre.setText("");
        lblDireccion.setText("");
        lblTelefono.setText("");
        lblCorreo.setText("");
        lblCiudad.setText("");
        lblEstado.setText("");
        txtCodigoColegio.setText("");
        txtNombreColegio.setText("");
        txtDireccionColegio.setText("");
        txtTelefonoColegio.setText("");
        txtEmailColegio.setText("");
        jCheckBoxActivo.setSelected(false);
        jComboBoxCiudad.removeAllItems();
        Funciones.activarComponentes(jPanelColegio, false);
    }
    
    private boolean validaCampos() {
        if (txtCodigoColegio.getText().isEmpty() || txtNombreColegio.getText().isEmpty() || txtDireccionColegio.getText().isEmpty()) {
            return false;
        }
        return true;
    }

    private void formatearEstado() {
        String estado = lblEstado.getText();
        if (estado.equals("Activo")) {
            jCheckBoxActivo.setSelected(true);
        }
        if (estado.equals("Inactivo")) {
            jCheckBoxActivo.setSelected(false);
        }
    }

    private boolean nuevoEstado() {
        if (jCheckBoxActivo.isSelected()) {
            return true;
        }
        return false;
    }
    
    private void cargarCiudades() {
        List<Ciudad> ciudades = Ciudad.listadoCiudades();
        for (int i = 0; i < ciudades.size(); i++) {
            jComboBoxCiudad.addItem(ciudades.get(i));            
        }  
    }

    private void datosAModificar() {
        txtCodigoColegio.setText(lblCodigo.getText());
        txtNombreColegio.setText(lblNombre.getText());
        txtDireccionColegio.setText(lblDireccion.getText());
        txtTelefonoColegio.setText(lblTelefono.getText());
        txtEmailColegio.setText(lblCorreo.getText());      
        for (int i = 0; i < jComboBoxCiudad.getItemCount(); i++) {
            if (jComboBoxCiudad.getItemAt(i).toString().equals(lblCiudad.getText())) {
                jComboBoxCiudad.setSelectedIndex(i);
                break;
            }
        }
        formatearEstado();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelColegio = new javax.swing.JPanel();
        lblCodigoColegio = new javax.swing.JLabel();
        txtCodigoColegio = new javax.swing.JTextField();
        lblNombreColegio = new javax.swing.JLabel();
        txtNombreColegio = new javax.swing.JTextField();
        txtDireccionColegio = new javax.swing.JTextField();
        lblDireccionColegio = new javax.swing.JLabel();
        txtTelefonoColegio = new javax.swing.JTextField();
        lblTelefonoColegio = new javax.swing.JLabel();
        txtEmailColegio = new javax.swing.JTextField();
        lblEmailColegio = new javax.swing.JLabel();
        jComboBoxCiudad = new javax.swing.JComboBox();
        lblCiudadColegio = new javax.swing.JLabel();
        BtnGuardar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jCheckBoxActivo = new javax.swing.JCheckBox();
        jPanelBuscarColegio = new javax.swing.JPanel();
        txtBuscarId = new javax.swing.JTextField();
        btnBuscarId = new javax.swing.JButton();
        lblBuscarId = new javax.swing.JLabel();
        lblBuscarCodigo = new javax.swing.JLabel();
        txtBuscarCodigo = new javax.swing.JTextField();
        btnBuscarCodigo = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblId = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblTelefono = new javax.swing.JLabel();
        lblCiudad = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblCodigo = new javax.swing.JLabel();
        lblDireccion = new javax.swing.JLabel();
        lblCorreo = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblEstado = new javax.swing.JLabel();
        btnEliminar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        BtnVolver = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ajustar colegios");

        jPanelColegio.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ingrese nuevos datos"));

        lblCodigoColegio.setText("Código:");

        lblNombreColegio.setText("Nombre:");

        lblDireccionColegio.setText("Dirección:");

        lblTelefonoColegio.setText("Teléfono:");

        lblEmailColegio.setText("Correo electrónico:");

        lblCiudadColegio.setText("Ciudad:");

        BtnGuardar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnGuardar.setText("Guardar");
        BtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnGuardarActionPerformed(evt);
            }
        });

        btnLimpiar.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        jLabel5.setText("Estado:");

        jCheckBoxActivo.setText("Activo");

        org.jdesktop.layout.GroupLayout jPanelColegioLayout = new org.jdesktop.layout.GroupLayout(jPanelColegio);
        jPanelColegio.setLayout(jPanelColegioLayout);
        jPanelColegioLayout.setHorizontalGroup(
            jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelColegioLayout.createSequentialGroup()
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelColegioLayout.createSequentialGroup()
                        .add(92, 92, 92)
                        .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(lblDireccionColegio)
                            .add(lblTelefonoColegio))
                        .add(18, 18, 18)
                        .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(txtTelefonoColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 540, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(txtDireccionColegio)))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelColegioLayout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelColegioLayout.createSequentialGroup()
                                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(lblCiudadColegio)
                                    .add(lblEmailColegio)
                                    .add(jLabel5))
                                .add(18, 18, 18)
                                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(jComboBoxCiudad, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelColegioLayout.createSequentialGroup()
                                        .add(jCheckBoxActivo)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 296, Short.MAX_VALUE)
                                        .add(btnLimpiar)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(BtnGuardar))
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, txtEmailColegio)))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelColegioLayout.createSequentialGroup()
                                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, lblCodigoColegio)
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, lblNombreColegio))
                                .add(18, 18, 18)
                                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(txtNombreColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 540, Short.MAX_VALUE)
                                    .add(txtCodigoColegio))))))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanelColegioLayout.setVerticalGroup(
            jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelColegioLayout.createSequentialGroup()
                .add(44, 44, 44)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblCodigoColegio)
                    .add(txtCodigoColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtNombreColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblNombreColegio))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtDireccionColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblDireccionColegio))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtTelefonoColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblTelefonoColegio))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtEmailColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblEmailColegio))
                .add(18, 18, 18)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboBoxCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblCiudadColegio))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(BtnGuardar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(btnLimpiar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanelColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel5)
                        .add(jCheckBoxActivo)))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanelBuscarColegio.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Buscar colegio"));

        btnBuscarId.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarId.setText("Buscar");
        btnBuscarId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIdActionPerformed(evt);
            }
        });

        lblBuscarId.setText("Buscar por ID:");

        lblBuscarCodigo.setText("Buscar por código:");

        btnBuscarCodigo.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarCodigo.setText("Buscar");
        btnBuscarCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarCodigoActionPerformed(evt);
            }
        });

        jLabel1.setText("ID:");

        jLabel2.setText("Nombre:");

        jLabel3.setText("Teléfono:");

        jLabel4.setText("Ciudad:");

        lblId.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                lblIdPropertyChange(evt);
            }
        });

        jLabel6.setText("Código:");

        jLabel7.setText("Dirección:");

        jLabel8.setText("Correo electrónico:");

        jLabel9.setText("Estado:");

        lblEstado.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                lblEstadoPropertyChange(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanelBuscarColegioLayout = new org.jdesktop.layout.GroupLayout(jPanelBuscarColegio);
        jPanelBuscarColegio.setLayout(jPanelBuscarColegioLayout);
        jPanelBuscarColegioLayout.setHorizontalGroup(
            jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBuscarColegioLayout.createSequentialGroup()
                .add(26, 26, 26)
                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(jPanelBuscarColegioLayout.createSequentialGroup()
                            .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(lblBuscarCodigo)
                                .add(lblBuscarId))
                            .add(35, 35, 35)
                            .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(txtBuscarCodigo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(32, 32, 32)
                            .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarId)
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarCodigo)))
                        .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 536, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanelBuscarColegioLayout.createSequentialGroup()
                        .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jLabel4)
                            .add(jLabel3)
                            .add(jLabel2)
                            .add(jLabel1))
                        .add(18, 18, 18)
                        .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(lblId, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(lblNombre, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(lblTelefono, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                            .add(lblCiudad, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .add(27, 27, 27)
                        .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jLabel6)
                            .add(jLabel7)
                            .add(jLabel8)
                            .add(jLabel9))
                        .add(18, 18, 18)
                        .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(lblCodigo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                            .add(lblDireccion, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(lblCorreo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(lblEstado, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelBuscarColegioLayout.setVerticalGroup(
            jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarColegioLayout.createSequentialGroup()
                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jPanelBuscarColegioLayout.createSequentialGroup()
                            .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jPanelBuscarColegioLayout.createSequentialGroup()
                                    .add(20, 20, 20)
                                    .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                        .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(btnBuscarId)
                                        .add(lblBuscarId))
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                    .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                        .add(txtBuscarCodigo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(btnBuscarCodigo)
                                        .add(lblBuscarCodigo))
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(18, 18, 18)
                                    .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                        .add(jLabel1)
                                        .add(lblId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarColegioLayout.createSequentialGroup()
                                    .addContainerGap()
                                    .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                        .add(jLabel6)
                                        .add(lblCodigo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                            .add(18, 18, 18)
                            .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                    .add(jLabel2)
                                    .add(lblNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                    .add(jLabel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(lblDireccion, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                            .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(jLabel3)
                                .add(lblTelefono, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel8))
                    .add(jPanelBuscarColegioLayout.createSequentialGroup()
                        .add(198, 198, 198)
                        .add(lblCorreo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel4)
                        .add(lblCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanelBuscarColegioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel9)
                        .add(lblEstado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        btnEliminar.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnModificar.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        BtnVolver.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnVolver.setText("Volver");
        BtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVolverActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(layout.createSequentialGroup()
                        .add(btnModificar)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(btnEliminar))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, layout.createSequentialGroup()
                        .add(BtnVolver)
                        .add(135, 135, 135))
                    .add(jPanelColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanelBuscarColegio, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanelBuscarColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnEliminar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnModificar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelColegio, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(BtnVolver, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(10, 10, 10))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIdActionPerformed
        if (!txtBuscarId.getText().trim().isEmpty()) {
            String c_id = txtBuscarId.getText().trim();
            Colegio colegio = Colegio.buscarPorId(c_id);
            if (colegio != null) {
                limpiarCampos();
                lblId.setText(String.valueOf(colegio.getId()));
                lblNombre.setText(colegio.getNombre());
                lblCodigo.setText(colegio.getCodigo());
                lblDireccion.setText(colegio.getDireccion());
                if (colegio.getTelefono().isEmpty() || colegio.getTelefono().equals("null")) {
                    lblTelefono.setText("No especificado");
                } else {
                    lblTelefono.setText(colegio.getTelefono());
                }
                if (colegio.getCorreo_electronico().isEmpty() || colegio.getCorreo_electronico().equals("null")) {
                    lblCorreo.setText("No especificado");
                } else {
                    lblCorreo.setText(colegio.getCorreo_electronico());
                }
                lblCiudad.setText(colegio.getCiudad().toString());
                lblEstado.setText(colegio.isEstado() ? "Activo" : "Inactivo");
            } else {
                JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
            }
        }
    }//GEN-LAST:event_btnBuscarIdActionPerformed

    private void btnBuscarCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarCodigoActionPerformed
        if (!txtBuscarCodigo.getText().trim().isEmpty()) {
            String c_codigo = txtBuscarCodigo.getText().trim();
            Colegio colegio = Colegio.buscarPorCodigo(c_codigo);
            if (colegio != null) {
                limpiarCampos();
                lblId.setText(String.valueOf(colegio.getId()));
                lblNombre.setText(colegio.getNombre());
                lblCodigo.setText(colegio.getCodigo());
                lblDireccion.setText(colegio.getDireccion());
                if (colegio.getTelefono().isEmpty() || colegio.getTelefono().equals("null")) {
                    lblTelefono.setText("No especificado");
                } else {
                    lblTelefono.setText(colegio.getTelefono());
                }
                if (colegio.getCorreo_electronico().isEmpty() || colegio.getCorreo_electronico().equals("null")) {
                    lblCorreo.setText("No especificado");
                } else {
                    lblCorreo.setText(colegio.getCorreo_electronico());
                }
                lblCiudad.setText(colegio.getCiudad().toString());
                lblEstado.setText(colegio.isEstado() ? "Activo" : "Inactivo");
            } else {
                JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
            }
        }
    }//GEN-LAST:event_btnBuscarCodigoActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        if (Colegio.eliminarColegio(lblId.getText())) {
            JOptionPane.showMessageDialog(null, "Eliminación exitosa.");
            limpiarCampos();
        } else if (!Administrador.error.isEmpty()) {
            JOptionPane.showMessageDialog(null, Administrador.error);
            Administrador.error = "";
        } else {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        Funciones.activarComponentes(jPanelColegio, true);
        cargarCiudades();
        datosAModificar();
    }//GEN-LAST:event_btnModificarActionPerformed

    private void BtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGuardarActionPerformed
        if (!validaCampos()) {
            JOptionPane.showMessageDialog(null, "Ingrese datos faltantes.");
        } else if (!lblCodigo.getText().equals(txtCodigoColegio.getText()) && !Ciudad.verificaCodigo(txtCodigoColegio.getText())) { // Si código se modificó y no existe
            JOptionPane.showMessageDialog(null, Administrador.error);
            Administrador.error = "";
        } else if (Colegio.modificarColegio(lblId.getText(), txtCodigoColegio.getText(), txtNombreColegio.getText(), txtDireccionColegio.getText(), txtTelefonoColegio.getText(), txtEmailColegio.getText(), (Ciudad) jComboBoxCiudad.getSelectedItem(), String.valueOf(nuevoEstado()))) {
            JOptionPane.showMessageDialog(null, "Actualización exitosa.");
            limpiarCampos();
        } else if (!Administrador.error.isEmpty()) {
            JOptionPane.showMessageDialog(null, Administrador.error);
            Administrador.error = "";
        } else {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
        }
    }//GEN-LAST:event_BtnGuardarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        limpiarCampos();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void BtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVolverActionPerformed
        dispose();
    }//GEN-LAST:event_BtnVolverActionPerformed

    private void lblIdPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_lblIdPropertyChange
        if (lblId.getText().isEmpty()) {
            btnModificar.setEnabled(false);
            btnEliminar.setEnabled(false);
        } else {
            btnModificar.setEnabled(true);
            btnEliminar.setEnabled(true);
        }
    }//GEN-LAST:event_lblIdPropertyChange

    private void lblEstadoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_lblEstadoPropertyChange
        if (lblEstado.getText().equals("Activo")) {
            btnEliminar.setEnabled(true);
        } else {
            btnEliminar.setEnabled(false);
        }
    }//GEN-LAST:event_lblEstadoPropertyChange

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AjustarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AjustarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AjustarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AjustarColegio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new AjustarColegio().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnGuardar;
    private javax.swing.JButton BtnVolver;
    private javax.swing.JButton btnBuscarCodigo;
    private javax.swing.JButton btnBuscarId;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JCheckBox jCheckBoxActivo;
    public static javax.swing.JComboBox jComboBoxCiudad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanelBuscarColegio;
    private javax.swing.JPanel jPanelColegio;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblBuscarCodigo;
    private javax.swing.JLabel lblBuscarId;
    private javax.swing.JLabel lblCiudad;
    private javax.swing.JLabel lblCiudadColegio;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JLabel lblCodigoColegio;
    private javax.swing.JLabel lblCorreo;
    private javax.swing.JLabel lblDireccion;
    private javax.swing.JLabel lblDireccionColegio;
    private javax.swing.JLabel lblEmailColegio;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblId;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblNombreColegio;
    private javax.swing.JLabel lblTelefono;
    private javax.swing.JLabel lblTelefonoColegio;
    private javax.swing.JTextField txtBuscarCodigo;
    private javax.swing.JTextField txtBuscarId;
    private javax.swing.JTextField txtCodigoColegio;
    private javax.swing.JTextField txtDireccionColegio;
    private javax.swing.JTextField txtEmailColegio;
    private javax.swing.JTextField txtNombreColegio;
    private javax.swing.JTextField txtTelefonoColegio;
    // End of variables declaration//GEN-END:variables
}
