package cl.ontour.frontend.desktop.views.update;

import cl.ontour.backend.desktop.domains.Pais;
import cl.ontour.frontend.desktop.Administrador;
import javax.swing.JOptionPane;

/**
 *
 * @author asaravia
 */
public class AjustarPais extends javax.swing.JFrame {

    public AjustarPais() {
        initComponents();
        setLocationRelativeTo(null);
        modificarOff();
    }
    
    private void limpiarCampos() {
        txtBuscarId.setText("");
        txtBuscarCodigo.setText("");
        idPais.setText("");
        nomPais.setText("");
        codPais.setText("");
        estPais.setText("");
        idPaisActual.setText("");
        txtNuevoNombrePais.setText("");
        txtNuevoCodigoPais.setText("");
        jCheckBoxActivo.setSelected(false);
        modificarOff();
    }
    
    private boolean validaCampos() {
        if (txtNuevoNombrePais.getText().isEmpty() || txtNuevoCodigoPais.getText().isEmpty()) {
            return false;
        }
        return true;
    }

    private void modificarOff() {
        for (int i = 0; i < jPanelNuevoPais.getComponents().length; i++) {
            jPanelNuevoPais.getComponent(i).setEnabled(false);
        }
    }

    private void modificarOn() {
        for (int i = 0; i < jPanelNuevoPais.getComponents().length; i++) {
            jPanelNuevoPais.getComponent(i).setEnabled(true);
        }
    }

    private void formatearEstado() {
        String estado = estPais.getText();
        if (estado.equals("Activo")) {
            jCheckBoxActivo.setSelected(true);
        }
        if (estado.equals("Inactivo")) {
            jCheckBoxActivo.setSelected(false);
        }
    }

    private boolean nuevoEstado() {
        if (jCheckBoxActivo.isSelected()) {
            return true;
        }
        return false;
    }

    private void datosAModificar() {
        idPaisActual.setText(idPais.getText());
        txtNuevoCodigoPais.setText(codPais.getText());
        txtNuevoNombrePais.setText(nomPais.getText());
        formatearEstado();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelBuscarPais = new javax.swing.JPanel();
        txtBuscarId = new javax.swing.JTextField();
        btnBuscarId = new javax.swing.JButton();
        lblBuscarNombre = new javax.swing.JLabel();
        lblBuscarId = new javax.swing.JLabel();
        txtBuscarCodigo = new javax.swing.JTextField();
        btnBuscarCodigo = new javax.swing.JButton();
        lblNombrePais = new javax.swing.JLabel();
        lblIdPais = new javax.swing.JLabel();
        lblCodigoPais = new javax.swing.JLabel();
        lblEstadoPais = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        idPais = new javax.swing.JLabel();
        nomPais = new javax.swing.JLabel();
        estPais = new javax.swing.JLabel();
        codPais = new javax.swing.JLabel();
        btnModificarP = new javax.swing.JButton();
        btnEliminarP = new javax.swing.JButton();
        jPanelNuevoPais = new javax.swing.JPanel();
        txtNuevoCodigoPais = new javax.swing.JTextField();
        lblNuevoIdPais = new javax.swing.JLabel();
        lblNuevoCodigoPais = new javax.swing.JLabel();
        lblNuevoNombrePais = new javax.swing.JLabel();
        txtNuevoNombrePais = new javax.swing.JTextField();
        BtnGuardar = new javax.swing.JButton();
        idPaisActual = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jCheckBoxActivo = new javax.swing.JCheckBox();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelBuscarPais.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Buscar país"));

        btnBuscarId.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarId.setText("Buscar");
        btnBuscarId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIdActionPerformed(evt);
            }
        });

        lblBuscarNombre.setText("Buscar por ID:");

        lblBuscarId.setText("Buscar por código:");

        btnBuscarCodigo.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarCodigo.setText("Buscar");
        btnBuscarCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarCodigoActionPerformed(evt);
            }
        });

        lblNombrePais.setText("Nombre:");

        lblIdPais.setText("ID:");

        lblCodigoPais.setText("Código:");

        lblEstadoPais.setText("Estado:");

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        idPais.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        idPais.setForeground(new java.awt.Color(153, 0, 0));
        idPais.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                idPaisPropertyChange(evt);
            }
        });

        nomPais.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        nomPais.setForeground(new java.awt.Color(153, 0, 0));

        estPais.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        estPais.setForeground(new java.awt.Color(153, 0, 0));
        estPais.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                estPaisPropertyChange(evt);
            }
        });

        codPais.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        codPais.setForeground(new java.awt.Color(153, 0, 0));

        btnModificarP.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnModificarP.setText("Modificar");
        btnModificarP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarPActionPerformed(evt);
            }
        });

        btnEliminarP.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnEliminarP.setText("Eliminar");
        btnEliminarP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarPActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelBuscarPaisLayout = new javax.swing.GroupLayout(jPanelBuscarPais);
        jPanelBuscarPais.setLayout(jPanelBuscarPaisLayout);
        jPanelBuscarPaisLayout.setHorizontalGroup(
            jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBuscarPaisLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelBuscarPaisLayout.createSequentialGroup()
                        .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelBuscarPaisLayout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(jPanelBuscarPaisLayout.createSequentialGroup()
                                        .addComponent(lblIdPais)
                                        .addGap(58, 58, 58)
                                        .addComponent(idPais, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanelBuscarPaisLayout.createSequentialGroup()
                                        .addComponent(lblNombrePais)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(nomPais, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanelBuscarPaisLayout.createSequentialGroup()
                                        .addGap(55, 55, 55)
                                        .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanelBuscarPaisLayout.createSequentialGroup()
                                                .addComponent(lblCodigoPais)
                                                .addGap(18, 18, 18)
                                                .addComponent(codPais, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(jPanelBuscarPaisLayout.createSequentialGroup()
                                                .addComponent(lblEstadoPais)
                                                .addGap(18, 18, 18)
                                                .addComponent(estPais, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGroup(jPanelBuscarPaisLayout.createSequentialGroup()
                                        .addGap(177, 177, 177)
                                        .addComponent(btnModificarP)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnEliminarP)))))
                        .addGap(38, 38, 38))
                    .addGroup(jPanelBuscarPaisLayout.createSequentialGroup()
                        .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblBuscarId)
                            .addComponent(lblBuscarNombre))
                        .addGap(35, 35, 35)
                        .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtBuscarId, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBuscarCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(32, 32, 32)
                        .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnBuscarId, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnBuscarCodigo, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(238, 238, 238))))
        );
        jPanelBuscarPaisLayout.setVerticalGroup(
            jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelBuscarPaisLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscarId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarId)
                    .addComponent(lblBuscarNombre))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscarCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBuscarCodigo)
                    .addComponent(lblBuscarId))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblIdPais)
                    .addComponent(lblCodigoPais)
                    .addComponent(idPais, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(codPais, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lblNombrePais)
                    .addComponent(lblEstadoPais)
                    .addComponent(nomPais, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(estPais, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17)
                .addGroup(jPanelBuscarPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnModificarP, javax.swing.GroupLayout.DEFAULT_SIZE, 49, Short.MAX_VALUE)
                    .addComponent(btnEliminarP, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanelNuevoPais.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ingrese nuevos datos"));

        lblNuevoIdPais.setText("ID:");

        lblNuevoCodigoPais.setText("Código:");

        lblNuevoNombrePais.setText("Nombre:");

        BtnGuardar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnGuardar.setText("Guardar");
        BtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnGuardarActionPerformed(evt);
            }
        });

        idPaisActual.setFont(new java.awt.Font("Lucida Grande", 1, 16)); // NOI18N

        jLabel1.setText("Estado:");

        jCheckBoxActivo.setText("Activo");

        javax.swing.GroupLayout jPanelNuevoPaisLayout = new javax.swing.GroupLayout(jPanelNuevoPais);
        jPanelNuevoPais.setLayout(jPanelNuevoPaisLayout);
        jPanelNuevoPaisLayout.setHorizontalGroup(
            jPanelNuevoPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelNuevoPaisLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(jPanelNuevoPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelNuevoPaisLayout.createSequentialGroup()
                        .addComponent(BtnGuardar)
                        .addGap(41, 41, 41))
                    .addGroup(jPanelNuevoPaisLayout.createSequentialGroup()
                        .addGroup(jPanelNuevoPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addGroup(jPanelNuevoPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblNuevoIdPais)
                                .addComponent(lblNuevoNombrePais)))
                        .addGap(41, 41, 41)
                        .addGroup(jPanelNuevoPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelNuevoPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtNuevoNombrePais, javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanelNuevoPaisLayout.createSequentialGroup()
                                    .addComponent(idPaisActual, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(51, 51, 51)
                                    .addComponent(lblNuevoCodigoPais)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtNuevoCodigoPais)))
                            .addComponent(jCheckBoxActivo))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanelNuevoPaisLayout.setVerticalGroup(
            jPanelNuevoPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelNuevoPaisLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanelNuevoPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(txtNuevoCodigoPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNuevoIdPais)
                    .addComponent(lblNuevoCodigoPais)
                    .addComponent(idPaisActual, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addGroup(jPanelNuevoPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNuevoNombrePais)
                    .addComponent(txtNuevoNombrePais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanelNuevoPaisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jCheckBoxActivo))
                .addGap(18, 18, 18)
                .addComponent(BtnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnCancelar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        btnCancelar.setText("Volver");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(93, 93, 93)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanelBuscarPais, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanelNuevoPais, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addComponent(btnCancelar)))
                .addContainerGap(116, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addComponent(jPanelBuscarPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(jPanelNuevoPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIdActionPerformed
        if (!txtBuscarId.getText().trim().isEmpty()) {
            String p_nom = txtBuscarId.getText().trim();
            Pais pais = Pais.buscarPorId(p_nom);
            if (!Administrador.error.isEmpty()) {
                JOptionPane.showMessageDialog(null, Administrador.error);
                Administrador.error = "";
            } else if (pais != null) {
                limpiarCampos();
                idPais.setText(String.valueOf(pais.getId_pais()));
                nomPais.setText(pais.getNombre_pais());
                codPais.setText(pais.getCodigo_pais());
                estPais.setText(pais.getEstado_pais() ? "Activo" : "Inactivo");                                                
            } else {
                JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
            }
        }
    }//GEN-LAST:event_btnBuscarIdActionPerformed

    private void btnBuscarCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarCodigoActionPerformed
        if (!txtBuscarCodigo.getText().trim().isEmpty()) {
            String p_cod = txtBuscarCodigo.getText().trim();
            Pais pais = Pais.buscarPorCodigo(p_cod);
            if (!Administrador.error.isEmpty()) {
                JOptionPane.showMessageDialog(null, Administrador.error);
                Administrador.error = "";
            } else if (pais != null) {
                limpiarCampos();
                idPais.setText(String.valueOf(pais.getId_pais()));
                nomPais.setText(pais.getNombre_pais());
                codPais.setText(pais.getCodigo_pais());
                estPais.setText(pais.getEstado_pais() ? "Activo" : "Inactivo");
            } else {
                JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
            }
        }
    }//GEN-LAST:event_btnBuscarCodigoActionPerformed

    private void btnModificarPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarPActionPerformed
        modificarOn();
        datosAModificar();
    }//GEN-LAST:event_btnModificarPActionPerformed

    private void btnEliminarPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarPActionPerformed
        if (Pais.eliminarPais(idPais.getText())) {
            JOptionPane.showMessageDialog(null, "Eliminación exitosa.");
            limpiarCampos();
        } else if (!Administrador.error.isEmpty()) {
            JOptionPane.showMessageDialog(null, Administrador.error);
            Administrador.error = "";
        } else {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
        }
    }//GEN-LAST:event_btnEliminarPActionPerformed

    private void BtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGuardarActionPerformed
        if (!validaCampos()) {
            JOptionPane.showMessageDialog(null, "Ingrese datos faltantes.");
        } else if (!codPais.getText().equals(txtNuevoCodigoPais.getText()) && !Pais.verificaCodigo(txtNuevoCodigoPais.getText())) { // Si se modificó el código y ya está registrado
            JOptionPane.showMessageDialog(null, Administrador.error);
            Administrador.error = "";
        } else if (Pais.modificarPais(idPaisActual.getText(), txtNuevoCodigoPais.getText(), txtNuevoNombrePais.getText(), String.valueOf(nuevoEstado()))) {
            JOptionPane.showMessageDialog(null, "Actualización exitosa.");
            limpiarCampos();
        } else if (!Administrador.error.isEmpty()) {
            JOptionPane.showMessageDialog(null, Administrador.error);
            Administrador.error = "";
        } else {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
        }
    }//GEN-LAST:event_BtnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void idPaisPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_idPaisPropertyChange
        if (idPais.getText().isEmpty()) {
            btnModificarP.setEnabled(false);
            btnEliminarP.setEnabled(false);
        } else {
            btnModificarP.setEnabled(true);
            btnEliminarP.setEnabled(true);
        }
    }//GEN-LAST:event_idPaisPropertyChange

    private void estPaisPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_estPaisPropertyChange
        if (estPais.getText().equals("Activo")) {
            btnEliminarP.setEnabled(true);
        } else {
            btnEliminarP.setEnabled(false);
        }
    }//GEN-LAST:event_estPaisPropertyChange

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AjustarPais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AjustarPais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AjustarPais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AjustarPais.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new AjustarPais().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnGuardar;
    private javax.swing.JButton btnBuscarCodigo;
    private javax.swing.JButton btnBuscarId;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminarP;
    private javax.swing.JButton btnModificarP;
    private javax.swing.JLabel codPais;
    private javax.swing.JLabel estPais;
    private javax.swing.JLabel idPais;
    private javax.swing.JLabel idPaisActual;
    private javax.swing.JCheckBox jCheckBoxActivo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanelBuscarPais;
    private javax.swing.JPanel jPanelNuevoPais;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblBuscarId;
    private javax.swing.JLabel lblBuscarNombre;
    private javax.swing.JLabel lblCodigoPais;
    private javax.swing.JLabel lblEstadoPais;
    private javax.swing.JLabel lblIdPais;
    private javax.swing.JLabel lblNombrePais;
    private javax.swing.JLabel lblNuevoCodigoPais;
    private javax.swing.JLabel lblNuevoIdPais;
    private javax.swing.JLabel lblNuevoNombrePais;
    private javax.swing.JLabel nomPais;
    private javax.swing.JTextField txtBuscarCodigo;
    private javax.swing.JTextField txtBuscarId;
    public static javax.swing.JTextField txtNuevoCodigoPais;
    public static javax.swing.JTextField txtNuevoNombrePais;
    // End of variables declaration//GEN-END:variables
}
