package cl.ontour.frontend.desktop.views.update;

import cl.ontour.backend.desktop.utilities.Funciones;

/**
 *
 * @author Andrea
 */
public class AjustarReserva extends javax.swing.JFrame {

    public AjustarReserva() {
        initComponents();

        setLocationRelativeTo(null);
        Funciones.activarComponentes(jPanelReserva, false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelReserva = new javax.swing.JPanel();
        lblCodigoReserva = new javax.swing.JLabel();
        txtCodigoReserva = new javax.swing.JTextField();
        lblDescripcionReserva = new javax.swing.JLabel();
        lblValorReserva = new javax.swing.JLabel();
        txtValorReserva = new javax.swing.JTextField();
        txtMontoAvalReserva = new javax.swing.JTextField();
        lblMontoAvalReserva = new javax.swing.JLabel();
        lblMontoAcumuladoReserva = new javax.swing.JLabel();
        lblFechaViajeReserva = new javax.swing.JLabel();
        txtFechaViajeReserva = new javax.swing.JTextField();
        lblFechaLimiteReserva = new javax.swing.JLabel();
        txtFechaLimiteReserva = new javax.swing.JTextField();
        txtMontoAcumuladoReserva = new javax.swing.JTextField();
        lblCantParticipantesReserva = new javax.swing.JLabel();
        txtCantParticipantesReserva = new javax.swing.JTextField();
        jComboBoxEjecutivo = new javax.swing.JComboBox();
        jComboBoxCurso = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jComboBoxPrograma = new javax.swing.JComboBox();
        txtDescripcionReserva = new javax.swing.JTextArea();
        jPanelBuscarReservas = new javax.swing.JPanel();
        txtBuscarNombre = new javax.swing.JTextField();
        btnBuscarNombre = new javax.swing.JButton();
        lblBuscarNombre = new javax.swing.JLabel();
        lblBuscarId = new javax.swing.JLabel();
        txtBuscarId = new javax.swing.JTextField();
        btnBuscarId = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        BtnGuardar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        BtnVolver = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelReserva.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ingrese una reserva"));

        lblCodigoReserva.setText("Código:");

        lblDescripcionReserva.setText("Descripcion");

        lblValorReserva.setText("Valor");

        lblMontoAvalReserva.setText("Monto aval");

        lblMontoAcumuladoReserva.setText("Monto acumulado");

        lblFechaViajeReserva.setText("Fecha viaje");

        txtFechaViajeReserva.setText("__/__/__");

        lblFechaLimiteReserva.setText("Fecha límite");

        txtFechaLimiteReserva.setText("__/__/__");

        lblCantParticipantesReserva.setText("Nº participantes");

        jComboBoxEjecutivo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Ejecutivo a cargo", "--", "--" }));

        jComboBoxCurso.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Curso", "--", "--" }));

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));

        jComboBoxPrograma.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Programa", "--", "--" }));

        txtDescripcionReserva.setColumns(20);
        txtDescripcionReserva.setRows(5);

        org.jdesktop.layout.GroupLayout jPanelReservaLayout = new org.jdesktop.layout.GroupLayout(jPanelReserva);
        jPanelReserva.setLayout(jPanelReservaLayout);
        jPanelReservaLayout.setHorizontalGroup(
            jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelReservaLayout.createSequentialGroup()
                .add(52, 52, 52)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jSeparator3)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelReservaLayout.createSequentialGroup()
                            .add(13, 13, 13)
                            .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jPanelReservaLayout.createSequentialGroup()
                                    .add(lblValorReserva)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(txtValorReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 162, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(lblMontoAcumuladoReserva)
                                        .add(lblMontoAvalReserva))
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                    .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                        .add(txtMontoAcumuladoReserva)
                                        .add(txtMontoAvalReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 168, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                                .add(jSeparator1)
                                .add(jPanelReservaLayout.createSequentialGroup()
                                    .add(lblFechaLimiteReserva)
                                    .add(11, 11, 11)
                                    .add(txtFechaLimiteReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 121, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 382, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(jPanelReservaLayout.createSequentialGroup()
                                    .add(lblFechaViajeReserva)
                                    .add(18, 18, 18)
                                    .add(txtFechaViajeReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 121, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(26, 26, 26)
                                    .add(lblCantParticipantesReserva)
                                    .add(18, 18, 18)
                                    .add(txtCantParticipantesReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 39, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .add(jComboBoxCurso, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 159, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(org.jdesktop.layout.GroupLayout.TRAILING, jSeparator2)))
                        .add(jPanelReservaLayout.createSequentialGroup()
                            .add(jComboBoxEjecutivo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 298, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jComboBoxPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 298, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(jPanelReservaLayout.createSequentialGroup()
                        .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jPanelReservaLayout.createSequentialGroup()
                                .add(lblDescripcionReserva)
                                .add(18, 18, 18)
                                .add(txtDescripcionReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 506, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanelReservaLayout.createSequentialGroup()
                                .add(35, 35, 35)
                                .add(lblCodigoReserva)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(txtCodigoReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 210, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(3, 3, 3)))
                .add(68, 68, 68))
        );
        jPanelReservaLayout.setVerticalGroup(
            jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelReservaLayout.createSequentialGroup()
                .add(31, 31, 31)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(lblCodigoReserva)
                    .add(txtCodigoReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblDescripcionReserva)
                    .add(txtDescripcionReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 40, Short.MAX_VALUE)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(txtValorReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(lblValorReserva))
                    .add(jPanelReservaLayout.createSequentialGroup()
                        .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(lblMontoAvalReserva)
                            .add(txtMontoAvalReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(lblMontoAcumuladoReserva)
                            .add(txtMontoAcumuladoReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .add(18, 18, 18)
                .add(jSeparator2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblFechaViajeReserva)
                    .add(txtFechaViajeReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblCantParticipantesReserva)
                    .add(txtCantParticipantesReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jComboBoxCurso, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblFechaLimiteReserva)
                    .add(txtFechaLimiteReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSeparator3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(28, 28, 28)
                .add(jPanelReservaLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboBoxEjecutivo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jComboBoxPrograma, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(70, 70, 70))
        );

        jPanelBuscarReservas.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Buscar reserva"));

        btnBuscarNombre.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarNombre.setText("Buscar");
        btnBuscarNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarNombreActionPerformed(evt);
            }
        });

        lblBuscarNombre.setText("Buscar por nombre:");

        lblBuscarId.setText("Buscar por Id:");

        btnBuscarId.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnBuscarId.setText("Buscar");
        btnBuscarId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIdActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanelBuscarReservasLayout = new org.jdesktop.layout.GroupLayout(jPanelBuscarReservas);
        jPanelBuscarReservas.setLayout(jPanelBuscarReservasLayout);
        jPanelBuscarReservasLayout.setHorizontalGroup(
            jPanelBuscarReservasLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBuscarReservasLayout.createSequentialGroup()
                .add(26, 26, 26)
                .add(jPanelBuscarReservasLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(lblBuscarId)
                    .add(lblBuscarNombre))
                .add(35, 35, 35)
                .add(jPanelBuscarReservasLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(txtBuscarNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(32, 32, 32)
                .add(jPanelBuscarReservasLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarNombre)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, btnBuscarId))
                .addContainerGap(143, Short.MAX_VALUE))
        );
        jPanelBuscarReservasLayout.setVerticalGroup(
            jPanelBuscarReservasLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarReservasLayout.createSequentialGroup()
                .add(20, 20, 20)
                .add(jPanelBuscarReservasLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarNombre, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarNombre)
                    .add(lblBuscarNombre))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelBuscarReservasLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txtBuscarId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarId)
                    .add(lblBuscarId))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        btnModificar.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnEliminar.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        BtnGuardar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnGuardar.setText("Guardar");
        BtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnGuardarActionPerformed(evt);
            }
        });

        btnLimpiar.setFont(new java.awt.Font(".SF NS Text", 0, 16)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        BtnVolver.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnVolver.setText("Volver");
        BtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVolverActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(70, 70, 70)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(BtnVolver)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 390, Short.MAX_VALUE)
                        .add(btnLimpiar)
                        .add(43, 43, 43)
                        .add(BtnGuardar))
                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelReserva, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                            .add(btnModificar)
                            .add(18, 18, 18)
                            .add(btnEliminar))
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarReservas, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(82, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(27, 27, 27)
                .add(jPanelBuscarReservas, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(22, 22, 22)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnEliminar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnModificar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanelReserva, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(40, 40, 40)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btnLimpiar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 47, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(BtnGuardar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(BtnVolver, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarNombreActionPerformed

    }//GEN-LAST:event_btnBuscarNombreActionPerformed

    private void btnBuscarIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIdActionPerformed

    }//GEN-LAST:event_btnBuscarIdActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        Funciones.activarComponentes(jPanelReserva, true);
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void BtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGuardarActionPerformed

    }//GEN-LAST:event_BtnGuardarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed

    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void BtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVolverActionPerformed

        dispose();
    }//GEN-LAST:event_BtnVolverActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AjustarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AjustarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AjustarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AjustarReserva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new AjustarReserva().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnGuardar;
    private javax.swing.JButton BtnVolver;
    private javax.swing.JButton btnBuscarId;
    private javax.swing.JButton btnBuscarNombre;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JComboBox jComboBoxCurso;
    private javax.swing.JComboBox jComboBoxEjecutivo;
    private javax.swing.JComboBox jComboBoxPrograma;
    private javax.swing.JPanel jPanelBuscarReservas;
    private javax.swing.JPanel jPanelReserva;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel lblBuscarId;
    private javax.swing.JLabel lblBuscarNombre;
    private javax.swing.JLabel lblCantParticipantesReserva;
    private javax.swing.JLabel lblCodigoReserva;
    private javax.swing.JLabel lblDescripcionReserva;
    private javax.swing.JLabel lblFechaLimiteReserva;
    private javax.swing.JLabel lblFechaViajeReserva;
    private javax.swing.JLabel lblMontoAcumuladoReserva;
    private javax.swing.JLabel lblMontoAvalReserva;
    private javax.swing.JLabel lblValorReserva;
    private javax.swing.JTextField txtBuscarId;
    private javax.swing.JTextField txtBuscarNombre;
    private javax.swing.JTextField txtCantParticipantesReserva;
    private javax.swing.JTextField txtCodigoReserva;
    private javax.swing.JTextArea txtDescripcionReserva;
    private javax.swing.JTextField txtFechaLimiteReserva;
    private javax.swing.JTextField txtFechaViajeReserva;
    private javax.swing.JTextField txtMontoAcumuladoReserva;
    private javax.swing.JTextField txtMontoAvalReserva;
    private javax.swing.JTextField txtValorReserva;
    // End of variables declaration//GEN-END:variables

    public void limpiaCampos() {
    }
}
