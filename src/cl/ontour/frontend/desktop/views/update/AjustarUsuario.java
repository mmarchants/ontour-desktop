package cl.ontour.frontend.desktop.views.update;

import cl.ontour.backend.desktop.domains.Agencia;
import cl.ontour.backend.desktop.domains.Apoderado;
import cl.ontour.backend.desktop.domains.Empleado;
import cl.ontour.backend.desktop.domains.Rol;
import cl.ontour.backend.desktop.domains.Usuario;
import cl.ontour.backend.desktop.utilities.Funciones;
import cl.ontour.frontend.desktop.Administrador;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Andrea
 */
public class AjustarUsuario extends javax.swing.JFrame {

    public AjustarUsuario() {
        initComponents();
        setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/cl/ontour/frontend/desktop/resources/images/logo_ontour_icon.png")));
        Funciones.activarComponentes(jPanelModificarUsuario, false);
    }
    
    private void limpiarCampos() {
        txtBuscarPorId.setText("");
        txtBuscarPorCorreo.setText("");
        lblIdBuscado.setText("");
        lblCorreoBuscado.setText("");
        lblRolBuscado.setText("");
        lblNombreBuscado.setText("");
        lblPrimerApellidoBuscado.setText("");
        lblSegundoApellidoBuscado.setText("");
        lblTelefonoBuscado.setText("");
        lblCodigoBuscado.setText("");
        lblEstadoBuscado.setText("");
        lblNombreBuscado.setText("");
        lblIdAModificar.setText("");
        lblCorreoAModificar.setText("");
        jCheckBoxActivo.setSelected(false);
        jComboBoxRol.removeAllItems();
        txtCodigoUsuario.setText("");
        txtTelefono.setText("");
        txtNombreUsuario.setText("");
        txtPrimerApellido.setText("");
        txtSegundoApellido.setText("");
        jCheckBoxContrasegna.setSelected(false);
        txtContrasegna.setEnabled(false);
        txtContrasegna.setText("");
        Funciones.activarComponentes(jPanelModificarUsuario, false);
    }
    
    private void formatearEstado() {
        String estado = lblEstadoBuscado.getText();
        if (estado.equals("Activo")) {
            jCheckBoxActivo.setSelected(true);
        }
        if (estado.equals("Inactivo")) {
            jCheckBoxActivo.setSelected(false);
        }
    }
    
    private boolean nuevoEstado() {
        if (jCheckBoxActivo.isSelected()) {
            return true;
        }
        return false;
    }
    
    private void cargarRoles() {
        jComboBoxRol.removeAllItems();
        List<Rol> roles = Rol.listadoRoles();
        if (lblRolBuscado.getText().startsWith("APO") || lblRolBuscado.getText().startsWith("REP")) {
            for (int i = 0; i < roles.size(); i++) {                        
                if (roles.get(i).getCodigo().equals("APO") || roles.get(i).getCodigo().equals("REP")) {
                    jComboBoxRol.addItem(roles.get(i));
                } 
            }
        } else {
            for (int i = 0; i < roles.size(); i++) {                        
                if (!roles.get(i).getCodigo().equals("APO") && !roles.get(i).getCodigo().equals("REP")) {
                    jComboBoxRol.addItem(roles.get(i));
                } 
            }
        }
    }
    
    private void datosAModificar() {
        lblIdAModificar.setText(lblIdBuscado.getText());
        lblCorreoAModificar.setText(lblCorreoBuscado.getText());
        formatearEstado();
        for (int i = 0; i < jComboBoxRol.getItemCount(); i++) {
            if (jComboBoxRol.getItemAt(i).toString().equals(lblRolBuscado.getText())) {
                jComboBoxRol.setSelectedIndex(i);
                break;
            }
        }
        if (!lblCodigoBuscado.getText().equals("N/A")) {
            txtCodigoUsuario.setText(lblCodigoBuscado.getText());
        }
        txtTelefono.setText(lblTelefonoBuscado.getText());
        txtNombreUsuario.setText(lblNombreBuscado.getText());
        txtPrimerApellido.setText(lblPrimerApellidoBuscado.getText());
        txtSegundoApellido.setText(lblSegundoApellidoBuscado.getText());        
    }
    
    private boolean validaCampos() {
        if (txtTelefono.getText().isEmpty() || 
            txtNombreUsuario.getText().isEmpty() || 
            txtPrimerApellido.getText().isEmpty() || 
            txtSegundoApellido.getText().isEmpty() ||
            (txtCodigoUsuario.isEnabled() && txtCodigoUsuario.getText().isEmpty()) || 
            (jCheckBoxContrasegna.isSelected() && txtContrasegna.getText().isEmpty())) {
            return false;
        }
        return true;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGroupTipoUsuario = new javax.swing.ButtonGroup();
        BtnVolver = new javax.swing.JButton();
        jPanelBuscarUsuario = new javax.swing.JPanel();
        lblBuscarPorId = new javax.swing.JLabel();
        txtBuscarPorId = new javax.swing.JTextField();
        btnBuscarId = new javax.swing.JButton();
        lblBuscarPorCorreo = new javax.swing.JLabel();
        txtBuscarPorCorreo = new javax.swing.JTextField();
        btnBuscarPorCorreo = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        lblIdBuscado = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblNombreBuscado = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblCorreoBuscado = new javax.swing.JLabel();
        lblPrimerApellidoBuscado = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        lblRolBuscado = new javax.swing.JLabel();
        lblSegundoApellidoBuscado = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        lblTelefonoBuscado = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        lblCodigoBuscado = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        lblEstadoBuscado = new javax.swing.JLabel();
        btnEliminar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        jPanelModificarUsuario = new javax.swing.JPanel();
        lblCodigo = new javax.swing.JLabel();
        txtCodigoUsuario = new javax.swing.JTextField();
        lblNombre = new javax.swing.JLabel();
        txtNombreUsuario = new javax.swing.JTextField();
        txtPrimerApellido = new javax.swing.JTextField();
        lblPrimerApellido = new javax.swing.JLabel();
        txtTelefono = new javax.swing.JTextField();
        lblTelefono = new javax.swing.JLabel();
        txtSegundoApellido = new javax.swing.JTextField();
        lblSegundoApellido = new javax.swing.JLabel();
        jComboBoxRol = new javax.swing.JComboBox();
        lblRolUsuarioAModificar = new javax.swing.JLabel();
        BtnGuardar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jCheckBoxActivo = new javax.swing.JCheckBox();
        lblIdAModificar = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblCorreoAModificar = new javax.swing.JLabel();
        jCheckBoxContrasegna = new javax.swing.JCheckBox();
        txtContrasegna = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ajustar usuarios");
        setBounds(new java.awt.Rectangle(0, 23, 1085, 1000));

        BtnVolver.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnVolver.setText("Volver");
        BtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnVolverActionPerformed(evt);
            }
        });

        jPanelBuscarUsuario.setBorder(javax.swing.BorderFactory.createTitledBorder("Buscar usuario"));

        lblBuscarPorId.setText("Buscar por ID:");

        btnBuscarId.setText("Buscar");
        btnBuscarId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarIdActionPerformed(evt);
            }
        });

        lblBuscarPorCorreo.setText("Buscar por correo electrónico:");

        btnBuscarPorCorreo.setText("Buscar");
        btnBuscarPorCorreo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarPorCorreoActionPerformed(evt);
            }
        });

        jLabel3.setText("ID:");

        lblIdBuscado.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                lblIdBuscadoPropertyChange(evt);
            }
        });

        jLabel5.setText("Nombre:");

        jLabel7.setText("Correo electrónico:");

        jLabel8.setText("Primer apellido:");

        jLabel11.setText("Privilegios:");

        jLabel12.setText("Segundo apellido:");

        jLabel15.setText("Teléfono:");

        jLabel17.setText("Código:");

        jLabel19.setText("Estado:");

        lblEstadoBuscado.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                lblEstadoBuscadoPropertyChange(evt);
            }
        });

        btnEliminar.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnModificar.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanelBuscarUsuarioLayout = new org.jdesktop.layout.GroupLayout(jPanelBuscarUsuario);
        jPanelBuscarUsuario.setLayout(jPanelBuscarUsuarioLayout);
        jPanelBuscarUsuarioLayout.setHorizontalGroup(
            jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBuscarUsuarioLayout.createSequentialGroup()
                .add(166, 166, 166)
                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(lblBuscarPorCorreo)
                    .add(lblBuscarPorId))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(txtBuscarPorId)
                    .add(txtBuscarPorCorreo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btnBuscarId)
                    .add(btnBuscarPorCorreo))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .add(jPanelBuscarUsuarioLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jSeparator1)
                    .add(jPanelBuscarUsuarioLayout.createSequentialGroup()
                        .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jPanelBuscarUsuarioLayout.createSequentialGroup()
                                .add(btnModificar)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(btnEliminar))
                            .add(jPanelBuscarUsuarioLayout.createSequentialGroup()
                                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(jLabel5)
                                    .add(jLabel15)
                                    .add(jLabel3))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(jPanelBuscarUsuarioLayout.createSequentialGroup()
                                        .add(lblTelefonoBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(jLabel17))
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarUsuarioLayout.createSequentialGroup()
                                        .add(lblNombreBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(jLabel8))
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarUsuarioLayout.createSequentialGroup()
                                        .add(lblIdBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jLabel7)))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                                    .add(jPanelBuscarUsuarioLayout.createSequentialGroup()
                                        .add(lblCorreoBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(jLabel11))
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarUsuarioLayout.createSequentialGroup()
                                        .add(lblCodigoBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .add(jLabel19))
                                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelBuscarUsuarioLayout.createSequentialGroup()
                                        .add(lblPrimerApellidoBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                        .add(jLabel12)))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(lblEstadoBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(lblSegundoApellidoBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(lblRolBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                        .add(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanelBuscarUsuarioLayout.setVerticalGroup(
            jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelBuscarUsuarioLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblBuscarPorId)
                    .add(txtBuscarPorId, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarId))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblBuscarPorCorreo)
                    .add(txtBuscarPorCorreo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnBuscarPorCorreo))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel3)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel11)
                            .add(lblRolBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(lblIdBuscado, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(lblCorreoBuscado, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .add(jLabel7))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(org.jdesktop.layout.GroupLayout.TRAILING, jLabel5)
                        .add(jLabel12)
                        .add(lblSegundoApellidoBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(lblNombreBuscado, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jLabel8)
                    .add(lblPrimerApellidoBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanelBuscarUsuarioLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel19)
                            .add(lblEstadoBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(lblTelefonoBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(lblCodigoBuscado, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                    .add(jPanelBuscarUsuarioLayout.createSequentialGroup()
                        .add(5, 5, 5)
                        .add(jLabel15))
                    .add(jPanelBuscarUsuarioLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel17)))
                .add(18, 18, 18)
                .add(jPanelBuscarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(btnModificar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(btnEliminar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 58, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanelModificarUsuario.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Ingrese nuevos datos"));

        lblCodigo.setText("Código:");

        lblNombre.setText("Nombre:");

        lblPrimerApellido.setText("Primer apellido:");

        lblTelefono.setText("Teléfono:");

        lblSegundoApellido.setText("Segundo apellido:");

        jComboBoxRol.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxRolItemStateChanged(evt);
            }
        });

        lblRolUsuarioAModificar.setText("Rol:");

        BtnGuardar.setFont(new java.awt.Font(".SF NS Text", 1, 16)); // NOI18N
        BtnGuardar.setText("Guardar");
        BtnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnGuardarActionPerformed(evt);
            }
        });

        jLabel6.setText("Estado:");

        jCheckBoxActivo.setText("Activo");

        jLabel4.setText("ID:");

        jLabel9.setText("Correo electrónico:");

        jCheckBoxContrasegna.setText("Modificar contraseña:");
        jCheckBoxContrasegna.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBoxContrasegnaItemStateChanged(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanelModificarUsuarioLayout = new org.jdesktop.layout.GroupLayout(jPanelModificarUsuario);
        jPanelModificarUsuario.setLayout(jPanelModificarUsuarioLayout);
        jPanelModificarUsuarioLayout.setHorizontalGroup(
            jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelModificarUsuarioLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jPanelModificarUsuarioLayout.createSequentialGroup()
                        .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelModificarUsuarioLayout.createSequentialGroup()
                                .add(lblNombre)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(txtNombreUsuario, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(lblPrimerApellido)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(txtPrimerApellido, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanelModificarUsuarioLayout.createSequentialGroup()
                                .add(32, 32, 32)
                                .add(lblRolUsuarioAModificar)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jComboBoxRol, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(54, 54, 54)
                                .add(lblCodigo)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(txtCodigoUsuario, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                        .add(18, 18, 18)
                        .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(lblSegundoApellido)
                            .add(lblTelefono))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(txtSegundoApellido, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(txtTelefono, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(jPanelModificarUsuarioLayout.createSequentialGroup()
                        .add(jLabel4)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(lblIdAModificar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 50, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(jLabel9)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(lblCorreoAModificar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 200, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(jLabel6)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jCheckBoxActivo))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanelModificarUsuarioLayout.createSequentialGroup()
                        .add(jCheckBoxContrasegna)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(txtContrasegna, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 197, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(BtnGuardar)))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelModificarUsuarioLayout.setVerticalGroup(
            jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanelModificarUsuarioLayout.createSequentialGroup()
                .add(10, 10, 10)
                .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                        .add(jLabel4)
                        .add(lblIdAModificar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(lblCorreoAModificar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jLabel9)))
                    .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jLabel6)
                        .add(jCheckBoxActivo)))
                .add(18, 18, 18)
                .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboBoxRol, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblRolUsuarioAModificar)
                    .add(lblCodigo)
                    .add(txtCodigoUsuario, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblTelefono)
                    .add(txtTelefono, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(lblNombre)
                    .add(txtNombreUsuario, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblPrimerApellido)
                    .add(txtPrimerApellido, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(lblSegundoApellido)
                    .add(txtSegundoApellido, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(18, 18, 18)
                .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(BtnGuardar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanelModificarUsuarioLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                        .add(jCheckBoxContrasegna)
                        .add(txtContrasegna, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(jPanelModificarUsuario, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, BtnVolver)
                    .add(jPanelBuscarUsuario, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanelBuscarUsuario, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanelModificarUsuario, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(BtnVolver, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(14, 14, 14))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        if (Usuario.eliminarUsuario(lblIdBuscado.getText())) {
            JOptionPane.showMessageDialog(null, "Eliminación exitosa.");
            limpiarCampos();
        } else if (!Administrador.error.isEmpty()) {
            JOptionPane.showMessageDialog(null, Administrador.error);
            Administrador.error = "";
        } else {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        Funciones.activarComponentes(jPanelModificarUsuario, true);
        txtContrasegna.setEnabled(false);
        cargarRoles();
        datosAModificar();
    }//GEN-LAST:event_btnModificarActionPerformed

    private void BtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnVolverActionPerformed
        dispose();
    }//GEN-LAST:event_BtnVolverActionPerformed

    private void btnBuscarIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarIdActionPerformed
        if (!txtBuscarPorId.getText().trim().isEmpty()) {
            Usuario usuario = Usuario.buscarPorId(txtBuscarPorId.getText().trim());
            if (usuario != null) {
                limpiarCampos();
                lblIdBuscado.setText(String.valueOf(usuario.getId()));
                lblCorreoBuscado.setText(usuario.getCorreo_electronico());
                lblRolBuscado.setText(usuario.getRol().toString());
                lblEstadoBuscado.setText(usuario.isEstado() ? "Activo" : "Inactivo");
                if (usuario.getRol().getCodigo().equals("APO") || usuario.getRol().getCodigo().equals("REP")) {
                    Apoderado apoderado = Apoderado.buscarPorCorreo(usuario.getCorreo_electronico());
                    lblNombreBuscado.setText(apoderado.getNombre());
                    lblPrimerApellidoBuscado.setText(apoderado.getPrimer_apellido());
                    lblSegundoApellidoBuscado.setText(apoderado.getSegundo_apellido());
                    lblTelefonoBuscado.setText(apoderado.getTelefono());
                    lblCodigoBuscado.setText("N/A");
                } else {
                    Empleado empleado = Empleado.buscarPorCorreo(usuario.getCorreo_electronico());
                    lblNombreBuscado.setText(empleado.getNombre());
                    lblPrimerApellidoBuscado.setText(empleado.getPrimer_apellido());
                    lblSegundoApellidoBuscado.setText(empleado.getSegundo_apellido());
                    lblTelefonoBuscado.setText(empleado.getTelefono());
                    lblCodigoBuscado.setText(empleado.getCodigo());
                }
            } else {
                JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
            }
        }
    }//GEN-LAST:event_btnBuscarIdActionPerformed

    private void btnBuscarPorCorreoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarPorCorreoActionPerformed
        if (!txtBuscarPorCorreo.getText().trim().isEmpty()) {
            Usuario usuario = Usuario.buscarPorCorreo(txtBuscarPorCorreo.getText().trim());
            if (usuario != null) {
                limpiarCampos();
                lblIdBuscado.setText(String.valueOf(usuario.getId()));
                lblCorreoBuscado.setText(usuario.getCorreo_electronico());
                lblRolBuscado.setText(usuario.getRol().toString());
                lblEstadoBuscado.setText(usuario.isEstado() ? "Activo" : "Inactivo");
                if (usuario.getRol().getCodigo().equals("APO") || usuario.getRol().getCodigo().equals("REP")) {
                    Apoderado apoderado = Apoderado.buscarPorCorreo(usuario.getCorreo_electronico());
                    lblNombreBuscado.setText(apoderado.getNombre());
                    lblPrimerApellidoBuscado.setText(apoderado.getPrimer_apellido());
                    lblSegundoApellidoBuscado.setText(apoderado.getSegundo_apellido());
                    lblTelefonoBuscado.setText(apoderado.getTelefono());
                    lblCodigoBuscado.setText("N/A");
                } else {
                    Empleado empleado = Empleado.buscarPorCorreo(usuario.getCorreo_electronico());
                    lblNombreBuscado.setText(empleado.getNombre());
                    lblPrimerApellidoBuscado.setText(empleado.getPrimer_apellido());
                    lblSegundoApellidoBuscado.setText(empleado.getSegundo_apellido());
                    lblTelefonoBuscado.setText(empleado.getTelefono());
                    lblCodigoBuscado.setText(empleado.getCodigo());
                }
            } else {
                JOptionPane.showMessageDialog(null, "No existen resultados en su búsqueda. Intente nuevamente.");
            }
        }
    }//GEN-LAST:event_btnBuscarPorCorreoActionPerformed

    private void BtnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGuardarActionPerformed
        if (!validaCampos()) {
            JOptionPane.showMessageDialog(null, "Ingrese datos faltantes.");
            return;
        }
        if (Usuario.modificarUsuario(lblIdAModificar.getText(), lblCorreoAModificar.getText(), jCheckBoxContrasegna.isSelected() ? String.valueOf(txtContrasegna.getPassword()).trim() : "", (Rol) jComboBoxRol.getSelectedItem(), String.valueOf(nuevoEstado()))) {
            Usuario usuario = Usuario.buscarPorId(lblIdAModificar.getText());
            // Actualización de apoderados:
            if (lblRolBuscado.getText().startsWith("APO") || lblRolBuscado.getText().startsWith("REP")) {                 
                Apoderado apoderado = Apoderado.buscarPorCorreo(usuario.getCorreo_electronico());
                if (Apoderado.modificarApoderado(apoderado.getId().toString(), txtNombreUsuario.getText(), txtPrimerApellido.getText(), txtSegundoApellido.getText(), txtTelefono.getText(), usuario)) {
                    JOptionPane.showMessageDialog(null, "Actualización exitosa.");
                    limpiarCampos();
                } else if (!Administrador.error.isEmpty()) {
                    JOptionPane.showMessageDialog(null, Administrador.error);
                    Administrador.error = "";
                } else {
                    JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
                }                
            } else { // Actualización de empleados:
                Empleado empleado = Empleado.buscarPorCorreo(usuario.getCorreo_electronico());
                if (Empleado.modificarEmpleado(empleado.getId().toString(), txtCodigoUsuario.getText(), txtNombreUsuario.getText(), txtPrimerApellido.getText(), txtSegundoApellido.getText(), txtTelefono.getText(), usuario, Agencia.buscarPorId("1"))) {
                    JOptionPane.showMessageDialog(null, "Actualización exitosa.");
                    limpiarCampos();
                } else if (!Administrador.error.isEmpty()) {
                    JOptionPane.showMessageDialog(null, Administrador.error);
                    Administrador.error = "";
                } else {
                    JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
                }
            }
        } else if (!Administrador.error.isEmpty()) {
            JOptionPane.showMessageDialog(null, Administrador.error);
            Administrador.error = "";
        } else {
            JOptionPane.showMessageDialog(null, "Ha ocurrido un error. Intente nuevamente.");
        }   
    }//GEN-LAST:event_BtnGuardarActionPerformed

    private void jComboBoxRolItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxRolItemStateChanged
        if (jComboBoxRol.getItemCount() != 0) {
            if (((Rol)jComboBoxRol.getSelectedItem()).getCodigo().equals("APO") || ((Rol)jComboBoxRol.getSelectedItem()).getCodigo().equals("REP")) {
                txtCodigoUsuario.setText("");
                txtCodigoUsuario.setEnabled(false);
            } else {
                txtCodigoUsuario.setEnabled(true);
            }
        }
    }//GEN-LAST:event_jComboBoxRolItemStateChanged

    private void lblIdBuscadoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_lblIdBuscadoPropertyChange
        if (lblIdBuscado.getText().isEmpty()) {
            btnModificar.setEnabled(false);
            btnEliminar.setEnabled(false);
        } else {
            btnModificar.setEnabled(true);
            btnEliminar.setEnabled(true);
        }
    }//GEN-LAST:event_lblIdBuscadoPropertyChange

    private void lblEstadoBuscadoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_lblEstadoBuscadoPropertyChange
        if (lblEstadoBuscado.getText().equals("Activo")) {
            btnEliminar.setEnabled(true);
        } else {
            btnEliminar.setEnabled(false);
        }
    }//GEN-LAST:event_lblEstadoBuscadoPropertyChange

    private void jCheckBoxContrasegnaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBoxContrasegnaItemStateChanged
        if (jCheckBoxContrasegna.isSelected()) {
            txtContrasegna.setEnabled(true);
        } else {
            txtContrasegna.setEnabled(false);
            txtContrasegna.setText("");
        }
    }//GEN-LAST:event_jCheckBoxContrasegnaItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AjustarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AjustarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AjustarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AjustarUsuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>


        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new AjustarUsuario().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnGuardar;
    private javax.swing.JButton BtnVolver;
    private javax.swing.JButton btnBuscarId;
    private javax.swing.JButton btnBuscarPorCorreo;
    private javax.swing.JButton btnEliminar;
    private javax.swing.ButtonGroup btnGroupTipoUsuario;
    private javax.swing.JButton btnModificar;
    private javax.swing.JCheckBox jCheckBoxActivo;
    private javax.swing.JCheckBox jCheckBoxContrasegna;
    public static javax.swing.JComboBox jComboBoxRol;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanelBuscarUsuario;
    private javax.swing.JPanel jPanelModificarUsuario;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lblBuscarPorCorreo;
    private javax.swing.JLabel lblBuscarPorId;
    private javax.swing.JLabel lblCodigo;
    private javax.swing.JLabel lblCodigoBuscado;
    private javax.swing.JLabel lblCorreoAModificar;
    private javax.swing.JLabel lblCorreoBuscado;
    private javax.swing.JLabel lblEstadoBuscado;
    private javax.swing.JLabel lblIdAModificar;
    private javax.swing.JLabel lblIdBuscado;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblNombreBuscado;
    private javax.swing.JLabel lblPrimerApellido;
    private javax.swing.JLabel lblPrimerApellidoBuscado;
    private javax.swing.JLabel lblRolBuscado;
    private javax.swing.JLabel lblRolUsuarioAModificar;
    private javax.swing.JLabel lblSegundoApellido;
    private javax.swing.JLabel lblSegundoApellidoBuscado;
    private javax.swing.JLabel lblTelefono;
    private javax.swing.JLabel lblTelefonoBuscado;
    private javax.swing.JTextField txtBuscarPorCorreo;
    private javax.swing.JTextField txtBuscarPorId;
    private javax.swing.JTextField txtCodigoUsuario;
    private javax.swing.JPasswordField txtContrasegna;
    private javax.swing.JTextField txtNombreUsuario;
    private javax.swing.JTextField txtPrimerApellido;
    private javax.swing.JTextField txtSegundoApellido;
    private javax.swing.JTextField txtTelefono;
    // End of variables declaration//GEN-END:variables

}
